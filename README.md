ai-common-scala

A common package containing functionality or extensions which 
are domain agnostic but largely pre-required for building machine learning applications. 
The ai-common-scala is intended to be used by user applications.

##### Publish new snapshot version

> sbt publish


##### Publish new version

> sbt release
