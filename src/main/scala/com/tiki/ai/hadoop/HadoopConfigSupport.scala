package com.tiki.ai.hadoop

import org.apache.hadoop.conf.Configuration

import scala.collection.{Seq, mutable}

trait HadoopConfigSupport {

  def hadoopConfigUpdates: mutable.Builder[Configuration => Unit, Seq[Configuration => Unit]]

  /***
    *
    * Apply the different config flavors to the config object.
    * This can be done multiple times to a [[Configuration]] object
    *
    * @param conf the [[Configuration]] object which needs to be altered
    */
  def applyHadoopConfig(conf: Configuration): Unit = hadoopConfigUpdates.result().foreach(_(conf))

  /***
    *
    * Collects all flavors of hadoop config.
    *
    * @param cb a callback to manipulate the hadoop configuration
    */
  def updateHadoopConfig(cb: Configuration => Unit): Unit = hadoopConfigUpdates.+=(cb)
}
