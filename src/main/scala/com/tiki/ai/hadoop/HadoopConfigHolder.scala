package com.tiki.ai.hadoop

import org.apache.hadoop.conf.Configuration

import scala.collection.{Seq, mutable}

trait HadoopConfigHolder extends HadoopConfigSupport {

  lazy val hadoopConfigUpdates: mutable.Builder[Configuration => Unit, Seq[Configuration => Unit]] = Seq.newBuilder

  implicit def hadoopConfiguration: Configuration = {
    val c = new Configuration()
    applyHadoopConfig(c)
    c
  }
}
