package com.tiki.ai.mqtt

import akka.Done
import akka.actor.ActorSystem
import akka.stream.alpakka.mqtt._
import akka.stream.alpakka.mqtt.scaladsl.MqttSink
import akka.stream.scaladsl._
import akka.util.ByteString
import com.tiki.ai.akka.AkkaContext
import com.tiki.ai.config.CommonLibConfig

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

object MqttTopics {
  val QosRegisterAsync = "qos/register/async"
  val QosResultAsync   = "qos/result/async"
}

trait MqttAkkaContext extends AkkaContext {
  override implicit def system: ActorSystem        = ActorSystem("mqtt")
  override implicit def ec: ExecutionContext       = system.dispatcher
}

trait MqttClient extends MqttAkkaContext {
  val defaultTimeout: FiniteDuration = 5.seconds
  def publishMessage(msg: String, payload: String, mqttQos: MqttQoS): Future[Done]
}

class MqttClientImpl(clientId: String) extends MqttClient {

  private val connectionSettings: MqttConnectionSettings = CommonLibConfig.MqttConfig.connectionSettings
  private val sinkSettings: MqttConnectionSettings       = connectionSettings.withClientId(clientId)

  def publishMessage(msg: String, payload: String, mqttQos: MqttQoS): Future[Done] = {
    val mqttMessage = MqttMessage(payload, ByteString(msg))
    Source.single(mqttMessage).runWith(MqttSink(sinkSettings, mqttQos))
  }

}
