package com.tiki.ai.mqtt

import akka.stream.alpakka.mqtt.MqttConnectionSettings
import com.tiki.ai.config.ConfigHolder
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence

trait MqttConfig {
  self: ConfigHolder =>

  private lazy val authConf = config.getConfig(MqttConfigKeys.MQTT_BLOCK)

  object MqttConfig {
    lazy val brokerUrl: String = authConf.getString(MqttConfigKeys.BROKER_URL)
    lazy val connectionSettings = MqttConnectionSettings(
      brokerUrl,
      "ai-common-scala",
      new MemoryPersistence
    )
  }

  private object MqttConfigKeys {
    val MQTT_BLOCK           = "mqtt"
    val BROKER_URL           = "brokerUrl"
  }

}
