package com.tiki.ai.config

import com.typesafe.config.{Config, ConfigFactory}

trait ConfigHolder {

  lazy val mainConfig: Config = ConfigFactory.load()

  //TODO : make clear what should happen here ....
  //val config: Option[Config] = mainConfig.getObject("tiki").map(_.toConfig)
  val config: Config = mainConfig.getConfig("tiki")

  implicit class RichConfig(val underlying: Config) {

    def getOptionalString(path: String): Option[String] =
      if (underlying.hasPath(path)) {
        Some(underlying.getString(path))
      } else {
        None
      }

    def getOptionalConfig(path: String): Option[Config] =
      if (underlying.hasPath(path)) {
        Some(underlying.getConfig(path))
      } else {
        None
      }

  }
}
