package com.tiki.ai.config

import com.tiki.ai.authentication.AuthConfig
import com.tiki.ai.fs.FileSystemConfig
import com.tiki.ai.mqtt.MqttConfig
import com.tiki.ai.spark.SparkConfig

object CommonLibConfig extends ConfigHolder with CommonLibConfig with FileSystemConfig with AuthConfig with SparkConfig with MqttConfig{}

trait CommonLibConfig { self: ConfigHolder =>

}
