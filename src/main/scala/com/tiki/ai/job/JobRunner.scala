package com.tiki.ai.job

import com.tiki.ai.authentication.oidc.OidcContext
import com.tiki.ai.authentication.{RequiredRoles, RoleChecking}
import com.tiki.ai.spark.{SparkApp, SparkSessionFactory}

import java.util.logging.{Level, Logger}
import scala.util.control.NonFatal
import scala.util.{Failure, Success, Try}

/**
  * This wraps any exception which was thrown during internal job execution
  */
final case class JobExecutionException(cause: Throwable) extends Throwable(cause)

/**
  * This exception marks is thrown when the user does not have the required roles
  */
final case class InsufficientRolesException(requiredRoles: Array[String]) extends Throwable

object JobRunner extends RoleChecking with OidcContext {

  @transient
  private val log = Logger.getLogger(getClass.getName)

  /**
    *
    * @return 0 if job was successfully executed, otherwise -1
    */
  def executeJob[TConfig <: JobConfig](job: Job[TConfig], jobConfig: TConfig)(
      implicit sparkSessionFactory: Option[SparkSessionFactory] = None): Try[Unit] = Try {

    job match {
      case sparkJob: SparkApp[TConfig] =>
        sparkSessionFactory.foreach(sparkJob.setSparkSessionFactory)
      case _ =>
    }

    if (job.getClass.isAnnotationPresent(classOf[RequiredRoles])) {
      //nossa, this jobs really wants some security....
      val token = oidcToken()

      val neededRoles = job.getClass.getAnnotation(classOf[RequiredRoles]).roles()
      if (!rolesSatisfied(token, neededRoles)) {

        val requiredRoles = job.getClass.getAnnotation(classOf[RequiredRoles]).roles()

        log.log(Level.SEVERE, s"Provided subject [${token.subject}] does not posses all of the required roles ${requiredRoles
          .mkString("[", ", ", "]")}")

        throw InsufficientRolesException(requiredRoles)
      }
    }

    performJobSteps(job, jobConfig) match {
      case Success(_) =>
      case Failure(e) =>
        throw JobExecutionException(e)
    }
  }

  private def performJobSteps[TConfig <: JobConfig](job: Job[TConfig], jobConfig: TConfig): Try[Unit] = Try {

    try {
      job.setup(jobConfig)
    } catch {
      case NonFatal(e) =>
        log.log(Level.SEVERE, s"An error setting up Job [${jobConfig.name}] has occurred", e)
        throw e
    }

    try {
      job.initialize(jobConfig)
    } catch {
      case NonFatal(e) =>
        log.log(Level.SEVERE, s"An error initializing Job [${jobConfig.name}] has occurred", e)
        throw e
    }

    try {
      import PrettyPrint._
      log.info(s"-- Starting ${jobConfig.name} with the following configuration --")
      log.info(jobConfig.prettyPrint())

      val t1 = System.currentTimeMillis()

      job.run(jobConfig)

      val t2 = System.currentTimeMillis()
      log.info(s"Job ${jobConfig.name} completed normally in ${t2 - t1} millis")

    } catch {
      case NonFatal(e) =>
        log.log(Level.SEVERE, s"An error starting Job [${jobConfig.name}] has occurred", e)
        throw e
    } finally {
      job.terminate(jobConfig)
    }

  }

}
