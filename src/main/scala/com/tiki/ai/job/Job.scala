package com.tiki.ai.job

trait JobConfig {
  def name: String
}

trait Job[TConfig <: JobConfig] {

  def setup(jobConfig: TConfig): Unit = {}

  def initialize(jobConfig: TConfig): Unit = {}

  /**
    * Main method that executes business logic of the job
    */
  def run(jobConfig: TConfig): Unit

  /**
    * Clean-up method called after the job execution (after run())
    */
  def terminate(jobConfig: TConfig): Unit = {}

}
