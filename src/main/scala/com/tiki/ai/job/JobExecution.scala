package com.tiki.ai.job

import java.io.{File, FileInputStream, FileNotFoundException}
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}
import java.util.logging.{Level, Logger}

import com.tiki.ai.job.JobStatus.JobStatus
import com.tiki.ai.json.EnumerationFormatSupport
import spray.json._

import scala.io.Source

object JobExecution extends DefaultJsonProtocol {

  implicit val modelFormat: JsonFormat[JobExecution] = jsonFormat3(JobExecution.apply)
  @transient private val log = Logger.getLogger(getClass.getName)

  def fromFile(path: String): JobExecution = {
    val executionFile = new File(path)
    if (executionFile.exists() && !executionFile.isDirectory && executionFile.length() > 0) {
      val stream = new FileInputStream(path)
      try {
        Source.fromInputStream(stream).getLines.mkString.parseJson.convertTo[JobExecution]
      } catch {
        case e: Throwable =>
          log.log(Level.SEVERE, s"Can not read property file: $path.", e)
          throw e
      } finally {
        if (stream != null) stream.close()
      }
    } else {
      throw new FileNotFoundException(s"Supplied file: $path was not found.")
    }
  }

  def persist(target : JobExecution, path: String): Unit = {

    new File(path).getParentFile.mkdirs()
    Files.write(
      Paths.get(path),
      asJsonString(target).getBytes(StandardCharsets.UTF_8)
    )
  }

  def asJsonString(target : JobExecution): String = target.toJson.compactPrint


}

/**
  * With this class you can persist or load job state to and from a text file.
  */
case class JobExecution(var executionId: String, var status: JobStatus, var message: String = "") {
}

object JobStatus extends Enumeration {
  type JobStatus = Value
  val  unknown, started, running, finished, failed = Value
  implicit val statusFormat: JsonFormat[JobStatus.Value] = EnumerationFormatSupport.enumFormat(JobStatus)
}
