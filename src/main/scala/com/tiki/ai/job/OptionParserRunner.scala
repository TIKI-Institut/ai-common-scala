package com.tiki.ai.job
import com.tiki.ai.spark.SparkSessionFactory
import scopt.{OParser, OptionParser}

import scala.util.Try

object OptionParserRunner {

  implicit class OptionParserArguments[TConfig <: JobConfig](initialConfig: TConfig) {

    def withArguments(args: Seq[String])(parser: OptionParser[TConfig]): TConfig = validateParsedConfig(args)(
      parser.parse(args, initialConfig)
    )

    def withArguments(args: Seq[String], oParser: OParser[_, TConfig]): TConfig = validateParsedConfig(args)(
      OParser.parse(oParser, args, initialConfig)
    )

    private def validateParsedConfig(args: Seq[String])(parsedConfig: Option[TConfig]) = parsedConfig match {
      case Some(config) => config
      case _ =>
        throw new IllegalArgumentException(
          s"Parsing of arguments [${args.mkString("\"", "; ", "\"")}] has failed, aborting Job: ${initialConfig.name}")
    }
  }
}

trait OptionParserRunner {

  def apply(arguments: Seq[String]): Try[Unit]
}

trait CLIOptionParserRunner[TConfig <: JobConfig] {
  self: Job[TConfig] =>

  def parse(arguments: Seq[String]): TConfig


  def execute(arguments: Seq[String])(implicit sparkSessionFactory: Option[SparkSessionFactory] = None): Try[Unit] =
    JobRunner.executeJob(self, self.parse(arguments))
}
