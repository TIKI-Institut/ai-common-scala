package com.tiki.ai.qos

import java.util.logging.Logger

import akka.http.scaladsl.model.{ContentTypes, HttpResponse}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.tiki.ai.qos.model._
import spray.json._

import scala.util.{Failure, Success, Try}

/**
  * Routes which handles qos requests.
  *
  * Currently there are two qos endpoints defined:
  *
  *   qos/run - execute sync qos run, this endpoint is invoked by ai-qos-manager, which run in sidecar container of current web deployment.
  *   qos/list - list all declared sync entries, this endpoint is invoked also by ai-qos-manager
  */
trait QosRoutes extends SyncEntriesScanner with QosJsonSupport {

  @transient private val log = Logger.getLogger(getClass.getName)

  val endpoint: QosEndpoint =
    QosEndpoint(syncEntries.map(_.entry), FlavorReference.currentFlavor(), PodReference.currentPod())

  def qosRoutes: Route =
    path("qos" / "run") {
      get {
        parameters("transactionId", "name") { (transactionId, name) =>
          {
            Try(transactionId.toInt) match {
              case Failure(_) =>
                val msg = s"Invalid transactionId: $transactionId"
                log.severe(msg)
                complete(HttpResponse(400).withEntity(msg))
              case Success(transactionId) =>
                complete(
                  HttpResponse(200).withEntity(ContentTypes.`application/json`, run(transactionId, name).toJson.prettyPrint))
            }
          }
        }
      }
    } ~
      path("qos" / "list") {
        get {
          complete({
            HttpResponse(200).withEntity(ContentTypes.`application/json`, endpoint.toJson.prettyPrint)
          })
        }
      }

  private def run(transactionId: Int, entryName: String): QosRunResult = {
    syncEntries.find(_.name == entryName) match {
      // There is entry with name entryName
      case Some(entry) =>
        Try(entry.run()) match {
          // The execution of the job is failed
          case Failure(exception) =>
            log.severe(s"Running sync QoS for id $transactionId, name $entryName has failed. ${exception.getMessage}")
            QosRunResult.failure(transactionId, entryName, entry)
          // The execution is successful
          case Success(value) => QosRunResult.success(transactionId, entryName, entry, value)
        }
      // There is no entry with the name entryName in this project
      case None =>
        log.warning(s"Sync QoS entry '$entryName' was not found")
        QosRunResult.notFound(transactionId, entryName)
    }
  }

}
