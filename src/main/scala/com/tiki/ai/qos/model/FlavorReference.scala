package com.tiki.ai.qos.model

import com.tiki.ai.config.CommonLibConfig

case class FlavorReference(name: String, principal: String, environment: String, flavorName: String, flavorVersion: String) {}

object FlavorReference {
  def currentFlavor(): FlavorReference = new FlavorReference(
    name = CommonLibConfig.AuthConfig.dspName,
    environment = CommonLibConfig.AuthConfig.dspEnvironment,
    flavorName = CommonLibConfig.AuthConfig.dspFlavor,
    principal = CommonLibConfig.AuthConfig.dspPrincipal,
    flavorVersion = CommonLibConfig.AuthConfig.dspFlavorVersion
  )
}
