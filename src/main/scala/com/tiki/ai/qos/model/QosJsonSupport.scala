package com.tiki.ai.qos.model

import com.tiki.ai.json.EnumerationFormatSupport
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormatter, ISODateTimeFormat}
import spray.json._

/**
  * JSON converter for joda DateTime.
  */
trait CommonJsonFormatSupport {

  implicit object DateJsonFormat extends RootJsonFormat[DateTime] {

    private val parserISO: DateTimeFormatter = ISODateTimeFormat.dateTimeNoMillis()

    override def write(obj: DateTime): JsValue = JsString(parserISO.print(obj))

    override def read(json: JsValue): DateTime = json match {
      case JsString(s) => parserISO.parseDateTime(s)
      case _           => throw DeserializationException("Unable to deserialize DateTime from json")
    }
  }

  implicit val qosRunStatusJsonFormat: RootJsonFormat[QosRunStatus.Value] = EnumerationFormatSupport.enumFormat(QosRunStatus)
}

/**
  * JSON converter for SyncQosEntry.
  */
trait QosEntryWrites extends DefaultJsonProtocol {

  implicit object QosEntryFormat extends RootJsonFormat[QosEntry] {
    override def write(obj: QosEntry): JsValue = JsObject(Map("name" -> JsString(obj.name), "type" -> JsString(obj.entryType)))
    override def read(json: JsValue): QosEntry = QosEntry (fromField[String](json, "name"),fromField[String](json, "type"))
  }
}

trait PodReferenceWrites extends CommonJsonFormatSupport with DefaultJsonProtocol {
  implicit val podWrites: RootJsonFormat[PodReference] = jsonFormat2(PodReference.apply)
}

/**
  * JSON converter for FlavorReference.
  */
trait FlavorReferenceWrites extends DefaultJsonProtocol {
  implicit val flavorWrites: RootJsonFormat[FlavorReference] = jsonFormat5(FlavorReference.apply)
}

trait QosRunResultWrites
    extends CommonJsonFormatSupport
    with FlavorReferenceWrites
    with QosEntryWrites
    with PodReferenceWrites
    with DefaultJsonProtocol {
  implicit val qosRunResultFormat: RootJsonFormat[QosRunResult] = jsonFormat8(QosRunResult.apply)

}

/**
  * JSON converter for SyncQosEndpoint.
  */
trait QosEndpointWrites extends QosEntryWrites with FlavorReferenceWrites with PodReferenceWrites {
  implicit val syncQosEndpointWrites: RootJsonFormat[QosEndpoint] = jsonFormat5(QosEndpoint.apply)
}

trait QosJsonSupport
    extends QosRunResultWrites
    with QosEntryWrites
    with QosEndpointWrites
    with PodReferenceWrites
    with CommonJsonFormatSupport
    with FlavorReferenceWrites
