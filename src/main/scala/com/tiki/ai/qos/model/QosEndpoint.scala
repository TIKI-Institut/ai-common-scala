package com.tiki.ai.qos.model

import org.joda.time.DateTime

case class QosEndpoint(entries: List[QosEntry],
                           flavorReference: FlavorReference,
                           podReference: Option[PodReference],
                           initialBeat: Option[DateTime],
                           lastBeat: Option[DateTime])

object QosEndpoint {
  def apply(entries: List[QosEntry], flavorReference: FlavorReference, podReference: Option[PodReference]): QosEndpoint =
    new QosEndpoint(entries, flavorReference, podReference, Some(DateTime.now()), Some(DateTime.now()))

  def apply(entries: List[QosEntry], flavorReference: FlavorReference): QosEndpoint =
    new QosEndpoint(entries, flavorReference, None, None, None)
}
