package com.tiki.ai.qos.model

case class QosEntry(name: String, entryType: String)

trait QosEntryImpl {
  def entry : QosEntry = QosEntry(name, entryType)

  def name: String
  def entryType: String

  def run(): Double
}

trait AsyncQosEntry extends QosEntryImpl {
  val entryType = "Async"
}

trait SyncQosEntry extends QosEntryImpl {
  val entryType = "Sync"
}
