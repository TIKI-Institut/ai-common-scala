package com.tiki.ai.qos.model

import com.tiki.ai.config.CommonLibConfig

case class PodReference(namespace: String, pod: String)

object PodReference {
  def currentPod(): Option[PodReference] = {
    if (CommonLibConfig.AuthConfig.dspPod == "") {
      return None
    }
    Some(
      new PodReference(
        namespace = CommonLibConfig.AuthConfig.dspNamespace,
        pod = CommonLibConfig.AuthConfig.dspPod
      ))
  }
}
