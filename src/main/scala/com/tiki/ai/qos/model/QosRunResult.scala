package com.tiki.ai.qos.model

import com.tiki.ai.json.EnumerationFormatSupport
import org.joda.time.DateTime
import spray.json.JsonFormat

object QosRunStatus extends Enumeration {
  type QosRunStatus = Value
  val Success, Failed, NotFound, Running = Value
  implicit val statusFormat: JsonFormat[QosRunStatus.Value] = EnumerationFormatSupport.enumFormat(QosRunStatus)
}

class QosRunStatus extends Enumeration {
  val Success  = 0
  val Failed   = 1
  val NotFound = 2
  val Running  = 3
}

case class QosRunResult(id: Int,
                            producedOn: DateTime,
                            status: QosRunStatus.Value,
                            statusMessage: String,
                            score: Option[Double],
                            flavorReference: FlavorReference,
                            podReference: Option[PodReference],
                            qosEntry: Option[QosEntry])

object QosRunResult {

  def failure(transactionId: Int, entryName: String, entry: QosEntryImpl): QosRunResult = QosRunResult(
    transactionId,
    DateTime.now(),
    QosRunStatus.Failed,
    s"Running QoS for id $transactionId, name $entryName has failed",
    None,
    FlavorReference.currentFlavor(),
    PodReference.currentPod(),
    Some(entry.entry)
  )

  def success(transactionId: Int, entryName: String, entry: QosEntryImpl, value: Double): QosRunResult =
    QosRunResult(
      transactionId,
      DateTime.now(),
      QosRunStatus.Success,
      s"QoS run for $entryName completed",
      Some(value),
      FlavorReference.currentFlavor(),
      PodReference.currentPod(),
      Some(entry.entry)
    )

  def notFound(transactionId: Int, entryName: String): QosRunResult = QosRunResult(
    transactionId,
    DateTime.now(),
    QosRunStatus.NotFound,
    s"QoS entry $entryName was not found",
    None,
    FlavorReference.currentFlavor(),
    PodReference.currentPod(),
    None
  )

}
