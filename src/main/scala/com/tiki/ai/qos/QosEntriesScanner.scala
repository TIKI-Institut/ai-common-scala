package com.tiki.ai.qos

import com.tiki.ai.dsp.SymbolProcessor
import com.tiki.ai.qos.model.{AsyncQosEntry, SyncQosEntry}

import java.util.logging.Logger
import scala.collection.JavaConverters._
import scala.util.{Failure, Success}

trait QosEntriesScanner[T] {

  @transient private val log = Logger.getLogger(getClass.getName)

  def collectEntries(canonicalName: String): List[T] = SymbolProcessor.resources {
    case Success(scanner) =>
      val entries = scanner
        .getClassesImplementing(canonicalName)
        .asScala
        .toList
        .filter(!_.isAbstract)
        .map(_.loadClass().getDeclaredConstructor().newInstance().asInstanceOf[T])
      scanner.close()
      entries
    case Failure(exception) =>
      log.severe("Unexpect error during collecting entries")
      exception.printStackTrace()
      List()

  }
}

trait AsyncEntriesScanner extends QosEntriesScanner[AsyncQosEntry] {
  lazy val asyncEntries: List[AsyncQosEntry] = collectEntries(classOf[AsyncQosEntry].getCanonicalName)
}

trait SyncEntriesScanner extends QosEntriesScanner[SyncQosEntry] {
  lazy val syncEntries: List[SyncQosEntry] = collectEntries(classOf[SyncQosEntry].getCanonicalName)
}
