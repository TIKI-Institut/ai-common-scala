package com.tiki.ai.qos.cmd

import java.util.logging.Logger
import akka.stream.alpakka.mqtt.MqttQoS
import com.tiki.ai.job.JobConfig
import com.tiki.ai.mqtt.{MqttAkkaContext, MqttClient, MqttClientImpl, MqttTopics}
import com.tiki.ai.qos.AsyncEntriesScanner
import com.tiki.ai.qos.cmd.QosRegisterCommand._
import com.tiki.ai.qos.model.{FlavorReference, PodReference, QosEndpoint, QosJsonSupport}
import com.tiki.ai.sys.Exit
import scopt.OptionParser
import spray.json._

import java.util.logging.Logger
import scala.util.{Failure, Success}
sealed case class RegisterInterceptor(name: String = "", register: Boolean = false) extends JobConfig

/**
  *  This command is called by registration job (ends with '-qos-reg' suffix), which is created during flavor registration (in DSP during 'provision' step of ai-provisioner)
  */
class QosRegisterCommand extends MqttAkkaContext with QosJsonSupport with AsyncEntriesScanner {

  @transient private val log      = Logger.getLogger(getClass.getName)
  val exit: Exit                  = Exit
  lazy val mqttClient: MqttClient = new MqttClientImpl(s"${PodReference.currentPod.get.pod}-register-qos")

  def qosRegister(args: Array[String], autoExit: Boolean = true): Unit = {

    val interceptConfig = RegisterInterceptor()

    registerInterceptParser.parse(args, interceptConfig) match {
      case Some(config) if config.register =>
        asyncEntries.foreach(entry => log.info(s"entry: $entry"))
        val endpoint = QosEndpoint(asyncEntries.map(_.entry), FlavorReference.currentFlavor())
        mqttClient.publishMessage(endpoint.toJson.compactPrint, MqttTopics.QosRegisterAsync, MqttQoS.AtLeastOnce) onComplete {
          case Success(_) =>
            log.info("Register message successfully send")
            exit.exit(Exit.NORMAL_TERMINATION)

          case Failure(ex) =>
            log.severe(s"Error on sending register message. Reason: ${ex.getMessage}")
            exit.exit(Exit.ABNORMAL_TERMINATION)
        }

      case Some(_) => log.warning("Config was provided but register command was set to false. Skip registering")
      case _       => log.warning("No register configuration provided. Skip registering")
    }
  }

}

object QosRegisterCommand {
  private val registerInterceptParser = new OptionParser[RegisterInterceptor]("") {
    override def errorOnUnknownArgument            = false
    override def showUsageOnError: Option[Boolean] = Some(false)
    override def reportWarning(msg: String): Unit  = { /* nnoop */ }

    cmd("qos-register")
      .action((_, c) => {
        c.copy(register = true)
      })
      .text("Registers all async QoS endpoints. Executed during 'provision' step of ai-provisioner")
  }
}
