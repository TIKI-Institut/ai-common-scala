package com.tiki.ai.qos.cmd

import akka.Done
import akka.stream.alpakka.mqtt.MqttQoS
import com.tiki.ai.job.JobConfig
import com.tiki.ai.mqtt.{MqttAkkaContext, MqttClient, MqttClientImpl, MqttTopics}
import com.tiki.ai.qos.AsyncEntriesScanner
import com.tiki.ai.qos.cmd.AsyncQosJobCommand._
import com.tiki.ai.qos.model._
import com.tiki.ai.sys.Exit
import scopt.OptionParser
import spray.json._

import java.util.logging.Logger
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success, Try}

sealed case class RunInterceptor(name: String = "",
                                 run: Boolean = false,
                                 transactionId: Option[Int] = None,
                                 entryName: Option[String] = None)
    extends JobConfig

/**
  * This qos-run command is called by ai-qos-manager (in infrastructure namespace) after the user trigger the qos job in the web-ui.
  */
class AsyncQosJobCommand extends MqttAkkaContext with QosJsonSupport with AsyncEntriesScanner {

  protected lazy val mqttClient: MqttClient = new MqttClientImpl(s"${PodReference.currentPod().get.pod}-qos-run")
  protected val exit: Exit                  = Exit
  @transient private val log                = Logger.getLogger(getClass.getName)

  def run(args: Array[String], autoExit: Boolean = true): Unit = {

    runInterceptorOptionParser.parse(args, RunInterceptor()) match {
      case Some(config) if config.run =>
        val transactionId: Int = parseTransactionIdArg(args)
        val entryName: String  = parseEntryNameArg(args)

        asyncEntries.find(_.name == entryName) match {
          // There is entry with name entryName
          case Some(entry) =>
            val result = Try(entry.run()) match {
              // The execution of the job is failed
              case Failure(exception) =>
                log.severe {
                  s"Running async QoS for id $transactionId, name $entryName has failed. ${exception.getMessage}"
                }
                QosRunResult.failure(transactionId, entryName, entry)
              // The execution is successful
              case Success(value) =>
                log.info(s"Run of Async QoS entry '$entryName' was successfully with score [$value]")
                QosRunResult.success(transactionId, entryName, entry, value)
            }
            publishResult(result)
          // There is no entry with the name entryName in this project
          case None =>
            log.warning(s"Async QoS entry '$entryName' was not found")
            val notFoundResult = QosRunResult.notFound(transactionId, entryName)
            publishResult(notFoundResult)
        }

        exit.exit(0)

      case Some(_) => log.warning("Config was provided but run command was set to false. Skip running")
      case _       => log.warning("No run configuration provided. Skip running")
    }
  }

  // TODO replace this indices with named parameters. This must be changed also in the ai-qos-manager project
  //  and other ai-common projects, ie. ai-common-dotnetcore
  private def parseEntryNameArg(args: Array[String]) =
    Try(args(2))
      .recover {
        case ex =>
          log.severe(s"Error on resolving entry name param: Error: ${ex.getMessage}")
          throw ex
      }
      .getOrElse {
        exit.exit(Exit.ABNORMAL_TERMINATION)
        ""
      }

  // TODO replace this indices with named parameters. This must be changed also in the ai-qos-manager project
  //  and other ai-common projects, ie. ai-common-dotnetcore
  private def parseTransactionIdArg(args: Array[String]) =
    Try(args(1).toInt)
      .recover {
        case ex =>
          log.severe(s"Error on resolving transaction id: Error: ${ex.getMessage}")
          throw ex
      }
      .getOrElse {
        exit.exit(Exit.ABNORMAL_TERMINATION)
        0
      }

  private def publishResult(result: QosRunResult): Unit = {

    val publishFuture: Future[Done] =
      mqttClient.publishMessage(result.toJson.compactPrint, MqttTopics.QosResultAsync, MqttQoS.AtLeastOnce)
    Await.ready(publishFuture, mqttClient.defaultTimeout)
  }
}

object AsyncQosJobCommand {
  private val runInterceptorOptionParser: OptionParser[RunInterceptor] = new OptionParser[RunInterceptor]("") {
    override def errorOnUnknownArgument = false

    override def showUsageOnError: Option[Boolean] = Some(false)

    override def reportWarning(msg: String): Unit = {
      /* nnoop */
    }

    cmd("qos-run")
      .action((_, c) => {
        c.copy(run = true)
      })
      .text("Runs a previously declared async QoS endpoint.")
  }
}
