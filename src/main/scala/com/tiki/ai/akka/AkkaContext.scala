package com.tiki.ai.akka

import akka.actor.ActorSystem

import scala.concurrent.ExecutionContext

/***
  * The base akka context definition which can be extended by concrete services.
  *
  * ATTENTION, when overriding implicits do not use `val` but use `def` or there can be `NPE`s!
  */
trait AkkaContext {
  implicit def system: ActorSystem
  implicit def ec: ExecutionContext
}
