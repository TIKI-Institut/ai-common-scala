package com.tiki.ai.akka

import java.util.logging.Logger

import akka.actor.{Actor, ActorRef}
import akka.event.LoggingReceive
import com.tiki.ai.akka.BroadcasterActor.{DeregisterListener, ListenerDeRegistered, ListenerRegistered, RegisterListener}
import com.tiki.ai.akka.BroadcasterAware.{InitializationDone, Initialize}

import scala.collection.mutable.ArrayBuffer

/**
  * Object is a general purpose messaging system.
  */
object BroadcasterActor {

  /**
    * Messages used by listeners to register and un-register themselves
    */
  case class RegisterListener(listener: ActorRef)

  case class DeregisterListener(listener: ActorRef)

  case class BroadcasterActorException(message: String, e: Throwable = null) extends Exception(message, e)

  case object ListenerRegistered

  case object ListenerDeRegistered
}

trait BroadcasterActor { this: Actor =>

  val listeners: ArrayBuffer[ActorRef] = ArrayBuffer.empty[ActorRef]

  /**
    * Sends the event to all of our listeners
    */
  def sendEvent[T](event: T): Unit = {
    listeners foreach (_ ! event)
  }

  /**
    * We create a specific partial function to handle the messages for
    * our event listener. Anything that mixes in our trait will need to
    * compose this receiver
    */
  def broadcasterReceive: Receive = LoggingReceive {
    case RegisterListener(listener) =>
      listeners += listener
      listener ! ListenerRegistered
    case DeregisterListener(listener) =>
      listeners -= listener
      listener ! ListenerDeRegistered
  }
}

/**
  * Object part of general messaging service
  */
object BroadcasterAware {
  case object Initialize

  case class InitializationDone()
}

trait BroadcasterAware { this: Actor =>

  @transient private val log = Logger.getLogger(getClass.getName)

  def broadcaster: ActorRef
  private var owner: ActorRef = _

  def broadcasterAwareReceive: Receive

  override def postStop(): Unit = {
    broadcaster ! DeregisterListener(self)
  }

  /**
    * Method defines behaviour on receipt of messages (ListeneerRegistered, Initialize).
    *
    * @return returns an object of type Receive that informs owner/broadcaster of message receipt.
    */
  final def receive: Receive = LoggingReceive {
    case ListenerRegistered =>
      log.finer(s"Successfully registered update actor $this with owner $owner and broadcaster $broadcaster .")
      context.become(broadcasterAwareReceive)
      if (owner != null) owner ! InitializationDone()
    case Initialize =>
      owner = sender
      log.finer(s"Registering $this update actor with owner $owner and broadcaster $broadcaster ...")
      broadcaster ! RegisterListener(self)
  }
}
