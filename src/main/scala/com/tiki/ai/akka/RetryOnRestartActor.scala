package com.tiki.ai.akka

import java.util.logging.Logger

import akka.actor.{Actor, ActorContext, ActorRef, ChildRestartStats, OneForOneStrategy, PossiblyHarmful, SupervisorStrategy}

import scala.concurrent.duration.Duration

/**
  * Normal akka behavior would be to not retain the message where the failure
  * occurred while restarting. But RestartableActor will do it.
  * Note: Use this actor only on leaf actors which just process
  * messages and don't do any supervision and/or coordination,
  * which also have a supervisor on parent level, otherwise this
  * can produce an infinite loop, cause default behavior is to
  * restart failing actors infinite.
  */
trait RetryOnRestartActor extends Actor {

  @transient private val log = Logger.getLogger(getClass.getName)

  override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
    log.finer(s"Restart of actor ${self.path} with message '${message.getOrElse("")}'")

    // do not retry any system messages derived from PossiblyHarmful
    // and it is important to use forward here, we want to retain the original sender
    message.filterNot(_.isInstanceOf[PossiblyHarmful]).foreach(self.forward)

    super.preRestart(reason, message)
  }
}

/**
  * Creates a supervisor strategy especially useful for actors which inherits RetryOnRestartActor and are within an actor pool.
  */
object RetryOnRestartForPoolsStrategy {
  // copied from SupervisorStrategy
  private def withinTimeRangeOption(withinTimeRange: Duration): Option[Duration] =
    if (withinTimeRange.isFinite && withinTimeRange >= Duration.Zero) Some(withinTimeRange)
    else None

  // copied from SupervisorStrategy
  private def maxNrOfRetriesOption(maxNrOfRetries: Int): Option[Int] =
    if (maxNrOfRetries < 0) None else Some(maxNrOfRetries)

  def apply(maxNrOfRetries: Int = -1, withinTimeRange: Duration = Duration.Inf, loggingEnabled: Boolean = true)(
      decider: SupervisorStrategy.Decider) =
    new OneForOneStrategy(maxNrOfRetries, withinTimeRange, loggingEnabled)(decider) with RetryOnRestartForPoolsStrategy
}

/**
  * This trait enhances the default akka OneForOneStrategy.
  * Instead of stopping the actor it restarts the actor with an additional suspend.
  * This is especially useful when using RetryOnRestartActor within an actor pool.
  */
trait RetryOnRestartForPoolsStrategy { self: OneForOneStrategy =>

  // copied from OneForOneStrategy
  private val retriesWindow =
    (RetryOnRestartForPoolsStrategy.maxNrOfRetriesOption(maxNrOfRetries),
     RetryOnRestartForPoolsStrategy.withinTimeRangeOption(withinTimeRange).map(_.toMillis.toInt))

  override def processFailure(context: ActorContext,
                              restart: Boolean,
                              child: ActorRef,
                              cause: Throwable,
                              stats: ChildRestartStats,
                              children: Iterable[ChildRestartStats]): Unit =
    if (restart && stats.requestRestartPermission(retriesWindow))
      restartChild(child, cause, suspendFirst = false)
    else {
      // 'suspend = true' seems to restart the actor but ignores the last message reconstructed via RetryOnRestartActor
      restartChild(child, cause, suspendFirst = true)
    }
}
