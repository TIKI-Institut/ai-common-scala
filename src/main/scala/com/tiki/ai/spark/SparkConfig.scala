package com.tiki.ai.spark

import com.tiki.ai.config.ConfigHolder

trait SparkConfig { self: ConfigHolder =>

  object SparkConfigKeys {

    /* main fs config block */
    val SPARK_BLOCK = "spark"

    // seems obsolete :  ....  as the default Spark SQL Hive Warehouse location. Which we dont use
    //val SQL_WAREHOUSE_DIR = "sqlWarehouseDir"

    val SERIALIZER_IMPL_KEY    = "serializerImpl"
    val JARS_TO_DISTRIBUTE_KEY = "jarsToDistribute"
  }

  object SparkConfig {
    private lazy val sparkBlock = config.getOptionalConfig(SparkConfigKeys.SPARK_BLOCK)

    lazy val directoryWithJarsToDistribute: Option[String] =
      sparkBlock.flatMap(_.getOptionalString(SparkConfigKeys.JARS_TO_DISTRIBUTE_KEY))

    // seems obsolete :  ....  as the default Spark SQL Hive Warehouse location. Which we dont use
    //lazy val warehouseDir: String = sparkBlock.map(_.getString(SparkConfigKeys.SQL_WAREHOUSE_DIR)).orNull
    lazy val serializerImpl: Option[String] = sparkBlock.map(_.getString(SparkConfigKeys.SERIALIZER_IMPL_KEY))
  }
}
