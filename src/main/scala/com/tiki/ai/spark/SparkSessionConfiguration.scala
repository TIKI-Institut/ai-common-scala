package com.tiki.ai.spark

import com.tiki.ai.config.CommonLibConfig
import org.apache.spark.SparkConf

import scala.io.Source

object SparkSessionConfiguration {

  val envSparkDefaultsFile = "SPARK_DEFAULTS"

  val defaultSparkConfiguration: SparkConf => Unit = { sparkConf =>
    sparkConf
      .set("spark.hadoop.validateOutputSpecs", "false")
      .set("spark.kryo.referenceTracking", "false")
      .set("spark.sql.session.timeZone", "UTC")

    CommonLibConfig.SparkConfig.serializerImpl.foreach(sparkConf.set("spark.serializer", _))

    readSparkDefaults(sparkConf)
  }

  val runSparkLocally: SparkConf => Unit = { sparkConf =>
    sparkConf.set("spark.master", "local")
    sparkConf.set("spark.ui.enabled", "true")
  }

  private def filterLine(s: String): Boolean = {
    val trimmed = s.trim
    trimmed.nonEmpty && !trimmed.startsWith("#")
  }

  private def splitFileLine(s: String): (String, String) = {
    val splits = s.split("\\s+")
    splits(0) -> splits(1)
  }

  /***
    *
    * @param sparkConfig [[org.apache.spark.SparkConf]] which gets enhanced by all the settings stored in a file referenced by the
    *                    value of the ENV_VARIABLE "SPARK_DEFAULTS"
    */
  def readSparkDefaults(sparkConfig: SparkConf): Unit = {
    if (sys.env.contains(envSparkDefaultsFile)) {
      val defaultsFile = sys.env(envSparkDefaultsFile)

      for (line <- Source.fromFile(defaultsFile).getLines.filter(filterLine)) {
        val splits = splitFileLine(line)
        sparkConfig.set(splits._1, splits._2)
      }
    }
  }

}
