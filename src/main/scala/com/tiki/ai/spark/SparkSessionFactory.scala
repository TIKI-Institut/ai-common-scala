package com.tiki.ai.spark

import com.tiki.ai.config.CommonLibConfig
import com.tiki.ai.spark.config._
import org.apache.hadoop.conf.Configuration
import org.apache.spark.sql.SparkSession
import org.apache.spark.{SparkConf, SparkContext}
import java.io.File

import com.tiki.ai.hadoop.{HadoopConfigHolder, HadoopConfigSupport}

class SharedSparkSessionFactory extends SparkSessionFactory {
  //this session factory will not stop when the spark app terminates
  override def stopSession(): Unit = {}
}

class SparkSessionFactory {

  private[this] var sparkSession : Option[SparkSession] = None

  def currentSession(): Option[SparkSession] = sparkSession

  def createSession( configs : SparkSessionConfigSupport with HadoopConfigSupport ): SparkSession = {
    import scala.collection.JavaConverters._

    //create new spark config (empty)
    val sparkConfig: SparkConf = new SparkConf()

    //prefill empty config with default
    SparkSessionConfiguration.defaultSparkConfiguration(sparkConfig)

    //take user configs for spark
    configs.applySparkConfig(sparkConfig)

    //create new hadoop config (empty) for transfer hadoop configs into the spark config
    val hadoopConfig = new Configuration()
    //take user configs for hadoop
    configs.applyHadoopConfig(hadoopConfig)

    //transfer user hadoop configs into spark session config
    hadoopConfig.asScala.foreach { entry =>
      sparkConfig.set(s"spark.hadoop.${entry.getKey}", entry.getValue)
    }

    val session = SparkSession.builder().config(sparkConfig).getOrCreate()

    SparkSessionFactory.distributeJars(session.sparkContext)

    sparkSession = Some(session)
    session
  }

  def stopSession() : Unit = {
    sparkSession.foreach { _.stop()}
  }
}




object SparkSessionFactory {

  /***
    *
    * @param sc SparkContext which is used tu distribute all additional JARs from the given directory
    * @param localJarDirectory the local (and relative) directory containing all JARS for the spark-job (without HADOOP and SPARK jars)
    */
  def distributeJars(sc: SparkContext, localJarDirectory: String): Unit = {
    listJars(localJarDirectory).foreach(sc.addJar)
  }

  /***
    * Distributes all JARs based on configuration....
    * @param sc SparkContext which is used tu distribute all additional JARs from the given directory
    */
  def distributeJars(sc: SparkContext): Unit = {
    CommonLibConfig.SparkConfig.directoryWithJarsToDistribute.foreach(distributeJars(sc, _))
  }

  /***
    *
    * @param jarsDirectory the local (and relative) directory containing all JARS for the spark-job (without HADOOP and SPARK jars)
    * @return a list of absolute paths of all .JAR files withing the given directory
    */
  private def listJars(jarsDirectory: String): Seq[String] = {
    //strange name; but true!
    val cwd = sys.props.get("user.dir")

    cwd match {
      case Some(dir) =>
        val d = new File(dir, jarsDirectory)
        if (d.exists && d.isDirectory) {
          d.listFiles.filter(f => f.isFile && f.getName.toLowerCase.endsWith(".jar")).map(_.getAbsolutePath)
        } else {
          Seq.empty[String]
        }

      case _ => Seq.empty[String]
    }
  }
}
