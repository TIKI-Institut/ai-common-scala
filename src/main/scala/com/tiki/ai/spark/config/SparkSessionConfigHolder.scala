package com.tiki.ai.spark.config

import org.apache.spark.SparkConf

import scala.collection.{Seq, mutable}

trait SparkSessionConfigHolder extends SparkSessionConfigSupport {

  lazy val sparkConfigUpdates : mutable.Builder[SparkConf => Unit,Seq[SparkConf => Unit]] = Seq.newBuilder
  lazy val managedSparkSettings : mutable.Map[String, Int] = mutable.Map.empty

  def sparkSessionConfiguration: SparkConf = {
    val c = new SparkConf()
    applySparkConfig(c)
    c
  }

}
