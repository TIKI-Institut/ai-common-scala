package com.tiki.ai.spark.config

import com.tiki.ai.spark.SparkConfig
import org.apache.spark.SparkConf

import scala.collection.{Seq, mutable}

trait SparkSessionConfigSupport {
  import SparkSessionConfigSupport._

  def sparkConfigUpdates: mutable.Builder[SparkConf => Unit, Seq[SparkConf => Unit]]
  def managedSparkSettings: mutable.Map[String, Int]

  /***
    *
    * Apply the different config flavors to the config object.
    * This can be done multiple times to a [[SparkConf]] object
    *
    * @param conf the [[SparkConf]] object which needs to be altered
    */
  def applySparkConfig(conf: SparkConf): Unit = {
    sparkConfigUpdates.result().foreach(_(conf))
    managedSessionConfigsToSparkConf(conf)
  }

  /***
    *
    * Collects all flavors of Spark config.
    *
    * @param cb a callback to manipulate the Spark configuration
    */
  def updateSparkConfig(cb: SparkConf => Unit): Unit = sparkConfigUpdates.+=(cb)

  /***
    *
    * set the name of this spark application
    *
    * @param  name name
    */
  def appName(name: String): Unit = updateSparkConfig { conf =>
    conf.set(SparkAppNameName, name)
  }

  /***
    *
    * spark master
    *
    * @param  master url of the spark master to connect to (might be "local" if you want to use your local machine)
    */
  def sparkMaster(master: String): Unit = updateSparkConfig { conf =>
    conf.set(SparkMasterConfigName, master)
  }

  /***
    *
    * enables local debug ui (port 4040)
    */
  def enabledUI(): Unit = updateSparkConfig { conf =>
    conf.set(SparkUiConfigName, "true")
  }

  /***
    *
    * set the number of executors (only if master is not 'local')
    *
    * @param  i number of executors to be created
    */
  def executorCount(i: Int): Unit = {
    updateSparkConfig { conf =>
      conf.set(SparkExecutorCountConfigName, s"$i")
    }
    updateSparkConfig { conf =>
      conf.set(SparkDynamicAllocationMaxExecutorsName, s"$i")
    }
  }

  /***
    *
    * set a dynamic count of executors (only if master is not 'local')
    *
    * @param  min number of minimum executors to be created
    * @param  max number of minimum executors to be created
    */
  def dynamicExecutorAllocation(min: Int, max: Int): Unit = {
    updateSparkConfig { conf =>
      conf.set(SparkDynamicAllocationEnabledName, "true")
    }
    updateSparkConfig { conf =>
      conf.set(SparkDynamicAllocationMinExecutorsName, s"$min")
    }
    updateSparkConfig { conf =>
      conf.set(SparkDynamicAllocationMaxExecutorsName, s"$max")
    }
  }

  /***
    *
    * disable dynamic allocation for spark application
    *
    */
  def disableDynamicAllocation(): Unit = updateSparkConfig { conf =>
    conf.set(SparkDynamicAllocationEnabledName, "false")
  }

  def localSparkMaster(): Unit = sparkMaster("local[*]")

  private[this] def updateToMaxValue(key: String, newValue: Int): Unit = {
    managedSparkSettings(key) = managedSparkSettings.get(key) match {
      case None                                  => newValue
      case Some(oldValue) if oldValue < newValue => newValue
      case Some(oldValue)                        => oldValue
    }
  }

  /**
    *
    * @param i executor memory in Megabytes (i.e. <512> for 512m RAM; <8192> for 8G RAM)
    */
  def executorMemory(i: Int): Unit = updateToMaxValue(SparkExecutorMemoryConfigName, i)

  /**
    *
    * @param i executor CPU count
    */
  def executorCores(i: Int): Unit = updateToMaxValue(SparkExecutorCoresConfigName, i)

  /**
    * @param i  default parallelism used for partitioning tasks
    *           will be used as fixed scale on (executorCount * executorCore)
    *
    *           i.e. a "defaultParallelism" of 4 with executorCount=10 and executorCore=5
    *           results in a 'spark.default.parallelism'=200 setting (4 * 5 * 10)
    *
    *           defaults to 4
    */
  def defaultParallelism(i: Int): Unit = updateToMaxValue(SparkDefaultParallelismConfigName, i)

  /**
    * @param i  default parallelism used for "shuffling" tasks (like joins)
    *           will be used as fixed scale on (executorCount * executorCore)
    *
    *           i.e. a "shuffleParallelism" of 40 with executorCount=10 and executorCore=5
    *           results in a 'spark.sql.shuffle.partitions'=2000 setting (40 * 5 * 10)
    *
    *           defaults to 16
    */
  def shuffleParallelism(i: Int): Unit = updateToMaxValue(SparkShuffleParallelismConfigName, i)

  /**
    *
    * @param i maximum driver result size in Megabytes (i.e. <512> for 512m; <8192> for 8G)
    */
  def driverResultSize(i: Int): Unit = updateToMaxValue(SparkDriverMaxResultSizeConfigName, i)

  /***
    *
    * Apply the different config flavors to the spark session builder.
    * This is done __once__ just before the final configuration (and the spark session) is created
    *
    * @param conf the [[SparkConfig]] object which needs to be altered
    */
  def managedSessionConfigsToSparkConf(conf: SparkConf): Unit = {

    managedSparkSettings.foreach {
      case (SparkExecutorMemoryConfigName, value)      => conf.set(SparkExecutorMemoryConfigName, s"${value.toString}m")
      case (SparkExecutorCoresConfigName, value)       => conf.set(SparkExecutorCoresConfigName, value.toString)
      case (SparkDefaultParallelismConfigName, value)  => conf.set(SparkDefaultParallelismConfigName, value.toString)
      case (SparkShuffleParallelismConfigName, value)  => conf.set(SparkShuffleParallelismConfigName, value.toString)
      case (SparkDriverMaxResultSizeConfigName, value) => conf.set(SparkDriverMaxResultSizeConfigName, s"${value.toString}m")
      case (key, value)                                => throw new IllegalArgumentException(s"key '$key' with value '$value' is not supported")
    }
  }

}

object SparkSessionConfigSupport {

  //used spark master (in k8s cluster this is the cluster-internal-url of the driver)
  val SparkMasterConfigName = "spark.master"

  //enable / disable spark UI
  val SparkUiConfigName = "spark.ui.enabled"

  //name of the spark application
  val SparkAppNameName = "spark.app.name"

  //number of spawned spark workers
  val SparkExecutorCountConfigName = "spark.executor.instances"

  //number of min spawned spark workers for dynamic allocation
  val SparkDynamicAllocationMinExecutorsName = "spark.dynamicAllocation.minExecutors"
  //number of max spawned spark workers for dynamic allocation
  val SparkDynamicAllocationMaxExecutorsName = "spark.dynamicAllocation.maxExecutors"

  // enable/disable dynamic allocation
  val SparkDynamicAllocationEnabledName = "spark.dynamicAllocation.enabled"

  //default parallelism used for partitioning tasks
  //will be used as fixed scale on (executorCount * executorCore)
  val SparkDefaultParallelismConfigName = "spark.default.parallelism"

  //default parallelism used for "shuffling" tasks (like joins)
  //will be used as fixed scale on (executorCount * executorCore)
  val SparkShuffleParallelismConfigName = "spark.sql.shuffle.partitions"

  //spark worker memory size
  val SparkExecutorMemoryConfigName = "spark.executor.memory"
  //spark worker cpu count
  val SparkExecutorCoresConfigName = "spark.executor.cores"

  //max size of a result set during collect (over all workers)
  val SparkDriverMaxResultSizeConfigName = "spark.driver.maxResultSize"
}
