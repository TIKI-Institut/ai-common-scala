package com.tiki.ai.spark

import breeze.linalg.{DenseVector, SparseVector}
import com.tiki.ai.hadoop.HadoopConfigHolder
import com.tiki.ai.job.{Job, JobConfig}
import com.tiki.ai.spark.config._
import org.apache.spark.SparkContext
import org.apache.spark.sql.{Encoder, SparkSession}
import org.joda.time.DateTime

/**
  *   Trait defines fundamental features of all SparkJobs, e.g. transient logging
  *   specifying how to connect with Hadoop
  */
trait SparkApp[TConfig <: JobConfig]
    extends Job[TConfig]
    with Serializable
    with SparkSessionConfigHolder
    with HadoopConfigHolder {

  defaultParallelism(4)
  shuffleParallelism(16)

  @transient private[this] var sparkSessionFactory: SparkSessionFactory = new SparkSessionFactory()

  def setSparkSessionFactory(sessionFactory: SparkSessionFactory): Unit = {
    sparkSessionFactory = sessionFactory
  }

  /**
    * Transfer the local configs to your sessionFactory
    */
  override def initialize(jobConfig: TConfig): Unit = {
    appName(jobConfig.name)

    super.initialize(jobConfig)
  }

  /**
    * Clean-up method called after the job execution (after run())
    */
  override def terminate(jobConfig: TConfig): Unit = {
    super.terminate(jobConfig)

    sparkSessionFactory.stopSession()
  }

  @transient lazy implicit val spark: SparkSession   = sparkSessionFactory.createSession(this)
  @transient lazy implicit val context: SparkContext = spark.sparkContext

  /* these are encoders for typed operations with spark data-sets, add new Encoders here! */
  /* For new types also add register entries to RegisterCoreTypes object */
  @transient implicit lazy val dateTimeEncoder: Encoder[DateTime] =
    org.apache.spark.sql.catalyst.encoders.ExpressionEncoder()
  @transient implicit lazy val sparseVectorEncoder: Encoder[SparseVector[Double]] =
    org.apache.spark.sql.catalyst.encoders.ExpressionEncoder()
  @transient implicit lazy val denseVectorEncoder: Encoder[DenseVector[Double]] =
    org.apache.spark.sql.catalyst.encoders.ExpressionEncoder()

  /**
    * This loads the given classes with the given classloader.
    * A method to workaround classloader problems within spark.
    * See [[https://community.mapr.com/message/51601-spark-maprdbhbase-linkageerror-writing-to-table]]
    *
    * Usage:
    * {{{
    *   val classes = Seq("com.mapr.fs.jni.MapRPut", "com.mapr.fs.jni.MapRTableTools")
    *
    *   rdd.mapPartitions[T] { e =>
    *      preloadClasses(classes)
    *      e
    *   }
    * }}}
    *
    * Using this within a .map() didn't work.
    *
    * @param classes     Sequence of classes to load
    * @param classloader the classloader. Default:
    *                    {{{ Thread.currentThread().getContextClassLoader }}}
    */
  @deprecated("We don't use MapR anymore; Most likely obsolete", "1.0")
  @deprecated("use only if the bug with class loader surfaces again.", "1.0")
  protected def preloadClasses(classes: Seq[String],
                               classloader: ClassLoader = Thread.currentThread().getContextClassLoader): Unit =
    classes.foreach(classloader.loadClass)

}
