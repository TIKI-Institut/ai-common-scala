package com.tiki.ai.dsp

import io.github.classgraph.{ClassGraph, ScanResult}

import scala.util.Try

object SymbolProcessor {

  def resources[T](pf: PartialFunction[Try[ScanResult], T]): T = pf {
    Try {
      // ClassGraph will open every JAR file to inspect
      // For spark dsp kernels this will lead to a long Startup Time and a increased usage of Heap Memory
      // therefore we need to reject bundled stuff as good as possible to increase performance and reduce memory footprint
      // Jar Rejection -> Saves Startup time and memory footprint
      // Package Rejection -> Only saves startup time
      new ClassGraph()
        // .verbose() // use verbosity for debugging and maintaining
        .enableAnnotationInfo()
        .enableClassInfo()
        // App bundle packaged by SBT Native Packager (used by Provisioner)
        // check bundles jars with 'sbt universal:packageBin'
        .rejectJars(
          "com.typesafe.*.jar",
          "com.softwaremill.*.jar",
          "com.lightbend.*.jar",
          "com.github.*.jar",
          "com.fasterxml.*.jar",
          "io.spray.*.jar",
          "io.netty.*.jar",
          "org.jetbrains.*.jar",
          "org.scala-lang.*.jar",
          "org.slf4j.*.jar",
          "org.apache.*.jar",
          "org.rocksdb.*.jar",
          "commons-io.*.jar",
          "commons-lang.*.jar"
        )
        // Kernel provided Spark Distribution has a different jar naming convention
        // check existing distributed jars with 'ls -haltr /opt/spark/jars'
        .rejectJars(
          "hadoop*.jar",
          "hive*.jar",
          "jackson*.jar",
          "kubernetes*.jar",
          "spark*.jar",
          "azure*.jar",
          "aws*.jar",
          "parquet*.jar",
          "avro*.jar",
          "commons-*.jar",
          "breeze*.jar",
          "rocksdb*.jar",
          "scala-*.jar"
        )
        // fallback
        .rejectPackages(
          "akka",
          "sttp",
          "org.apache",
          "org.glassfish",
          "io.netty",
          "io.spray",
          "com.github.scopt",
          "com.softwaremill",
          "com.typesafe",
          "com.lightbend",
          "com.esotericsoftware",
          "com.google",
          "com.fasterxml",
          "scala",
          "spire",
          "breeze",
          "org.bouncycastle",
          "shaded.parquet"
        )
        .disableNestedJarScanning()
        .removeTemporaryFilesAfterScan()
        .scan
    }
  }

}
