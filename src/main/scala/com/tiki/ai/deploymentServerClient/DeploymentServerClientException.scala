package com.tiki.ai.deploymentServerClient

case class DeploymentServerClientException(msg: String) extends Exception(msg)

