package com.tiki.ai.deploymentServerClient

import spray.json.enrichAny

trait OverrideJsonSupport extends OverrideJsonMarshaller  {

  /**
   * Creates an override Json string for the deployment request to the ai-deployment-server.
   * Note: The actual implementation only supports for the envVars key, value and not configMapKeyRef or secretKeyRef!
   *       In addition to that, it has to be mentioned, that the deployment server will handle an empty list of args
   *       (e.g. "args":[]) as non existent value.
   *
   * @param jobName    the job name (for Python jobs: The name of the job annotation, for Scala Jobs: the whole classpath
   *                   of the Scala job).
   * @param jobArgs    the arguments of the job (e.g. Map(("--in", "I"), ("--out", "O"))). Note hat you may write "--" before
   *                   the keys, but you don't have to. The function can handle both variants. This param defaults to an empty map.
   * @param envVars    environment variables you want to override (e.g. Map("ENV_VAR_1" -> "value1", "ENV_VAR_2" -> "val2")).
   *                   This param defaults to an empty map.
   * @param postfix    the postfix of the later deployed job on the DSP cluster (e.g. "-toni"). Defaults to "".
   * @param isSparkJob the information, if this job is a Spark job. If it's a Spark job, the "driver" will be added
   *                   as first job argument. This param defaults to true.
   * @return The Json string of the override Json
   */
  def buildJobOverrideJsonString(jobName: String,
                                 jobArgs: Map[String, String] = Map.empty,
                                 envVars: Map[String, String] = Map.empty,
                                 postfix: String = "",
                                 isSparkJob: Boolean = true): String = {

    val formattedEnvVars = buildEnvVars(envVars)

    Seq(DeploymentOverride(args = buildJobArgs(jobName,jobArgs, isSparkJob), envVars = formattedEnvVars, namePostfix = postfix))
      .toJson.compactPrint
  }

  private def buildEnvVars(envVars: Map[String, String]): Seq[EnvVar] = {
    envVars.map{case (key, value) => EnvVar(name = key, value = value)}.toSeq
  }

  private def buildJobArgs(jobName: String, args: Map[String, String], isSparkJob: Boolean): Seq[String] = {

    //No args given -> empty sequence is sufficient
    if (args.isEmpty) {
      Seq.empty
    } else {
      //Args given -> we have to create the sequence for them
      val formattedArgs = args.flatMap { case (key, value) => Seq(s"--${key.stripPrefix("--")}", s"$value") }.toSeq
      val retVal = jobName +: formattedArgs

      if (isSparkJob) {
        "driver" +: retVal
      } else {
        retVal
      }
    }
  }
}
