package com.tiki.ai.deploymentServerClient

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, JsonFormat}

case class DeploymentOverride(args: Seq[String], envVars: Seq[EnvVar], namePostfix: String)
case class EnvVar(name: String, value: String)

/**
 * JSON converter for DeploymentOverride and EnvVar.
 */
trait OverrideJsonMarshaller extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val envVarFormat: JsonFormat[EnvVar] = jsonFormat2(EnvVar)
  implicit val deploymentOverrideFormat: JsonFormat[DeploymentOverride] = jsonFormat3(DeploymentOverride)
}