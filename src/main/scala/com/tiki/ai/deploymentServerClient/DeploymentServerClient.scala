package com.tiki.ai.deploymentServerClient

import sttp.client3.{HttpURLConnectionBackend, Identity, SttpBackend, UriContext, asString, basicRequest}

import java.util.logging.Logger
import scala.util.{Failure, Properties, Success, Try}

/**
  * The [[DeploymentServerClient]] provides the functionality to communicate with the ai-deployment-server.
  * The actual implementation enables the opportunity to deploy flavors (jobs, webservices) via the deploy() function
  */
trait DeploymentServerClient {

  @transient private val logger = Logger.getLogger(getClass.getName)

  //Set the default values if the user does not specify
  private[deploymentServerClient] def principal: String = sys.env("DSP_PRINCIPAL")

  private[deploymentServerClient] def project: String = sys.env("DSP_NAME")

  private[deploymentServerClient] def environment: String = sys.env("DSP_ENVIRONMENT")

  private[deploymentServerClient] def flavorVersion: String = sys.env("DSP_FLAVOR_VERSION")

  //The HttpClientSyncBackend() could not be used, because it needs Java 11 onwards
  protected lazy val httpBackend: SttpBackend[Identity, Any] = HttpURLConnectionBackend()

  /**
    * Deploys a flavor (job, webservice) via the ai-deployment-server. This will be achieved with a POST request
    * to the ai-deployment-server
    * Note: The flavor has to be provisioned first!
    *
    * @param flavorName              the flavor name (e.g. train-job)
    * @param flavorVersion           the version of the flavor (e.g. "0.0.1", defaults to the envVar DSP_FLAVOR_VERSION)
    * @param principal               the principal of the flavor (e.g. "customerX", defaults to the envVar DSP_PRINCIPAL)
    * @param project                 the corresponding project (e.g. "projectA", defaults to the envVar DSP_NAME)
    * @param environment             the specific environment of the project (e.defaults to the envVar DSP_ENVIRONMENT)
    * @param deploymentServerBaseUrl the url of the ai-deployment-server that has to be requested
    *                                (defaults to "https://deployment-server.tiki-dsp.io")
    * @param deploymentServerVersion the version of the deployment server (defaults "latest")
    * @param token                   the Bearer Token that is needed for authentication with the ai-deployment-server
    *                                (defaults to the token of the local "/dsp/auth/token" file)
    * @param overrideJson            optional Json object for overrides (e.g. override args, envVars or add a postfix to the name).
    * @return On success: The status code & message of the ai-deployment-server response, wrapped in a [[Success]] object
    *         On error: The exception wrapped in a [[Failure]] object
    */
  def deploy(flavorName: String,
             flavorVersion: String = flavorVersion,
             principal: String = principal,
             project: String = project,
             environment: String = environment,
             deploymentServerBaseUrl: String = "https://deployment-server.tiki-dsp.io",
             deploymentServerVersion: String = "latest",
             token: String = getTokenFromFile(),
             overrideJson: Option[String] = None): Try[(Int, String)] = {

    val requestUri = deployUrl(flavorName, flavorVersion, principal, project, environment, deploymentServerBaseUrl, deploymentServerVersion)

    val response = makeDeployRequest(requestUri = requestUri, token = token, overrideJson = overrideJson)

    response match {
      case Success(s) =>
        logger.info(s"Successfully deployed job $flavorName with version $flavorVersion.")
        Success((s._1, s._2))
      case Failure(e) =>
        logger.severe(s"Deployment of the job failed! aborting with exception")
        Failure(e)
    }
  }

  def getTokenFromFile(filename: String = "/dsp/auth/token"): String = {
    import scala.io.Source
    val bufferedSource = Source.fromFile(filename)
    val token          = bufferedSource.getLines.mkString
    bufferedSource.close
    token
  }

  private def makeDeployRequest(requestUri: String, token: String, overrideJson: Option[String]): Try[(Int, String)] = {
    val baseRequest = basicRequest.auth
      .bearer(token)
      .post(uri"$requestUri")
      .response(asString)

    //Extend the request with the override json body
    val deployRequest =
      if (overrideJson.isDefined) baseRequest.body(overrideJson.get).contentType("application/json")
      else baseRequest

    val response = deployRequest.send(httpBackend)

    if (!response.code.isSuccess) {
      Failure(DeploymentServerClientException(s"unexpected status ${response.code.code} code on $requestUri POST"))
    } else {
      Success((response.code.code, response.body.fold(l => s"could not resolve response body: $l", r => r)))
    }
  }

  private def deployUrl(flavorName: String,
                        flavorVersion: String,
                        principal: String,
                        project: String,
                        environment: String,
                        deploymentServerBaseUrl: String,
                        deploymentServerVersion: String): String = {
    s"$deploymentServerBaseUrl/$deploymentServerVersion/phase/deploy/$principal/$project/$environment/$flavorName/$flavorVersion"
  }
}
