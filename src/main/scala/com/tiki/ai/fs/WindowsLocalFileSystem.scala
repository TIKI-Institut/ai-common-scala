package com.tiki.ai.fs

import java.io.{File, FileNotFoundException}
import java.net.URI

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs._
import org.apache.hadoop.fs.local.LocalConfigKeys
import org.apache.hadoop.fs.permission.FsPermission

class WindowsLocalFileSystem extends LocalFileSystem(new WindowsRawLocalFileSystem())

class WindowsRawLocalFileSystem extends RawLocalFileSystem {

  override def listStatus(path: Path): Array[FileStatus] = {
    val localf = this.pathToFile(path)
    if (!localf.exists()) {
      throw new FileNotFoundException("File " + path + " does not exist")
    } else if (localf.isFile) {
      Array(WindowsFileStatus(localf, getDefaultBlockSize(path), this))
    } else {
      val names = localf.list()
      if (names == null) {
        throw new FileNotFoundException(path + ": null file list")
      } else {
        names.map(name => this.getFileStatus(new Path(path, new Path(null, null, name))))
      }
    }
  }

  override def getFileStatus(path: Path): FileStatus = {
    val localf = this.pathToFile(path)

    if (localf.exists) {
      WindowsFileStatus(localf, getDefaultBlockSize(path), this)
    } else {
      throw new FileNotFoundException("File " + path + " does not exist")
    }
  }

  override def setPermission(p: Path, permission: FsPermission): Unit = { /* noop */ }
}

class WindowsLocalFs(conf: Configuration) extends ChecksumFs(new InnerWindowsLocalFs(conf)) {
  def this(theUri: URI, conf: Configuration) {
    this(conf)
  }
}

private[this] class InnerWindowsLocalFs(theUri: URI, conf: Configuration)
    extends DelegateToFileSystem(theUri, new WindowsLocalFileSystem(), conf, FsConstants.LOCAL_FS_URI.getScheme, false) {

  def this(conf: Configuration) {
    this(FsConstants.LOCAL_FS_URI, conf)
  }

  override def getUriDefaultPort: Int = -1

  override def getServerDefaults: FsServerDefaults = LocalConfigKeys.getServerDefaults

  override def isValidName(src: String) = true
}

object WindowsFileStatus {
  def apply(f: File, defaultBlockSize: Long, fs: FileSystem): FileStatus =
    new FileStatus(f.length(),
                   f.isDirectory,
                   1,
                   defaultBlockSize,
                   f.lastModified(),
                   new Path(f.getPath).makeQualified(fs.getUri, fs.getWorkingDirectory))

}
