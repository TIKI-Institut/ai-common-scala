package com.tiki.ai.fs
import org.apache.hadoop.fs.Path

class AbsoluteFileSystemDescriptor extends FileSystemDescriptor {

  override def basePath: Path = null

  /** *
    * Checks whether a given path is allowed to be used in [[resolve]] or [[open]]
    *
    * @param path Relative path as [[Path]] to the wanted resource
    * @return true if the path is allowed; otherwise false
    */
  override def isAllowed(path: Path): Boolean = true

  /** *
    * Resolves the absolute URI to a resource taking configuration into account
    *
    * @param path Relative path as [[Path]] to the wanted resource
    * @return Absolute Path to the wanted resource
    */
  override def resolve(path: Path): Path = path
}
