package com.tiki.ai.fs

import com.tiki.ai.config.CommonLibConfig
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileUtil, Path}

import java.net.URI
import scala.annotation.tailrec
import scala.util.matching.Regex

object FileSystems {
  def apply(fsAlias: String): FileSystemDescriptor = {
    fsAlias match {
      case a if a != null =>
        CommonLibConfig.FileSystemConfig.mappings(a) // todo <- why there is hard-coded reference to CommonLibConfig?
      case null =>
        new AbsoluteFileSystemDescriptor()
    }
  }
}

object FileSystemLocation {

  import scala.language.implicitConversions

  lazy val FileSystemLocationParsingExpression: Regex = s"(.+)\\$filesystemLocationSeparator(.*)".r
  val filesystemLocationSeparator: String             = "|"

  def root(fileSystem: String): FileSystemLocation = FsLocation(fileSystem, ".")

  def apply(alias: String, path: String): FileSystemLocation = FsLocation(alias, path)

  def apply(s: String): FileSystemLocation = parseLocation(s)

  def apply(s: URI): FileSystemLocation = parseLocation(s)

  def empty: FileSystemLocation = EmptyFileSystemLocation$

  object EmptyFileSystemLocation$ extends FileSystemLocation {
    override def fsAlias: String = null

    override def path: Path = null

    override def resolve: Path = throw new UnsupportedOperationException

    override def toString: String = throw new UnsupportedOperationException

    /** *
      *
      * @see com.tiki.ai.fs.FileSystemLocation.locate
      */
    override def locate(target: String): FileSystemLocation = throw new UnsupportedOperationException

    /** *
      *
      * @param target a relative path to a resource in the current directory or a subdirectory of the current location
      * @return a new [[FileSystemLocation]] object
      */
    override def locate(target: Path): FileSystemLocation = throw new UnsupportedOperationException

    /** *
      *
      * @see com.tiki.ai.fs.FileSystemDescriptor.open
      */
    override def open()(implicit hadoopConfig: Configuration): FileAccess = throw new UnsupportedOperationException

    /** *
      *
      * @see com.tiki.ai.fs.FileSystemDescriptor.exists
      */
    override def exists()(implicit hadoopConfig: Configuration): Boolean = throw new UnsupportedOperationException

    /** *
      * Prepends a prefix to the Resource and returns a new file system location
      *
      */
    override def prefix(prefix: String): FileSystemLocation = throw new UnsupportedOperationException

    /** *
      * Appends a postfix to the Resource and returns a new file system location
      *
      */
    override def postfix(postfix: String): FileSystemLocation = throw new UnsupportedOperationException

    /** *
      * Deletes the underlying file
      *
      */
    override def delete()(implicit hadoopConfig: Configuration): Boolean = throw new UnsupportedOperationException

    /**
      * @see com.tiki.ai.fs.FileSystemDescriptor.list
      *
      */
    override def list()(implicit hadoopConfig: Configuration): Array[FileSystemLocation] =
      throw new UnsupportedOperationException
  }

  implicit def parseLocation(string: String): FsLocation = {
    string match {
      case FileSystemLocationParsingExpression(alias, p) =>
        val path = new Path(if (p.isEmpty) "." else p)
        assert(!path.isAbsolute, "unable to parse fs location argument which is a absolute path")

        FsLocation(alias, if (path.toString.isEmpty) "." else path.toString)

      case s =>
        //maybe this is a absolute URI than we use it as this
        val uri = new URI(s)

        assert(uri.isAbsolute, "unable to parse fs location argument which is a relative path")

        //no Exception here? Okay; go on
        FsLocation(null, new Path(uri).toString)
    }
  }

  implicit def parseLocation(absoluteUri: URI): FsLocation = {
    assert(absoluteUri.isAbsolute, "unable to parse fs location argument which is a relative path")
    FsLocation(null, new Path(absoluteUri).toString)
  }
}

trait FileSystemLocation {

  def fsAlias: String

  def path: Path

  override def toString: String = {
    fsAlias match {
      case null => path.toString
      case s    => s"$s${FileSystemLocation.filesystemLocationSeparator}$path"
    }
  }

  /** *
    *
    * @see com.tiki.ai.fs.FileSystemDescriptor.open
    */
  def open()(implicit hadoopConfig: Configuration): FileAccess = FileSystems(fsAlias).open(path)

  /** *
    *
    * @see com.tiki.ai.fs.FileSystemDescriptor.exists
    */
  def exists()(implicit hadoopConfig: Configuration): Boolean = FileSystems(fsAlias).exists(path)

  /** *
    * Prepends a prefix to the Resource and returns a new FileSystem location
    *
    */
  def prefix(prefix: String): FileSystemLocation =
    FsLocation(fsAlias, new Path(path.getParent, s"$prefix${path.getName}").toString)

  /** *
    * Appends a postfix to the Resource and returns a new FileSystem location
    *
    */
  def postfix(postfix: String): FileSystemLocation =
    FsLocation(fsAlias, new Path(path.getParent, s"${path.getName}$postfix").toString)

  /** *
    * Deletes the underlying file
    *
    */
  def delete()(implicit hadoopConfig: Configuration): Boolean = FileSystems(fsAlias).delete(path)

  /**
    * Returns an array of FileSystemLocation provided relative path
    *
    * @param hadoopConfig The hadoop config for file systems
    * @return Array of FileSystemLocation
    */
  def list()(implicit hadoopConfig: Configuration): Array[FileSystemLocation] = {

    val resolved               = resolve
    val underlyingFSListResult = FileSystems(fsAlias).list(path)
    if (underlyingFSListResult.length == 1 && underlyingFSListResult(0).getPath == resolved) {
      return Array.empty
    }

    val localRoot =
      if (!resolved.toString.endsWith(Path.SEPARATOR_CHAR.toString)) {
        resolved + Path.SEPARATOR_CHAR.toString
      } else {
        resolved.toString
      }

    underlyingFSListResult.map { path =>
      val resolvedChildPath = path.getPath.toString

      assert(resolvedChildPath.startsWith(localRoot))

      this.locate(resolvedChildPath.substring(localRoot.length))
    }

  }

  /** *
    *
    * @return if [[FileSystemLocation]] is File
    */
  def isFile(implicit hadoopConfig: Configuration): Boolean = FileSystems(fsAlias).isFile(path)

  /** *
    *
    * @return if [[FileSystemLocation]] is Directory
    */
  def isDirectory(implicit hadoopConfig: Configuration): Boolean = FileSystems(fsAlias).isDirectory(path)

  /** *
    * Lists all files within the specified [[FileSystemLocation]], if it is a file itself, it will return itself.
    * @param recursive: if list should be done recursive
    * @return Array[[FileSystemLocation]] all Files in [[FileSystemLocation]]
    */
  final def listFiles(recursive: Boolean)(implicit hadoopConfig: Configuration): Array[FileSystemLocation] = {
    @tailrec
    def listFilesRecursive(files: Array[FileSystemLocation], directoryStack: List[FileSystemLocation]): Array[FileSystemLocation] = {
      directoryStack match {
        case Nil => files
        case head :: tail =>
          val locations = head.list()
          listFilesRecursive(files ++ locations.filter(x => x.isFile), tail ++ locations.filter(x => x.isDirectory))
      }
    }

    if (this.isFile) return Array(this)

    val locations = this.list()
    val files = locations.filter(x => x.isFile)
    if (!recursive) files
    else {
      val dirs = locations.filter(x => x.isDirectory).toList
      listFilesRecursive(files, dirs)
    }
  }

  def resolve: Path = FileSystems(fsAlias).resolve(path)

  /** *
    *
    * @see com.tiki.ai.fs.FileSystemLocation.locate
    */
  def locate(target: String): FileSystemLocation = locate(new Path(target))

  /** *
    *
    * @param target a relative path to a resource in the current directory or a subdirectory of the current location
    * @return a new [[FileSystemLocation]] object
    */
  def locate(target: Path): FileSystemLocation = {
    assert(!target.isAbsolute, "no absolute path allowed")
    FsLocation(fsAlias, new Path(path, target).toString)
  }

  /**
    * Copies the current FileSystemLocation to another FileSystemLocation
    */
  def copy(destLocation: FileSystemLocation, deleteSource: Boolean = false, overwrite: Boolean = false)(
      implicit hadoopConfig: Configuration): Unit = {

    val srcFileSystem  = FileSystems(fsAlias).newFileSystem(resolve.toUri)
    val destFileSystem = FileSystems(destLocation.fsAlias).newFileSystem(destLocation.resolve.toUri)

    try {
      FileUtil.copy(srcFileSystem, resolve, destFileSystem, destLocation.resolve, deleteSource, overwrite, hadoopConfig)
    } finally {
      srcFileSystem.close()
      destFileSystem.close()
    }
  }
}

case class FsLocation(fsAlias: String, pathString: String) extends FileSystemLocation {

  @transient lazy val path: Path = new Path(pathString)

  if (fsAlias == null && !path.isAbsolute)
    assert(assertion = false, "relative paths are not allowed without a filesystem alias")
  if (fsAlias != null && path.isAbsolute) assert(assertion = false, "absolute paths are not allowed with a filesystem alias")

}
