package com.tiki.ai.fs

import com.tiki.ai.config.ConfigHolder
import com.typesafe.config.{Config, ConfigObject}
import org.apache.commons.lang3.StringUtils
import org.apache.hadoop.fs.Path

import java.io.File
import java.util.regex.Pattern
import scala.collection.JavaConverters._

/**
  * Creates a FS descriptor for local FS access
  * See root parameter spec for options
  *
  * @param root base directory to use as root for this FS descriptor. use :
  *             - "." for current directory
  *             - "XXX" as alias for "./XXX"
  *             - "xxx/yyyy" as alias for "./xxx/yyyy"
  *             - "C:/blub/test" for "C:/blub/test" (on windows)
  *             - "/asdf/test" for "/asdf/test" (on linux)
  */
case class LocalFileSystemMapping(root: String) extends FileSystemDescriptor {

  import LocalFileSystemMapping._

  assert(root != null, "root arguemnt is requiered")

  override lazy val basePath: Path = {

    var myRoot = root

    if (hasWindowsDrive(root)) {

      if (root.head != '/') {
        myRoot = "/" + root
      }

      myRoot = StringUtils.replace(myRoot, "\\", "/")
    }

    val workingDir = myRoot match {
      case null => new File(".").getAbsolutePath
      case s =>
        try {
          new Path(FileSystemTypes.FS_TYPE_FILE_LOCAL, null, s)
          s
        } catch {
          case _: IllegalArgumentException =>
            new File(s).getAbsolutePath
        }
    }

    //This handles windows and linux paths
    new Path(FileSystemTypes.FS_TYPE_FILE_LOCAL, null, workingDir)
  }

  //Outcome of this has to be a absolute path or we are in trouble
  assert(basePath.isAbsolute, "relative paths are not allowed within this FS Mapping")
}

object LocalFileSystemMapping {
  val HAS_DRIVE_LETTER_SPECIFIER: Pattern = Pattern.compile("^/?[a-zA-Z]:")

  def hasWindowsDrive(path: String): Boolean = HAS_DRIVE_LETTER_SPECIFIER.matcher(path).find
}

case class HdfsFileSystemMapping(host: String, port: Int, root: String) extends FileSystemDescriptor {
  override lazy val basePath: Path = new Path(FileSystemTypes.FS_TYPE_HDFS, s"$host:$port", root)
}

case class S3AFileSystemMapping(scheme: String,
                                endpoint: String,
                                accessKey: String,
                                secretKey: String,
                                enableSsl: Boolean,
                                bucket: String,
                                pathStyleAccess: Boolean,
                                root: String)
    extends FileSystemDescriptor {
  assert(scheme == FileSystemTypes.FS_TYPE_S3A, "only S3A is allowed here")
  override lazy val basePath: Path = new Path(scheme, bucket, root)
}

case class AzureDls2FileSystemMapping(containerName: String, storageAccountName: String, root: String)
    extends FileSystemDescriptor {
  override lazy val basePath: Path = {
    new Path(FileSystemTypes.FS_TYPE_AZURE_DATA_LAKE_2, s"$containerName@$storageAccountName.dfs.core.windows.net", root)
  }
}

case class AzureBlobFileSystemMapping(containerName: String, storageAccountName: String, root: String)
    extends FileSystemDescriptor {
  override lazy val basePath: Path = {
    new Path(FileSystemTypes.FS_TYPE_AZURE_DATA_BLOB, s"$containerName@$storageAccountName.blob.core.windows.net", root)
  }
}

case class SftpFileSystemMapping(host: String, port: Int, root: String, user: String, keyFile: String, password: String = "")
    extends FileSystemDescriptor {

  override lazy val basePath: Path = new Path(FileSystemTypes.FS_TYPE_SFTP, f"$host:$port", root)
}

trait FileSystemConfig {
  self: ConfigHolder =>

  object FileSystemConfigKeys {

    /* main fs config block */
    val FS_BLOCK = "fileSystems"

    /* main fs Types */
    val FS_SCHEME = "scheme"

    /* custom azure file system implementations */
    val FS_AZURE_DATA_LAKE_2_IMPL = "abfssImpl"
    val FS_AZURE_DATA_BLOB_IMPL   = "wasbsImpl"
    val FS_SFTP_IMPL              = "sftpImpl"

    /* hdfs */
    object HDFS {
      val HOST = "host"
      val PORT = "port"
      val ROOT = "root"
    }

    /* file (LOCAL) */
    object LOCAL {
      val ROOT = "root"
    }

    /* S3A */
    object S3A {
      val ENDPOINT          = "endpoint"
      val ACCESS_KEY        = "accessKey"
      val SECRET_KEY        = "secretKey"
      val ENABLE_SSL        = "enableSsl"
      val BUCKET            = "bucket"
      val ROOT              = "root"
      val PATH_STYLE_ACCESS = "pathStyleAccess"
    }

    /* Azure DL storage Gen2 */
    object AZDLS2 {
      val STORAGE_ACCOUNT_NAME = "storageAccountName"
      val CONTAINER_NAME       = "containerName"
      val IMPL                 = "impl"
      val ROOT                 = "root"
    }

    /* Azure Blob storage */
    object AZBLOB {
      val STORAGE_ACCOUNT_NAME = "storageAccountName"
      val CONTAINER_NAME       = "containerName"
      val IMPL                 = "impl"
      val ROOT                 = "root"
    }

    /* STFP */
    object SFTP {
      val host     = "host"
      val port     = "port"
      val root     = "root"
      val user     = "user"
      val password = "password"
      val keyFile  = "keyFile"
    }
  }

  object FileSystemConfig {

    private[fs] lazy val filesystemConfig = config.getOptionalConfig(FileSystemConfigKeys.FS_BLOCK).map(_.root())
    private def readHdfsFileSystemMappingConfig(c: Config): FileSystemDescriptor =
      HdfsFileSystemMapping(
        host = c.getString(FileSystemConfigKeys.HDFS.HOST),
        port = c.getInt(FileSystemConfigKeys.HDFS.PORT),
        root = c.getString(FileSystemConfigKeys.HDFS.ROOT)
      )

    private def readS3AFileSystemMappingConfig(scheme: String, c: Config): FileSystemDescriptor =
      S3AFileSystemMapping(
        scheme = scheme,
        endpoint = c.getString(FileSystemConfigKeys.S3A.ENDPOINT),
        accessKey = c.getString(FileSystemConfigKeys.S3A.ACCESS_KEY),
        secretKey = c.getString(FileSystemConfigKeys.S3A.SECRET_KEY),
        enableSsl = c.getBoolean(FileSystemConfigKeys.S3A.ENABLE_SSL),
        bucket = c.getString(FileSystemConfigKeys.S3A.BUCKET),
        pathStyleAccess = c.getBoolean(FileSystemConfigKeys.S3A.PATH_STYLE_ACCESS),
        root = c.getString(FileSystemConfigKeys.S3A.ROOT)
      )

    private def readLocalFileSystemMappingConfig(c: Config): FileSystemDescriptor =
      LocalFileSystemMapping(
        root = c.getString(FileSystemConfigKeys.LOCAL.ROOT)
      )

    private def readAzureDls2FileSystemMappingConfig(c: Config): FileSystemDescriptor =
      AzureDls2FileSystemMapping(
        containerName = c.getString(FileSystemConfigKeys.AZDLS2.CONTAINER_NAME),
        storageAccountName = c.getString(FileSystemConfigKeys.AZDLS2.STORAGE_ACCOUNT_NAME),
        root = c.getString(FileSystemConfigKeys.AZDLS2.ROOT)
      )

    private def readAzureBlobFileSystemMappingConfig(c: Config): FileSystemDescriptor =
      AzureBlobFileSystemMapping(
        containerName = c.getString(FileSystemConfigKeys.AZBLOB.CONTAINER_NAME),
        storageAccountName = c.getString(FileSystemConfigKeys.AZBLOB.STORAGE_ACCOUNT_NAME),
        root = c.getString(FileSystemConfigKeys.AZBLOB.ROOT)
      )

    private def readSftpFileSystemMappingConfig(c: Config): FileSystemDescriptor =
      SftpFileSystemMapping(
        host = c.getString(FileSystemConfigKeys.SFTP.host),
        port = c.getInt(FileSystemConfigKeys.SFTP.port),
        root = c.getString(FileSystemConfigKeys.SFTP.root),
        user = c.getString(FileSystemConfigKeys.SFTP.user),
        password = c.getString(FileSystemConfigKeys.SFTP.password),
        keyFile = c.getString(FileSystemConfigKeys.SFTP.keyFile)
      )

    lazy val mappings: Map[String, FileSystemDescriptor] =
      filesystemConfig match {
        case Some(configObject) =>
          configObject.asScala.flatMap {
            case (fsAlias: String, config: ConfigObject) =>
              if (config.toConfig.hasPath(FileSystemConfigKeys.FS_SCHEME)) {

                val c     = config.toConfig
                val $type = c.getString(FileSystemConfigKeys.FS_SCHEME)

                $type match {
                  case t if t == FileSystemTypes.FS_TYPE_HDFS       => Some(fsAlias -> readHdfsFileSystemMappingConfig(c))
                  case t if t == FileSystemTypes.FS_TYPE_FILE_LOCAL => Some(fsAlias -> readLocalFileSystemMappingConfig(c))
                  case t if t == FileSystemTypes.FS_TYPE_S3A        => Some(fsAlias -> readS3AFileSystemMappingConfig(t, c))
                  case t if t == FileSystemTypes.FS_TYPE_AZURE_DATA_LAKE_2 =>
                    Some(fsAlias -> readAzureDls2FileSystemMappingConfig(c))
                  case t if t == FileSystemTypes.FS_TYPE_AZURE_DATA_BLOB =>
                    Some(fsAlias -> readAzureBlobFileSystemMappingConfig(c))
                  case t if t == FileSystemTypes.FS_TYPE_SFTP => Some(fsAlias -> readSftpFileSystemMappingConfig(c))
                  case _                                      => None
                }

              } else {
                None
              }
            case (_, _) => None
          }.toMap

        case _ => Map.empty[String, FileSystemDescriptor]
      }

    lazy val azureDataLakeV2Impl: Option[String] = config
      .getOptionalConfig(FileSystemConfigKeys.FS_BLOCK)
      .flatMap(config => config.getOptionalString(FileSystemConfigKeys.FS_AZURE_DATA_LAKE_2_IMPL))

    lazy val azureDataBlobImpl: Option[String] = config
      .getOptionalConfig(FileSystemConfigKeys.FS_BLOCK)
      .flatMap(config => config.getOptionalString(FileSystemConfigKeys.FS_AZURE_DATA_BLOB_IMPL))

    lazy val sftpImpl: Option[String] = config
      .getOptionalConfig(FileSystemConfigKeys.FS_BLOCK)
      .flatMap(config => config.getOptionalString(FileSystemConfigKeys.FS_SFTP_IMPL))
  }
}
