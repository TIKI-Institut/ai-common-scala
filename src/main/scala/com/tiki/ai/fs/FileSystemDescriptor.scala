package com.tiki.ai.fs

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileStatus, FileSystem, Path}

import java.io.{Closeable, FileNotFoundException, InputStream, OutputStream}
import java.net.URI

/**
  * A class to specify a FileSystem and its parameters
  */
private[fs] trait FileSystemDescriptor {

  def basePath: Path

  /**
    * Checks whether a given path is allowed to be used in [[resolve]] or [[open]]
    *
    * @param path Relative path as [[Path]] to the wanted resource
    * @return true if the path is allowed; otherwise false
    */
  def isAllowed(path: Path): Boolean = path.toUri.getPath.startsWith(basePath.toUri.getPath)

  /**
    * Checks whether a given path is allowed to be used in [[resolve]] or [[open]]
    *
    * @param path Relative path as [[String]] to the wanted resource
    * @return true if the path is allowed; otherwise false
    */
  def isAllowed(path: String): Boolean = isAllowed(new Path(path))

  /**
    * Resolves the absolute URI to a resource taking configuration into account
    *
    * @param path Relative path as [[String]] to the wanted resource
    * @return Absolute Path to the wanted resource
    */
  def resolve(path: String): Path = resolve(new Path(path))

  /**
    * Resolves the absolute URI to a resource taking configuration into account
    *
    * @param path Relative path as [[Path]] to the wanted resource
    * @return Absolute Path to the wanted resource
    */
  def resolve(path: Path): Path = {
    assert(path != null, "path must not be null")
    //assert(path.toString.nonEmpty, "path must not be empty")
    assert(!path.isAbsolute, "path must not be absolute")

    val result = new Path(basePath, path)

    assert(isAllowed(result), "parenting up is forbidden")

    result
  }

  /**
    * Checks existence of a file in der underlying FileSystem
    *
    * @param path         Relative path as [[Path]] to the wanted resource
    * @param hadoopConfig The hadoop config for file systems
    * @return [true] if the resource exists otherwise false
    */
  def exists(path: Path)(implicit hadoopConfig: Configuration): Boolean = {
    val fs = newFileSystem(resolve(path).toUri)
    try {
      fs.exists(resolve(path))
    } finally {
      fs.close()
    }
  }

  /**
    * Checks existence of a file in der underlying FileSystem
    *
    * @param path         Relative path as [[String]] to the wanted resource
    * @param hadoopConfig The hadoop config for file systems
    * @return [true] if the resource exists otherwise false
    */
  def exists(path: String)(implicit hadoopConfig: Configuration): Boolean = exists(new Path(path))

  /**
    * Creates a proxy for read or write from/to the wanted resource
    *
    * @param path         Relative path as [[Path]] to the wanted resource
    * @param hadoopConfig The hadoop config for file systems
    * @return A [[FileAccess]] proxy for direct access to the resource
    */
  def open(path: Path)(implicit hadoopConfig: Configuration): FileAccess = new FileAccessImpl(resolve(path))

  /**
    * Creates a proxy for read or write from/to the wanted resource
    *
    * @param path         Relative path as [[String]] to the wanted resource
    * @param hadoopConfig The hadoop config for file systems
    * @return A [[FileAccess]] proxy for direct access to the resource
    */
  def open(path: String)(implicit hadoopConfig: Configuration): FileAccess = open(new Path(path))

  /**
    * Deletes the resource
    *
    * @param path         Relative path as [[Path]] to resource to be deleted
    * @param hadoopConfig The hadoop config for file systems
    * @return true if deletion was successful
    */
  def delete(path: Path)(implicit hadoopConfig: Configuration): Boolean = {
    val fs = newFileSystem(resolve(path).toUri)
    try {
      fs.delete(resolve(path), true)
    } finally {
      fs.close()
    }
  }

  /**
    * Deletes the resource
    *
    * @param path         Relative path as [[String]] to resource to be deleted
    * @param hadoopConfig The hadoop config for file systems
    * @return true if deletion was successful
    */
  def delete(path: String)(implicit hadoopConfig: Configuration): Boolean = delete(new Path(path))

  /** *
    * Determines if path is file
    *
    * @param path         Relative path as [[Path]] to resource
    * @param hadoopConfig The hadoop config for file systems
    * @return true if path file
    */
  def isFile(path: Path)(implicit hadoopConfig: Configuration): Boolean = {
    val fs = newFileSystem(resolve(path).toUri)

    try {
      fs.getFileStatus(resolve(path)).isFile
    } catch {
      case _: FileNotFoundException => false
    } finally {
      fs.close()
    }
  }

  /** *
    * Determines if path is file
    *
    * @param path         Relative path as [[String]] to resource
    * @param hadoopConfig The hadoop config for file systems
    * @return true if path is file
    */
  def isFile(path: String)(implicit hadoopConfig: Configuration): Boolean = isFile(new Path(path))

  /** *
    * Determines if path is directory
    *
    * @param path         Relative path as [[Path]] to resource
    * @param hadoopConfig The hadoop config for file systems
    * @return true if path is directory
    */
  def isDirectory(path: Path)(implicit hadoopConfig: Configuration): Boolean = {
    val fs = newFileSystem(resolve(path).toUri)

    try {
      fs.getFileStatus(resolve(path)).isDirectory
    } catch {
      case _: FileNotFoundException => false
    } finally {
      fs.close()
    }
  }

  /***
    * Determines if path is directory
    *
    * @param path Relative path as [[String]] to resource
    * @param hadoopConfig The hadoop config for file systems
    * @return true if path is directory
    */
  def isDirectory(path: String)(implicit hadoopConfig: Configuration): Boolean = isDirectory(new Path(path))

  /**
    * Returns an array of FileStatus provided relative path
    *
    * @param path         relative path contents of which shall be listed
    * @param hadoopConfig The hadoop config for file systems
    * @return Array of FileStatus
    */
  def list(path: Path)(implicit hadoopConfig: Configuration): Array[FileStatus] = {
    val fs = newFileSystem(resolve(path).toUri)

    try {
      fs.listStatus(resolve(path))
    } finally {
      fs.close()
    }
  }

  /**
    * Returns an array of FileStatus provided relative path
    *
    * @param path         Relative path as [[String]] contents of which shall be listed
    * @param hadoopConfig The hadoop config for file systems
    * @return Array of FileStatus
    */
  def list(path: String)(implicit hadoopConfig: Configuration): Array[FileStatus] = list(new Path(path))

  class FileAccessImpl(target: Path)(implicit val hadoopConfig: Configuration) extends FileAccess {

    private def loan[A <: Closeable, B](fn: FileSystem => A)(f: A => B): B = {
      // we have to use FileSystem.newInstance method to get a unique, non-shared instance (otherwise parser and tagger
      // models share FileSystem instance and we get FileSystem closed exception when running vectorization spark job using POS tagger)
      val fs     = newFileSystem(target.toUri)
      val stream = fn(fs)
      try {
        f(stream)
      } finally {
        stream.close()
        fs.close()
      }
    }

    override def read[T](f: InputStream => T): T = loan(_.open(target))(f)

    override def write[T](f: OutputStream => T): T = loan(_.create(target))(f)
  }

  /** *
    * Creates a new filesystem for this the given configuration
    *
    * @param hadoopConfig The hadoop config for file systems
    * @return A [[FileSystem]] object
    */
  def newFileSystem(path: URI, writeChecksum: Boolean = false)(implicit hadoopConfig: Configuration): FileSystem = {
    val fs = FileSystem.newInstance(path, hadoopConfig)
    fs.setWriteChecksum(writeChecksum)
    fs
  }
}
