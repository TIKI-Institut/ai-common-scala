package com.tiki.ai.fs

import java.io._

import scala.io.Source

trait FileAccess {

  private val inputStreamToString: InputStream => String = stream => Source.fromInputStream(stream, "UTF-8").mkString

  def read[T](f: InputStream => T): T

  def write[T](f: OutputStream => T): T

  def writeObject[T](obj: T): Unit = {
    write { oStream: OutputStream =>
      val oos = new ObjectOutputStream(oStream)
      oos.writeObject(obj)
      oos.close()
    }
  }

  def readObject[T]: T = {
    read { iStream =>
      val ois = new ObjectInputStream(iStream)
      val obj = ois.readObject()
      ois.close()
      obj.asInstanceOf[T]
    }
  }

  // read and write text are implemented since they will be used at most
  def writeText(text: String): Unit = {
    write { oStream =>
      val writer = new BufferedWriter(new OutputStreamWriter(oStream))
      writer.write(text)
      writer.flush()
    }
  }

  def readText: String = read(inputStreamToString)
}
