package com.tiki.ai.fs

object FileSystemTypes {
  final val FS_TYPE_FILE_LOCAL        = "file"
  final val FS_TYPE_HDFS              = "hdfs"
  final val FS_TYPE_S3A               = "s3a"
  final val FS_TYPE_AZURE_DATA_LAKE_2 = "abfss"
  final val FS_TYPE_AZURE_DATA_BLOB   = "wasbs"
  final val FS_TYPE_SFTP              = "sftp"
}
