package com.tiki.ai.fs

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.io.IOUtils

import java.io.{File, IOException, InputStream}
import java.nio.file.{Files, Paths}

trait FileSystemUtils {

  /**
    * Copies the target Resource @param(target) to a local temporary File.
    * Calls the provided @param(action) with the new File
    * Deletes the File after @param(action) finishes
    *
    * @param target       The target Resource
    * @param hadoopConfig The hadoop config for file systems
    * @param action       The action to be executed
    * @tparam T The Return type
    * @return Returns the return value of the action
    */
  def copyToLocalTempFile[T](target: FileSystemLocation)(action: File => T)(implicit hadoopConfig: Configuration): T = {
    val tmpFile = File.createTempFile(java.util.UUID.randomUUID.toString.replaceAll("-", ""), null)
    try {

      target.open().read { is: InputStream =>
        Files.copy(is, Paths.get(tmpFile.getAbsolutePath))
      }
      action(tmpFile)
    } finally {

      if (tmpFile.exists()) {
        tmpFile.delete()
      }
    }

  }

  /**
    * Copies and appends contents from all files of source directory srcFileSystemLocation to single file dstFileSystemLocation.
    * Content will be append through alphabetical order of source file names.
    *
    * @param srcFileSystemLocation source FileSystemLocation must be Directory
    * @param dstFileSystemLocation destination FileSystemLocation will be single File
    * @param deleteSource          Boolean if the source should be deleted after copy
    * @param overwriteTarget       Boolean if existing target file should be overwritten
    * @param hadoopConf            implicit hadoopConfiguration
    * @return Boolean which returns true if everything worked
    */
  def copyMerge(srcFileSystemLocation: FileSystemLocation,
                dstFileSystemLocation: FileSystemLocation,
                deleteSource: Boolean,
                overwriteTarget: Boolean)(implicit hadoopConf: Configuration): Boolean = {

    val fs = FileSystem.newInstance(hadoopConf)
    // do not write crc files
    fs.setWriteChecksum(false)
    if (dstFileSystemLocation.exists()) {
      if (overwriteTarget) {
        dstFileSystemLocation.delete()
      } else {
        throw new IOException(s"Target ${dstFileSystemLocation.resolve.toString} already exists")
      }
    }
    // Source path is expected to be a directory:
    if (!fs.getFileStatus(srcFileSystemLocation.resolve).isDirectory) {
      return false
    }

    val outputFile = fs.create(dstFileSystemLocation.resolve)
    try {
      srcFileSystemLocation
        .list()
        .sortBy(x => x.resolve.getName)
        .collect {
          case status if fs.getFileStatus(status.resolve).isFile =>
            val inputFile = fs.open(status.resolve)
            try {
              IOUtils.copyBytes(inputFile, outputFile, hadoopConf, false)
            } finally {
              inputFile.close()
            }
        }
    } finally {
      outputFile.close()
    }

    if (deleteSource) srcFileSystemLocation.delete() else true
  }
}

object FileSystemUtils extends FileSystemUtils