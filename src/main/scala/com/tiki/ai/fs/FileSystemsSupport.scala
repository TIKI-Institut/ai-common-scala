package com.tiki.ai.fs

import com.tiki.ai.config.CommonLibConfig
import com.tiki.ai.hadoop.HadoopConfigSupport
import org.apache.hadoop.conf.Configuration

/**
  * Registers a filesystem hadoop configuration from actual runtime configuration
  */
trait FileSystemsSupport extends HadoopConfigSupport {

  import FileSystemsSupport._

  // todo <- why there is hard-coded reference to CommonLibConfig?
  CommonLibConfig.FileSystemConfig.mappings.map(_._2.createModifier()).foreach(updateHadoopConfig)

  // Optional: Set explicit file system implementation when it is set in config
  updateHadoopConfig(cfg => {
    CommonLibConfig.FileSystemConfig.azureDataLakeV2Impl.foreach(value =>
      cfg.set(s"fs.${FileSystemTypes.FS_TYPE_AZURE_DATA_LAKE_2}.impl", value))

    CommonLibConfig.FileSystemConfig.azureDataBlobImpl.foreach(value =>
      cfg.set(s"fs.${FileSystemTypes.FS_TYPE_AZURE_DATA_BLOB}.impl", value))

    CommonLibConfig.FileSystemConfig.sftpImpl.foreach(value =>
      cfg.set(s"fs.${FileSystemTypes.FS_TYPE_SFTP}.impl", value))
  })
}

object FileSystemsSupport {

  private val noopModifier: Configuration => Unit = { _ =>
    }

  implicit class FsDescriptorHadoopConfigModifier(descriptor: FileSystemDescriptor) {

    def createModifier(): Configuration => Unit = {
      descriptor match {
        case fsb: S3AFileSystemMapping => setPerBucketConfiguration(fsb)
        case fsb: SftpFileSystemMapping => setSftpConfiguration(fsb)
        case _                         => noopModifier
      }
    }
  }

  private[this] def setPerBucketConfiguration(fsDescriptor: S3AFileSystemMapping): Configuration => Unit = {
    config: Configuration =>
      config.set(s"fs.${fsDescriptor.scheme}.bucket.${fsDescriptor.bucket}.endpoint", fsDescriptor.endpoint)
      config.set(s"fs.${fsDescriptor.scheme}.bucket.${fsDescriptor.bucket}.access.key", fsDescriptor.accessKey)
      config.set(s"fs.${fsDescriptor.scheme}.bucket.${fsDescriptor.bucket}.secret.key", fsDescriptor.secretKey)
      config.setBoolean(s"fs.${fsDescriptor.scheme}.bucket.${fsDescriptor.bucket}.connection.ssl.enabled",
        fsDescriptor.enableSsl)
      config.setBoolean(s"fs.${fsDescriptor.scheme}.bucket.${fsDescriptor.bucket}.path.style.access",
        fsDescriptor.pathStyleAccess)
  }

  private[this] def setSftpConfiguration(fsDescriptor: SftpFileSystemMapping):Configuration => Unit = {
    config: Configuration =>
    config.set("fs.sftp.keyfile", fsDescriptor.keyFile)
    config.set(f"fs.sftp.user.${fsDescriptor.host}", fsDescriptor.user)
    config.set(f"fs.sftp.password.${fsDescriptor.host}.${fsDescriptor.user}", fsDescriptor.password)
  }
}
