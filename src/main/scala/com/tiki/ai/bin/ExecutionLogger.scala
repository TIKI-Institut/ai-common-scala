package com.tiki.ai.bin

import java.util.logging.Logger

/**
  * SEVERE — Indicates a serious failure. In general, SEVERE messages describe events that are of considerable importance and which will prevent normal program execution.
    WARNING — Indicates a potential problem. In general, WARNING messages describe events that will be of interest to end users or system managers, or which indicate potential problems.
    INFO — A message level for informational messages. The INFO level should only be used for reasonably significant messages that will make sense to end users and system administrators.
    CONFIG — A message level for static configuration messages. CONFIG messages are intended to provide a variety of static configuration information, and to assist in debugging problems that may be associated with particular configurations.
    FINE — A message level providing tracing information. All options, FINE, FINER, and FINEST, are intended for relatively detailed tracing. Of these levels, FINE should be used for the lowest volume (and most important) tracing messages.
    FINER — Indicates a fairly detailed tracing message.
    FINEST — Indicates a highly detailed tracing message. FINEST should be used for the most voluminous detailed output.
  */
trait ExecutionLogger {
  def logger: Logger

  private def performAction[T](action: => T): (T, Long) = {
    val start     = System.currentTimeMillis()
    val result: T = action
    val end       = System.currentTimeMillis()
    val duration  = end - start
    (result, duration)
  }

  def severe[T](logMessage: String, action: => T): T = {
    val (result, duration) = performAction(action)
    logger.severe(s"$logMessage | Execution took ${duration}ms")
    result
  }

  def warning[T](logMessage: String, action: => T): T = {
    val (result, duration) = performAction(action)
    logger.warning(s"$logMessage | Execution took ${duration}ms")
    result
  }

  def info[T](logMessage: String, action: => T): T = {
    val (result, duration) = performAction(action)
    logger.info(s"$logMessage | Execution took ${duration}ms")
    result
  }

  def logConfig[T](logMessage: String, action: => T): T = {
    val (result, duration) = performAction(action)
    logger.config(s"$logMessage | Execution took ${duration}ms")
    result
  }

  def logFine[T](logMessage: String, action: => T): T = {
    val (result, duration) = performAction(action)
    logger.fine(s"$logMessage | Execution took ${duration}ms")
    result
  }

  def logFiner[T](logMessage: String, action: => T): T = {
    val (result, duration) = performAction(action)
    logger.finer(s"$logMessage | Execution took ${duration}ms")
    result
  }

  def logFinest[T](logMessage: String, action: => T): T = {
    val (result, duration) = performAction(action)
    logger.finest(s"$logMessage | Execution took ${duration}ms")
    result
  }
}
