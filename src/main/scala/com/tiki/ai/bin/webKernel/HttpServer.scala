package com.tiki.ai.bin.webKernel

import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import com.tiki.ai.akka.AkkaContext

import java.util.logging.{Level, Logger}
import scala.concurrent.Future
import scala.util.{Failure, Success}

/**
  * The akka http server specific integration
  */
trait HttpServer extends AkkaContext {

  def routes: Route

  def interface: String

  def port: Int

  @transient private val log = Logger.getLogger(getClass.getName)

  private lazy val binding: Future[Http.ServerBinding] = Http().newServerAt(interface, port).bindFlow(routes)

  binding onComplete {
    case Success(_) =>
      log.info(s"Successfully bound server address [$interface]:[$port]")
    case Failure(ex) =>
      log.log(Level.SEVERE, s"Could not bind server address [$interface]:[$port]", ex)
      system.terminate() foreach { _ =>
        log.info("Akka http server successfully shutdown")
        // The actor context still run in background
        sys.exit(-1)
      }
  }
}
