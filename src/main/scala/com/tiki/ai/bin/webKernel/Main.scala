package com.tiki.ai.bin.webKernel

import akka.actor.ActorSystem
import com.tiki.ai.akka.AkkaContext
import com.tiki.ai.authentication.RolesListCommand
import com.tiki.ai.bin.ExecutionLogger

import java.util.logging.Logger
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success, Try}

/**
  * Main web scala app entrypoint
  */
object Main extends App with ExecutionLogger {
  val logger = Logger.getLogger(getClass.getName)

  // add roles list command
  RolesListCommand.listRoles(args)

  Try {
    // This will instantiate the web scala kernel
    // Main function will end after initialization
    // The underlying actor context will run that server
    info("initiated scala web kernel", new WebScalaKernel())
  } match {
    case Success(_) =>
    case Failure(ex) =>
      logger.severe(s"An fatal error $ex executing ${args.mkString(" ")} has occurred")
      sys.exit(-1)
  }
}

// At this time static
sealed trait WebScalaKernelHost {
  def interface: String = "0.0.0.0"
  def port: Int         = 5000
}

// At this time static
sealed trait WebScalaKernelAkkaContext extends AkkaContext {

  override implicit def system: ActorSystem  = ActorSystem("web-scala-kernel")
  override implicit def ec: ExecutionContext = system.dispatcher
}

private class WebScalaKernel
    extends HttpServer
    with WebScalaKernelHost
    with SecuredHttpService
    with WebScalaKernelAkkaContext
