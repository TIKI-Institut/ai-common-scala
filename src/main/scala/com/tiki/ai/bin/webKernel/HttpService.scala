package com.tiki.ai.bin.webKernel

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.tiki.ai.akka.AkkaContext
import com.tiki.ai.authentication.web.Jwks
import com.tiki.ai.config.CommonLibConfig
import com.tiki.ai.qos.QosRoutes

import scala.concurrent.ExecutionContext

/**
  * The root http service to provide akka http specific server routes
  */
trait HttpService {
  implicit def ec: ExecutionContext
  def routes: Route
}

trait SecuredHttpService extends HttpService with WebResourceProvider with Jwks with QosRoutes with AkkaContext {

  override def routes: Route =
    qosRoutes ~ new SecuredRoutes(webResources, CommonLibConfig.AuthConfig.dspWebDeploymentPath).routes
}
