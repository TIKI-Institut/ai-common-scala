package com.tiki.ai.bin.webKernel

import akka.http.scaladsl.server.{Directive0, Route}
import com.tiki.ai.authentication.oidc.OidcToken
import com.tiki.ai.dsp.SymbolProcessor

import java.util.logging.Logger
import scala.collection.JavaConverters._
import scala.util.{Failure, Success}

/**
  * The web resource interface which describes
  * how to build a akka http related endpoint
  */
trait WebResource {

  /**
    * A http web endpoint
    * e.g. path("user")
    */
  def endpoint: Directive0

  /**
    * * The actual route / action will be performed on an incoming http request
    * * on the above mentioned endpoint
    *
    * @param oidcToken The OIDC token from the resource owner
    *                  If 'NoRoles' are supplied, token will be set to None
    */
  def route(oidcToken: Option[OidcToken] = None): Route
}

/**
  * Provides all classes that mix in [[WebResource]].
  *
  * ATTENTION: Unfortunately, all concrete derivatives are required to have a default no arg constructor for this to work
  */
trait WebResourceProvider {
  def webResources: Seq[WebResource] = WebResourceProvider.webResources
}

/**
  * Injector which determine all classes
  * which have a relation with adapter "WebResource"
  * Injector fetches all implementation,
  * which can be further processed to routes
  */
private object WebResourceProvider {

  @transient private val log = Logger.getLogger(getClass.getName)

  private val webResources: Seq[WebResource] = {
    SymbolProcessor.resources {
      case Success(scanner) =>
        val webResources: Seq[WebResource] = scanner
          .getClassesImplementing(classOf[WebResource].getCanonicalName)
          .getStandardClasses
          .asScala
          .flatMap { classInfo =>
            val constructors = classInfo.loadClass().getConstructors
            if (!classInfo.isAbstract && constructors.nonEmpty && constructors.head.getGenericParameterTypes.isEmpty) {
              log.finer(s"Found WebResource [${classInfo.getName}] will attempt to instantiate now...")
              // return
              Some(constructors.head.newInstance().asInstanceOf[WebResource])
            } else {
              log.warning(s"""Class [${classInfo.getName}] has either no constructors or no no-arg constructor.
                An no-arg constructor is required for the instantiation of WebResources""")
              // return
              None
            }
          }
        scanner.close()
        webResources
      case Failure(exception) =>
        throw exception
    }
  }
}
