package com.tiki.ai.bin.webKernel

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Directive, Route}
import com.tiki.ai.akka.AkkaContext
import com.tiki.ai.authentication.RequiredRoles
import com.tiki.ai.authentication.web.TokenSecurityDirective

import scala.concurrent.ExecutionContext

class SecuredRoutes(webResources: Seq[WebResource], dspServicePath: String)(implicit val system: ActorSystem,
                                                                            val ec: ExecutionContext)
    extends AkkaContext
    with TokenSecurityDirective {

  private lazy val prefix: Directive[Unit] = dspPathToPathMatcher(dspServicePath)
  private val dspPathToPathMatcher: String => Directive[Unit] = _.split("/").foldLeft(rawPathPrefix("")) {
    case (x, y) if y.nonEmpty => x & pathPrefix(y)
    case (x, _)               => x
  }

  def routes: Route = logRequestResult("web-kernel-scala") { prefix { webResources } }

  private def webResourceToRoute: WebResource => Route =
    resource =>
      if (resource.getClass.isAnnotationPresent(classOf[RequiredRoles])) {
        val requiredRolesAnnotation = resource.getClass.getAnnotation(classOf[RequiredRoles])
        resource.endpoint {
          requireAllRoles(requiredRolesAnnotation.roles()) { oidcToken =>
            resource.route(Some(oidcToken))
          }
        }
      } else resource.endpoint(resource.route())

  private implicit def webResourceRoutes: Seq[WebResource] => Route =
    _.map(webResourceToRoute).reduceLeft(_ ~ _)
}
