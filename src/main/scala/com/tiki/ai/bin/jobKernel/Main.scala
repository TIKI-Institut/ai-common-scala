package com.tiki.ai.bin.jobKernel

import com.tiki.ai.authentication.RolesListCommand
import com.tiki.ai.bin.ExecutionLogger
import com.tiki.ai.job.OptionParserRunner
import com.tiki.ai.qos.cmd.{AsyncQosJobCommand, QosRegisterCommand}

import java.util.logging.Logger
import scala.reflect.runtime.universe
import scala.util.{Failure, Success}

object Main extends App with ExecutionLogger {

  val logger = Logger.getLogger(getClass.getName)

  // add roles list command
  RolesListCommand.listRoles(args)
  new QosRegisterCommand().qosRegister(args)
  new AsyncQosJobCommand().run(args)

  val (jobRunnerName, arguments) = args.toList match {
    case jName :: Nil   => (jName, Seq.empty[String])
    case jName :: jArgs => (jName, jArgs)
    case Nil =>
      throw new IllegalArgumentException(s"At least the canonical name of the job has to be provided as argument")
  }

  val rm: universe.Mirror = info("create runtime mirror", universe.runtimeMirror(getClass.getClassLoader))

  val module = try {
    info("load static module", rm.staticModule(jobRunnerName))
  } catch {
    case e: ScalaReflectionException =>
      throw new IllegalArgumentException(s"Provided job runner: $jobRunnerName was not found, aborting job execution", e)
  }

  val obj = info("load reflective module", rm.reflectModule(module))
  if (!obj.instance.isInstanceOf[OptionParserRunner]) {
    throw new IllegalArgumentException(
      "When using jobKernel, job class has to point to object implementing com.tiki.ai.job.OptionParserRunner")
  }

  val jobRunner: OptionParserRunner = obj.instance.asInstanceOf[OptionParserRunner]

  info(s"job $jobRunnerName", {
    jobRunner.apply(arguments) match {
      case Success(_)         =>
      case Failure(exception) => throw exception
    }
  })
}
