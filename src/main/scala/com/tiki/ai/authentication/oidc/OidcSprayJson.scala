package com.tiki.ai.authentication.oidc

import com.tiki.ai.authentication.{NoRoles, Roles}
import com.tiki.ai.config.CommonLibConfig
import pdi.jwt.{JwtBase64, JwtHeader, JwtSprayJsonParser, JwtUtils}
import spray.json.DefaultJsonProtocol

import java.time.Clock

object OidcSprayJson {
  private val authConfig = CommonLibConfig.AuthConfig

  def apply(oauthClient: String = authConfig.oauthClient, bearerToken: String) = new OidcSprayJson(oauthClient, bearerToken)
}

class OidcSprayJson(oauthClient: String, bearerToken: String) extends JwtSprayJsonParser[JwtHeader, OidcToken] {
  import DefaultJsonProtocol._
  import spray.json._

  implicit def clock: Clock = Clock.systemUTC()

  def jwtHeaderFromRawToken(rawToken: String): Option[JwtHeader] = {
    val header = JwtUtils.splitString(rawToken, '.').headOption

    header.map(base64Header => JwtBase64.decodeString(base64Header)).map(parseHeader)
  }

  def parseHeader(header: String): JwtHeader = {
    val jsObj = parse(header)
    JwtHeader(
      algorithm = getAlgorithm(jsObj),
      typ = safeGetField[String](jsObj, "typ"),
      contentType = safeGetField[String](jsObj, "cty"),
      keyId = safeGetField[String](jsObj, "kid")
    )
  }

  def parseClaim(claim: String): OidcToken = {
    val jsObj = parse(claim)

    val realmRoles: Roles = jsObj.fields
      .get("realm_access")
      .flatMap(ra => safeGetField[Seq[String]](ra.asJsObject, "roles"))
      .map(Roles(_: _*))
      .getOrElse(NoRoles)

    val allClientRoles = jsObj.fields
      .get("resource_access")
      .map { clients =>
        clients.asJsObject.fields.flatMap {
          case (client, roles) =>
            safeGetField[Seq[String]](roles.asJsObject, "roles") match {
              case Some(cr) => Some(client -> Roles(cr: _*))
              case _        => None
            }
        }
      }
      .getOrElse(Map.empty)

    val content = JsObject(
      jsObj.fields - "iss" - "sub" - "aud" - "exp" - "nbf" - "iat" - "jti" - "realm_access" - "resource_access"
    )
    val claims = content.fields.flatMap {
      case (key, value) =>
        safeRead[String](value) match {
          case Some(claimValue) => Some(key -> claimValue)
          case None             => None
        }
    }

    new OidcToken(
      bearerToken = bearerToken,
      oauthClient = oauthClient,
      issuer = safeGetField[String](jsObj, "iss"),
      subject = safeGetField[String](jsObj, "sub"),
      audience = safeGetField[Set[String]](jsObj, "aud")
        .orElse(safeGetField[String](jsObj, "aud").map(s => Set(s))),
      expiration = safeGetField[Long](jsObj, "exp"),
      notBefore = safeGetField[Long](jsObj, "nbf"),
      issuedAt = safeGetField[Long](jsObj, "iat"),
      jwtId = safeGetField[String](jsObj, "jti"),
      realmRoles = realmRoles,
      allClientRoles = allClientRoles,
      claims = claims
    )
  }

  private[this] def safeGetField[A: JsonReader](js: JsObject, name: String) =
    js.fields.get(name).flatMap(safeRead[A])

  private[this] def safeRead[A: JsonReader](js: JsValue) = safeReader[A].read(js).fold(_ => Option.empty, a => Option(a))

  override protected def extractExpiration(claim: OidcToken): Option[Long] = claim.expiration

  override protected def extractNotBefore(claim: OidcToken): Option[Long] = claim.notBefore
}
