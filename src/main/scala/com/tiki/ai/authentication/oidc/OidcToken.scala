package com.tiki.ai.authentication.oidc
import com.tiki.ai.authentication.{NoRoles, Roles}
import pdi.jwt.{JwtClaim, JwtUtils}

class OidcToken(override val issuer: Option[String] = None,
                override val subject: Option[String] = None,
                override val audience: Option[Set[String]] = None,
                override val expiration: Option[Long] = None,
                override val notBefore: Option[Long] = None,
                override val issuedAt: Option[Long] = None,
                override val jwtId: Option[String] = None,
                val bearerToken: String,
                val oauthClient: String,
                val realmRoles: Roles = NoRoles,
                val allClientRoles: Map[String, Roles] = Map.empty,
                val claims: Map[String, String] = Map.empty)
    extends JwtClaim("{}", issuer, subject, audience, expiration, notBefore, issuedAt, jwtId) {

  def clientRoles: Roles = allClientRoles.getOrElse(oauthClient, NoRoles)
  override def toJson: String = {

    val basicJwtValues = JwtUtils.hashToJson(
      Seq(
        "iss" -> issuer,
        "sub" -> subject,
        "aud" -> audience,
        "exp" -> expiration,
        "nbf" -> notBefore,
        "iat" -> issuedAt,
        "jti" -> jwtId
      ).collect {
        case (key, Some(value)) => key -> value
      })

    /***************************************************/
    /* outputting roles to oidc compatible json string */
    /***************************************************/
    val realmRoleJson = JwtUtils.hashToJson(Seq("realm_access" -> ("roles" -> realmRoles.roles)))

    val allClientRolesInnerJson = allClientRoles
      .map {
        case (client, roles) =>
          s"""\"$client\":{\"roles\":${JwtUtils.seqToJson(roles.roles)}}"""
      }
      .mkString(",")
    val allClientRolesJson = s"""{\"resource_access\":{$allClientRolesInnerJson}}"""

    val claimJson = JwtUtils.hashToJson(claims.toSeq)

    JwtUtils.mergeJson(basicJwtValues, realmRoleJson, allClientRolesJson, claimJson, content)
  }
}
