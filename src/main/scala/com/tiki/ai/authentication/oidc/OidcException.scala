package com.tiki.ai.authentication.oidc

sealed trait OidcException

class OidcAuthenticationException(message: String, cause: Throwable) extends RuntimeException(message, cause) with OidcException {
  def this(message: String) = this(message, null)
}

class OidcConfigurationException(message: String) extends RuntimeException(message) with OidcException
