package com.tiki.ai.authentication.oidc
import com.tiki.ai.authentication.web.Jwks
import com.tiki.ai.config.CommonLibConfig

import java.security.PublicKey

trait OidcContext extends Jwks with OidcAuthenticator {

  private val authConfig                                   = CommonLibConfig.AuthConfig
  override def jwtIssuer: String                           = authConfig.jwtIssuer
  override def publicKeyFromJwkKey(kid: String): PublicKey = getPublicKeyFromJWKS(authConfig.jwkUrl, kid)
  override def tokenTargetFileLocation: Option[String]     = authConfig.tokenTargetFile
  override def oauthClient: String                         = authConfig.oauthClient
}
