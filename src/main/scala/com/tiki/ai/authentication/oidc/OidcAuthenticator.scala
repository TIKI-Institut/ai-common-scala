package com.tiki.ai.authentication.oidc

import java.security.PublicKey
import java.time.Clock
import scala.io.Source
import scala.util.{Failure, Success}

trait OidcAuthenticator {

  /**
    * oidc client that has to be provided to oidc context, default is CommonLibConfig.AuthConfig.oauthClient and
    * can be provided in application config or through ENV OIDC_CLIENT
    */
  def oauthClient: String

  /**
    * issuer of oidc token has to be provided to oidc context, default is CommonLibConfig.AuthConfig.oidcIssuer and
    * is constructed from identityProviderUrl in application config and realm through ENV OIDC_REALM
    */
  def jwtIssuer: String

  /**
    * An optional location to retrieve a bearer token from file system
    * @return an optional location to a token file
    */
  def tokenTargetFileLocation: Option[String]

  /**
    * Retrieves a public key for the given jwk key id
    * @param kid The jwk key id from a bearer token
    * @return Return a public key or an exception wrapped as Try result
    */
  def publicKeyFromJwkKey(kid: String): PublicKey

  implicit val clock: Clock = Clock.systemUTC()

  /**
    * Authenticate a raw bearer token to get an OidcToken
    * When all auth checks passes then the function provide a positive validated OidcToken
    * @param bearerToken A raw JWT bearer token
    * @throws OidcAuthenticationException Exception which point ot an authentication error
    * @return OidcToken
    */
  def oidcToken(bearerToken: String = retrieveRawOidcTokenFromFile()): OidcToken = {

    val oidcJsonSupport = OidcSprayJson(oauthClient, bearerToken)

    oidcJsonSupport.jwtHeaderFromRawToken(bearerToken) match {
      case Some(h) =>
        val kid = h.keyId.get

        val publicKey: PublicKey = publicKeyFromJwkKey(kid)

        oidcJsonSupport.decode(bearerToken, publicKey) match {
          case Success(oidcToken) =>
            if (oidcToken.isValid(jwtIssuer, oauthClient)) {
              oidcToken
            } else
              throw new OidcAuthenticationException(
                s"Supplied token is invalid! Either provided oauth-client or jwt-issuer did not match with the expected values")
          case Failure(ex) =>
            throw new OidcAuthenticationException(
              s"Unable to decode provided token from provided raw-token and ${publicKey.getFormat} jwk-key",
              ex)
        }

      case None => throw new OidcAuthenticationException(s"Unable to decode header from provided raw-token")
    }
  }

  /**
    * Retrieves raw oidc token (usually populated by authentication side-car) from a configured file path.
    * Is configured through application config
    */
  private def retrieveRawOidcTokenFromFile(): String = {
    val source = Source.fromFile(tokenTargetFile, "utf-8")
    try source.getLines.mkString
    finally source.close()
  }

  /**
    * Path to file containing raw oidc token (usually populated by authentication side-car). Shall be configured through
    * application config
    */
  private def tokenTargetFile: String = tokenTargetFileLocation match {
    case Some(targetFile) => targetFile
    case _ =>
      throw new OidcConfigurationException(
        "oidc-token target file has to be provided through application config or env variable")
  }

}
