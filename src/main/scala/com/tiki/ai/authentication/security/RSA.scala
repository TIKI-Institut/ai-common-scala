package com.tiki.ai.authentication.security
import java.io.IOException
import java.net.URL
import java.security._
import java.security.spec.{InvalidKeySpecException, PKCS8EncodedKeySpec, X509EncodedKeySpec}
import java.util.Base64
import java.util.logging.Logger

import scala.io.Source

/**
  * Helper for creating a public key from a json web key
  */
object RSA {

  @transient private val log = Logger.getLogger(getClass.getName)

  private val ALGORITHM      = "RSA"
  private val DEFAULT_FORMAT = "UTF-8"

  /**
    * Converts a textual private key into PrivateKey
    *
    * @param pemLocation the location of the pem
    * @return a PrivateKey
    * @throws IOException if key location is wrong
    * @throws NoSuchAlgorithmException if java distribution lacks RSA algorithm
    * @throws InvalidKeySpecException if key has wrong format
    */
  @throws[IOException]
  @throws[NoSuchAlgorithmException]
  @throws[InvalidKeySpecException]
  def getPrivateKey(pemLocation: URL): PrivateKey = {
    log.finer("Loading private key from location: " + pemLocation)
    getRequestedKey(pemLocation, isPrivateKey = true).asInstanceOf[PrivateKey]
  }

  /**
    * Converts an textual public key into PublicKey
    *
    * @param pemLocation the location of the pem
    * @return a PublicKey
    * @throws IOException if key location is wrong
    * @throws NoSuchAlgorithmException if java distribution lacks RSA algorithm
    * @throws InvalidKeySpecException if key has wrong format
    */
  @throws[IOException]
  @throws[NoSuchAlgorithmException]
  @throws[InvalidKeySpecException]
  def getPublicKey(pemLocation: URL): PublicKey = {
    log.finer("Loading public key from location: " + pemLocation)
    getRequestedKey(pemLocation, isPrivateKey = false).asInstanceOf[PublicKey]
  }

  /**
    * Just requests a public or a private key
    *
    * @param pemLocation the location of the pem
    * @param isPrivateKey the request decision | Either public or private
    * @return Either a public or private key
    * @throws IOException if key location is wrong
    * @throws NoSuchAlgorithmException if java distribution lacks RSA algorithm
    * @throws InvalidKeySpecException if key has wrong format
    */
  @throws[IOException]
  @throws[NoSuchAlgorithmException]
  @throws[InvalidKeySpecException]
  private def getRequestedKey(pemLocation: URL, isPrivateKey: Boolean): Key = {
    assert(pemLocation != null)
    val key = Base64.getDecoder.decode(getKey(pemLocation))
    val kf  = KeyFactory.getInstance(ALGORITHM)
    if (isPrivateKey) {
      val spec = new PKCS8EncodedKeySpec(key)
      kf.generatePrivate(spec)
    } else {
      val spec = new X509EncodedKeySpec(key)
      kf.generatePublic(spec)
    }
  }

  @throws[IOException]
  private def getKey(pemLocation: URL) = {
    val source = Source.fromURL(pemLocation, DEFAULT_FORMAT)
    try {
      source
        .getLines()
        .filter(!_.contains("-----"))
        .reduce(_ concat _)
    } finally {
      source.close()
    }
  }
}
