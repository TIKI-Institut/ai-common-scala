package com.tiki.ai.authentication
import com.tiki.ai.authentication.oidc.OidcToken
import com.tiki.ai.dsp.SymbolProcessor
import com.tiki.ai.job.JobConfig
import scopt.OptionParser

import java.io.{File, PrintWriter}
import java.nio.charset.StandardCharsets
import java.util.logging.{Level, Logger}
import scala.collection.JavaConverters._
import scala.util.{Failure, Success}

trait Roles {
  def roles: Seq[String]
}

object Roles {
  def apply(rawRoles: String*): Roles = new Roles {
    override def roles: Seq[String] = rawRoles
  }

  def unapply(arg: Roles): Option[Seq[String]] = Some(arg.roles)
}

case object NoRoles extends Roles {
  override def roles: Seq[String] = Seq.empty
}

trait RoleChecking {
  def rolesSatisfied(oidcToken: OidcToken, roles: Seq[String]): Boolean = oidcToken match {
    case c if roles.forall(reqRole => c.realmRoles.roles.contains(reqRole) || c.clientRoles.roles.contains(reqRole)) =>
      true
    case _ => false
  }
}

object RolesListCommand {

  @transient private val log = Logger.getLogger(getClass.getName)

  private lazy val buildRoleListJson: String = collectRoles
    .map("\"%s\"".format(_))
    .mkString("[", ", ", "]")

  private val collectRoles = SymbolProcessor.resources {
    case Success(scanner) =>
      val allRoles: Seq[String] = scanner
        .getClassesWithAnnotation(classOf[RequiredRoles].getCanonicalName)
        .asScala
        .flatMap { classInfo =>
          val requiredRolesParams = classInfo.getAnnotationInfo(classOf[RequiredRoles].getCanonicalName).getParameterValues
          requiredRolesParams.getValue("roles").asInstanceOf[Array[String]]
        }
      scanner.close()
      allRoles.distinct
    case Failure(exception) =>
      throw exception
  }

  /**
    * Lists all the roles from all the classes annotated with [[RequiredRoles]]. Automatically exits programm unless
    * configured otherwise
    *
    * @param args pass program arguments so that list roles can identify if the 'roles' command shall be invoked
    * @param autoExit automatically exits program. If set to false, will not exit (currently only for tests)
    */
  def listRoles(args: Seq[String], autoExit: Boolean = true): Unit = {
    case class RoleInterceptor(name: String = "", dumpRoles: Boolean = false, file: Option[String] = None) extends JobConfig
    val interceptConfig = RoleInterceptor()

    val roleInterceptParser = new OptionParser[RoleInterceptor]("") {
      override def errorOnUnknownArgument            = false
      override def showUsageOnError: Option[Boolean] = Some(false)
      override def reportWarning(msg: String): Unit  = { /* nnoop */ }

      cmd("roles")
        .action((_, c) => c.copy(dumpRoles = true))
        .text("dump roles either to stderr or to the supplied file path")
        .children(opt[String]('f', "file") optional () valueName "</path/to/file>" action ((f, c) => c.copy(file = Some(f))))
    }

    roleInterceptParser.parse(args, interceptConfig) match {
      case Some(config) =>
        if (config.dumpRoles) {
          config.file match {
            case Some(filePath) =>
              try {
                val file = new File(filePath)
                file.getParentFile.mkdirs()
                new PrintWriter(file, StandardCharsets.UTF_8.name()) {
                  write(s"""{"roles": $buildRoleListJson}"""); close()
                }
                if (autoExit) sys.exit()
              } catch {
                case ex: Throwable => log.log(Level.SEVERE, s"Failed to write roles list.", ex)
              }
            case _ =>
              println(s"""{"roles": $buildRoleListJson}""")
              if (autoExit) sys.exit()
          }
        }
      case _ => // noop
    }
  }
}
