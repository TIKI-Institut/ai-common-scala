package com.tiki.ai.authentication.web

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.tiki.ai.authentication.oidc.OidcAuthenticationException
import spray.json.{DefaultJsonProtocol, JsonParser, RootJsonFormat}

import java.security.PublicKey
import java.util.Base64
import scala.io.Source
import scala.util.Try

/**
  * The json definition -> to be spray json conform
  */
trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val jwkFormat: RootJsonFormat[Jwk]                                       = jsonFormat6(Jwk)
  implicit val jwkCertificateResponseFormat: RootJsonFormat[JwkCertificateResponse] = jsonFormat1(JwkCertificateResponse)
}

/**
  * An open id jwk certificate response representation
  */
case class JwkCertificateResponse(keys: Seq[Jwk])

/**
  * The jwk certificate representation
  */
case class Jwk(kid: String, kty: String, alg: String, use: String, n: String, e: String)

trait Jwks extends JsonSupport {

  def getPublicKeyFromJWKS(jwksUrl: String, kid: String): PublicKey =
    Try(publicKeyFromJwks(jwksUrl, kid)).recover {
      case ex =>
        throw new IllegalArgumentException(
          s"A public key could not be retrieved from the configured url [$jwksUrl]. Cause: $ex")
    }.get

  /**
    * Match the kid from the token header to find the correct JWK signer
    */
  private def publicKeyFromJwks(jwksUrl: String, kid: String): PublicKey = {
    val jwKKeys = retrieveJwksCertificate(jwksUrl).keys.filter(_.kid == kid)

    if (jwKKeys.length != 1) {
      throw new OidcAuthenticationException(s"JWK key could not be retrieved for kid: $kid")
    }

    publicKeyFromJwk(jwKKeys.head)
  }

  /**
    * Fetches the actual configured realm specific jwks certificate from an identity provider
    */
  private def retrieveJwksCertificate(url: String): JwkCertificateResponse = {
    val source = Source.fromURL(url);
    try { JsonParser(source.mkString).convertTo[JwkCertificateResponse] } finally { source.close() }
  }

  private def publicKeyFromJwk(jwk: Jwk): PublicKey = {
    import java.math.BigInteger
    import java.security.KeyFactory
    import java.security.spec.RSAPublicKeySpec

    val mod: String      = jwk.n
    val exponent: String = jwk.e

    val modulus        = new BigInteger(1, Base64.getUrlDecoder.decode(mod))
    val publicExponent = new BigInteger(1, Base64.getUrlDecoder.decode(exponent))
    val kf             = KeyFactory.getInstance(jwk.kty)

    kf.generatePublic(new RSAPublicKeySpec(modulus, publicExponent))
  }

}
