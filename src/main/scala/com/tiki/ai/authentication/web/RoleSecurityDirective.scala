package com.tiki.ai.authentication.web

import akka.http.scaladsl.model.headers.HttpChallenges
import akka.http.scaladsl.server.AuthenticationFailedRejection.CredentialsMissing
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Directive1, _}
import com.tiki.ai.authentication.RoleChecking
import com.tiki.ai.authentication.oidc.OidcToken
import com.tiki.ai.config.CommonLibConfig

import scala.language.postfixOps

/**
  * The security directives describes how to
  */
trait RoleSecurityDirective extends RoleChecking {

  private lazy val authConfig = CommonLibConfig.AuthConfig

  def authorize: Directive1[OidcToken]

  def requireAllRoles(roles: Seq[String]): Directive1[OidcToken] =
    authorize flatMap {
      case oidcToken if oidcToken != null && rolesSatisfied(oidcToken, roles) =>
        provide(oidcToken)
      case null =>
        reject(AuthenticationFailedRejection(CredentialsMissing, HttpChallenges.oAuth2(authConfig.oauthClient)))
          .toDirective[Tuple1[OidcToken]]
      case _ =>
        reject(AuthorizationFailedRejection).toDirective[Tuple1[OidcToken]]
    }
}
