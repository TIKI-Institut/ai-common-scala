package com.tiki.ai.authentication.web

import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Rejection, RejectionHandler}

case object BearerTokenNotFoundRejection extends Rejection

trait HttpRejectionHandler {

  lazy val tokenMissingRejectionHandler: RejectionHandler = RejectionHandler
    .newBuilder()
    .handle {
      case BearerTokenNotFoundRejection =>
        complete(
          HttpResponse(StatusCodes.Unauthorized,
                       entity = "Authentication has failed since no valid auth token was provided."))
    }
    .result()
}
