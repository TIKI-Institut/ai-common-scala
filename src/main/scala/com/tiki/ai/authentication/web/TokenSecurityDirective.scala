package com.tiki.ai.authentication.web

import akka.http.scaladsl.model.headers.{Authorization, OAuth2BearerToken}
import akka.http.scaladsl.server.Directives.{optionalCookie, optionalHeaderValueByType, parameters, provide, reject, _}
import akka.http.scaladsl.server._
import com.tiki.ai.akka.AkkaContext
import com.tiki.ai.authentication.oidc.{OidcContext, OidcToken}

import java.util.logging.{Level, Logger}
import scala.concurrent.Future
import scala.language.postfixOps

object TokenSecurityDirective {
  final val codeParameterName: String        = "code"
  final val destinationParameterName: String = "destination"
  final val cookieName: String               = "X-AUTH-TOKEN"
}

/**
  * The auth2 specific browser flow specification
  * which provides an additional oidc callback for
  * authorization code flow
  */
trait TokenSecurityDirective extends RoleSecurityDirective with OidcContext with HttpRejectionHandler {
  self: AkkaContext =>

  @transient private val log = Logger.getLogger(getClass.getName)

  import com.tiki.ai.authentication.web.TokenSecurityDirective._

  override def authorize: Directive1[OidcToken] = bearerToken.flatMap { rawToken =>
    onComplete(parseToken(rawToken)).flatMap {
      _.map { oidcToken =>
        log.fine(s"User [${oidcToken.subject}] successfully authorized")
        provide(oidcToken)
      }.recover {
          case ex =>
            log.log(Level.SEVERE, "Authorization Token could not be verified", ex)
            reject(AuthorizationFailedRejection).toDirective[Tuple1[OidcToken]]
        }
        .getOrElse(reject(AuthorizationFailedRejection).toDirective[Tuple1[OidcToken]])
    }
  }

  def parseToken(token: String): Future[OidcToken] = Future(oidcToken(token))

  def bearerToken: Directive1[String] = handleRejections(tokenMissingRejectionHandler).tflatMap { _ =>
    bearerTokenFromHeader | bearerTokenFromCookie | bearerTokenFromParameter
  }

  private def bearerTokenFromHeader: Directive1[String] =
    optionalHeaderValueByType[Authorization](classOf[Authorization]).flatMap {
      case Some(authHeader) =>
        authHeader match {
          case Authorization(OAuth2BearerToken(token)) => provide(token)
          case _                                       => reject(BearerTokenNotFoundRejection)
        }
      case _ => reject(BearerTokenNotFoundRejection)
    }

  private def bearerTokenFromCookie: Directive1[String] = optionalCookie(cookieName).flatMap {
    case Some(cookie) => provide(cookie.value)
    case _            => reject(BearerTokenNotFoundRejection)
  }

  private def bearerTokenFromParameter: Directive1[String] = parameters(codeParameterName).recover {
    case MissingQueryParamRejection(_) :: Nil => reject(BearerTokenNotFoundRejection)
  }
}
