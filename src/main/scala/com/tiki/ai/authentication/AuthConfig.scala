package com.tiki.ai.authentication

import com.tiki.ai.authentication.oidc.OidcAuthenticationException
import com.tiki.ai.config.ConfigHolder

trait AuthConfig {
  self: ConfigHolder =>

  private lazy val authConf = config.getConfig(AuthConfigKeys.AUTH_BLOCK)

  object AuthConfig {
    lazy val dspWebDeploymentPath: String = authConf.getString(AuthConfigKeys.DSP_WEB_DEPLOYMENT_PATH)
    lazy val dspName: String              = authConf.getString(AuthConfigKeys.DSP_NAME).toLowerCase
    lazy val dspEnvironment: String       = authConf.getString(AuthConfigKeys.DSP_ENVIRONMENT).toLowerCase
    lazy val dspFlavor: String            = authConf.getString(AuthConfigKeys.DSP_FLAVOR).toLowerCase
    lazy val dspFlavorVersion: String     = authConf.getString(AuthConfigKeys.DSP_FLAVOR_VERSION).toLowerCase
    lazy val dspPrincipal: String         = authConf.getString(AuthConfigKeys.DSP_PRINCIPAL).toLowerCase
    lazy val dspPod: String               = authConf.getString(AuthConfigKeys.DSP_POD).toLowerCase
    lazy val dspNamespace: String         = authConf.getString(AuthConfigKeys.DSP_NAMESPACE).toLowerCase
    lazy val jwtIssuer: String            = authConf.getString(AuthConfigKeys.JWT_ISSUER).toLowerCase
    lazy val jwkUrl: String               = authConf.getString(AuthConfigKeys.JWK_URL)
    lazy val oauthClient: String = authConf
      .getOptionalString(AuthConfigKeys.OAUTH_CLIENT)
      .getOrElse(throw new OidcAuthenticationException(
        s"An OAuth2 client could not be determined, impossible to authenticate. Please contact admin of this application."))
    lazy val tokenTargetFile: Option[String] = authConf.getOptionalString(AuthConfigKeys.TOKEN_TARGET_FILE)
  }

  private object AuthConfigKeys {
    val AUTH_BLOCK              = "authentication"
    val DSP_PRINCIPAL           = "dspPrincipal"
    val DSP_NAME                = "dspName"
    val DSP_ENVIRONMENT         = "dspEnvironment"
    val DSP_FLAVOR              = "dspFlavor"
    val DSP_FLAVOR_VERSION      = "dspFlavorVersion"
    val DSP_POD                 = "dspPod"
    val DSP_NAMESPACE           = "dspNamespace"
    val DSP_WEB_DEPLOYMENT_PATH = "dspWebDeploymentPath"
    val OAUTH_CLIENT            = "oauthClient"
    val JWT_ISSUER              = "jwtIssuer"
    val JWK_URL                 = "jwkUrl"
    val TOKEN_TARGET_FILE       = "tokenTargetFile"
  }

}
