package com.tiki.ai.dw

import com.typesafe.config.{Config, ConfigFactory}

object Configuration {
  lazy private val configHolder = ConfigFactory.load().getConfig("tiki")
  lazy val dw: Config           = configHolder.getConfig("dw")
  lazy val authConfig: Config   = configHolder.getConfig("authentication")

  lazy val dwService: String       = dw.getString("service")
  lazy val tokenTargetFile: String = authConfig.getString("tokenTargetFile")
}
