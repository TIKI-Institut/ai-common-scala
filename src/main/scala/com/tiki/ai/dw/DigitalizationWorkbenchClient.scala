package com.tiki.ai.dw

import org.joda.time.DateTime
import sttp.client3.sprayJson.{asJson, _}
import sttp.client3.{HttpURLConnectionBackend, Identity, RequestBody, SttpBackend, UriContext, asByteArray, basicRequest, multipart, multipartFile}
import sttp.model.{Part, StatusCode}

import java.io.File
import java.util.logging.Logger
import scala.annotation.tailrec
import scala.concurrent.duration.{Duration, _}
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}


case class DigitalizationWorkbenchClient(serviceURL: String = Configuration.dwService)
  extends JsonMarshaller {

  lazy val backend: SttpBackend[Identity, Any] = HttpURLConnectionBackend()

  val logger: Logger = Logger.getLogger(getClass.getName)


  private def get(url: String)(implicit bc: BearerContext) = {
    basicRequest.header("Authorization", s"Bearer ${bc.token}").get(uri"$url")
  }

  private def post(url: String, source: String, destination: String)(implicit bc: BearerContext) = {
    basicRequest
      .header("Authorization", s"Bearer ${bc.token}")
      .post(uri"$url")
      .body(RequestPayload(source, destination, overwrite = true))
  }

  private def post(url: String, source: String)(implicit bc: BearerContext) = {
    basicRequest
      .header("Authorization", s"Bearer ${bc.token}")
      .post(uri"$url")
      .body(RequestSinglePayload(source))
  }

  def fs()(implicit bc: BearerContext): Try[Seq[String]] = {
    val response = get(serviceURL + "/fs").response(asJson[Seq[String]]).send(backend)

    returnBasedOnResponseCode(response.code, response.body.right.get)
  }

  def list(source: String)(implicit bc: BearerContext): Try[Seq[ListResult]] = {
    val response = post(serviceURL + s"/list", source).response(asJson[Seq[ListResult]]).send(backend)

    returnBasedOnResponseCode(response.code, response.body.right.get)
  }

  def delete(source: String)(implicit bc: BearerContext): Try[Unit] = {
    val response = post(serviceURL + s"/delete", source).send(backend)

    returnBasedOnResponseCode(response.code)
  }

  def copy(source: String, destination: String, wait: Boolean,
           maximalWait: Duration = 5.minutes, updateWait: Duration = 10.seconds)
          (implicit bc: BearerContext): Try[Unit] =
    moveOrCopy(source, destination, wait, s"/copy", maximalWait, updateWait)

  def move(source: String, destination: String, wait: Boolean,
           maximalWait: Duration = 5.minutes, updateWait: Duration = 10.seconds)
          (implicit bc: BearerContext): Try[Unit] =
    moveOrCopy(source, destination, wait, s"/move", maximalWait, updateWait)

  private def moveOrCopy(source: String, destination: String, wait: Boolean, operation: String,
                         maximalWait: Duration, updateWait: Duration)
                        (implicit bc: BearerContext): Try[Unit] = {
    val response = post(serviceURL + operation, source, destination).response(asJson[TaskResult]).send(backend)

    (response.code, wait) match {
      case (code, _) if code != StatusCode.Ok => Failure(ClientException(s"unexpected status ${response.code.code} code"))
      case (_, true) => val taskID: Int = response.body.right.get.taskId; waitForCompletion(taskID, maximalWait, updateWait)
      case _ => Success(())
    }
  }

  def taskStatus(taskID: Int)(implicit bc: BearerContext): Try[String] = {
    val response = get(serviceURL + s"/task/$taskID").response(asJson[TaskResult]).send(backend)

    returnBasedOnResponseCode(response.code, response.body.right.get.status)
  }

  def download(source: String)(implicit bc: BearerContext): Try[Array[Byte]] = {
    val response = post(serviceURL + s"/download", source).response(asByteArray).send(backend)

    returnBasedOnResponseCode(response.code, response.body.right.get)
  }

  def upload(to: String, files: Seq[File], overwrite: Boolean = false)(implicit bc: BearerContext): Try[Unit] = {
    val url = serviceURL + s"/upload"

    val request = basicRequest
      .header("Authorization", s"Bearer ${bc.token}")
      .post(uri"$url")

    val requestBody: Seq[Part[RequestBody[Any]]] = Seq(
      multipart("to", to),
      multipart("overwrite", overwrite.toString),
    ) ++ files.map(file => multipartFile("files", file))

    val response = request.multipartBody(requestBody.head, requestBody.tail: _*).send(backend)

    returnBasedOnResponseCode(response.code)
  }

  def mkDir(source: String)(implicit bc: BearerContext): Try[Unit] = {
    val response = post(serviceURL + s"/mkdir", source).send(backend)

    returnBasedOnResponseCode(response.code)
  }

  private def returnBasedOnResponseCode[T](code: StatusCode, value: => T): Try[T] =
    if (code == StatusCode.Ok) Success(value)
    else Failure(ClientException(s"unexpected status $code code on operation"))

  private def returnBasedOnResponseCode(code: StatusCode): Try[Unit] = returnBasedOnResponseCode(code, ())

  @tailrec
  private def waitForCompletion(taskID: Int,
                                maximalWait: Duration = 5.minutes,
                                updateWait: Duration = 10.seconds,
                                startTime: DateTime = DateTime.now()
                               )(implicit bc: BearerContext): Try[Unit] = {

    logger.info(s"wait $updateWait seconds for result")

    Thread.sleep(updateWait.toMillis)

    logger.info(s"fetch task result for task id $taskID")

    taskStatus(taskID) match {
      case Success(TaskStatus.StatusDone) => logger.info(s"task is in done state")
        Success(())
      case _ if DateTime.now().getMillis - startTime.getMillis > maximalWait.toMillis =>
        logger.warning(s"task $taskID has not yet complete, aborting wait")
        Failure(ClientException(s"task $taskID has not yet complete, aborting wait"))
      case Success(TaskStatus.StatusRunning) => logger.info(s"task is in running state")
        waitForCompletion(taskID, maximalWait, updateWait, startTime)
      case Success(TaskStatus.StatusCreated) => logger.info(s"task is in created state")
        waitForCompletion(taskID, maximalWait, updateWait, startTime)
      case Failure(exception) => logger.warning(s"Fetching of status $taskID failed, task may still complete")
        Failure(exception)
      case _ =>  logger.warning(s"task $taskID is in unknown state")
        Failure(ClientException(s"task $taskID is in unknown state"))
    }
  }
}