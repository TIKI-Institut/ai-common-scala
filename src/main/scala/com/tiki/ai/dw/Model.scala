package com.tiki.ai.dw

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

case class ClientException(msg: String) extends Exception(msg)

// {"name":"aoi-axi-failure-analysis","size":0,"isDir":true,"fsType":"smb","lastModified":1623326013}
case class ListResult(name: String, isDir: Boolean, fsType: String, lastModified: Int)

// {"taskId":3,"operation":"MOVE","state":"ERROR","source":"smb:/","destination":"hdfs:/","username":"tiki-tfrischholz","principal":"zol","created":1625570252
case class TaskResult(taskId: Int,
                      operation: String,
                      status: String,
                      from: String,
                      to: Option[String],
                      overwrite: Boolean,
                      username: Option[String],
                      principal: String,
                      created: Long)

case class RequestPayload(from: String, to: String, overwrite: Boolean)
case class RequestSinglePayload(uri: String)

trait JsonMarshaller extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val listResultFormat: RootJsonFormat[ListResult] = jsonFormat4(ListResult)
  implicit val taskResultFormat: RootJsonFormat[TaskResult] = jsonFormat9(TaskResult)
  implicit val requestPayloadFormat: RootJsonFormat[RequestPayload] = jsonFormat3(RequestPayload)
  implicit val requestSinglePayloadFormat: RootJsonFormat[RequestSinglePayload] = jsonFormat1(RequestSinglePayload)
}

object TaskStatus {
  val StatusCreated = "CREATED"
  val StatusRunning = "RUNNING"
  val StatusDone    = "DONE"
  val StatusError   = "ERROR"
  val StatusAborted = "ABORTED"
  val StatusStopped = "STOPPED"
}
