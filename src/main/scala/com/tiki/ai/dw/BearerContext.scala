package com.tiki.ai.dw

import scala.io.Source

trait BearerContext {
  def token: String
}

object FileBearerContext extends BearerContext {
  override def token: String = {
    val source = Source.fromFile(Configuration.tokenTargetFile)
    val result = source.getLines().mkString("")
    source.close()
    result
  }
}
