package com.tiki.ai.sys

trait Exit {
    lazy val NORMAL_TERMINATION: Int   = 0
    lazy val ABNORMAL_TERMINATION: Int = -1
    def exit(code: Int): Unit = sys.exit(code)
}

object Exit extends Exit
