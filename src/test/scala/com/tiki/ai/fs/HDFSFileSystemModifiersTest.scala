
package com.tiki.ai.fs

import com.tiki.ai.hadoop.HadoopConfigHolder
import org.apache.hadoop.conf.Configuration
import org.scalatest.FlatSpec
import org.scalatest.Matchers.convertToAnyShouldWrapper

class HDFSFileSystemModifiersTest extends FlatSpec with FileSystemsSupport with HadoopConfigHolder {

  "The HDFSFileSystemModifiers" should "correctly modify the FS implementation Hadoop config" in {

    val conf: Configuration = new Configuration()
    applyHadoopConfig(conf)

    conf.get("fs.abfss.impl") shouldBe "shaded.databricks.azurebfs.org.apache.hadoop.fs.azurebfs.SecureAzureBlobFileSystem"
    conf.get("fs.wasbs.impl") shouldBe "org.apache.hadoop.fs.azure.NativeAzureFileSystem$Secure"
  }
}
