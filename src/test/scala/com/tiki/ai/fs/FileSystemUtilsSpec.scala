package com.tiki.ai.fs

import com.tiki.ai.hadoop.HadoopConfigHolder
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

import java.io.IOException

class FileSystemUtilsSpec
    extends FlatSpec
    with Matchers
    with BeforeAndAfterAll
    with FileSystemUtils
    with HadoopConfigHolder {

  val input: FileSystemLocation   = FileSystemLocation(s"localFsUtils|copyMerge")
  def uid: String                 = java.util.UUID.randomUUID.toString
  val tempDir: FileSystemLocation = FileSystemLocation(s"temp|$uid")

  it should "copyMerge multiple files" in {

    input.locate("multipleFiles").copy(input.locate("copiedMultipleFiles"))
    copyMerge(input.locate("multipleFiles"), tempDir.locate("singleFile.csv"), deleteSource = false, overwriteTarget = true)
    tempDir.locate("singleFile.csv").exists() shouldBe true

    copyMerge(input.locate("copiedMultipleFiles"),
              tempDir.locate("singleFile.csv"),
              deleteSource = true,
              overwriteTarget = true)
    tempDir.locate("singleFile.csv").exists() shouldBe true
    input.locate("copiedMultipleFiles").exists() shouldBe false
  }

  it should "copyMerge with single file as input" in {
    val res = copyMerge(input.locate("singleFile"), tempDir.locate("test.csv"), deleteSource = true, overwriteTarget = true)
    res shouldBe false
  }

  it should "copyMerge with existing target should fail" in {
    an[IOException] should be thrownBy {
      copyMerge(input.locate("multipleFiles"), input.locate("singleFile"), deleteSource = false, overwriteTarget = false)
    }
  }

  override def afterAll(): Unit = {
    tempDir.delete()
  }

}
