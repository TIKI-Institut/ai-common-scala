package com.tiki.ai.fs

import com.tiki.ai.hadoop.HadoopConfigHolder
import org.apache.hadoop.fs.Path
import org.scalatest.{FlatSpec, GivenWhenThen, Inside, Matchers}

class FileSystemLocationTest extends FlatSpec with GivenWhenThen with Matchers with Inside with FileSystemsSupport with HadoopConfigHolder {

  private[this] def assert(expect: FileSystemLocation, test: FileSystemLocation): Unit = {
    inside(test) {
      case FsLocation(alias, path) =>
        alias should be(expect.fsAlias)
        new Path(path).toString should be(expect.path.toString)
    }
  }

  private[this] def assertResolved(expect: String, test: FileSystemLocation): Unit = test.resolve.toString should be(expect)

  it should "not accept an absolute FS location" in {
    assertThrows[AssertionError] {
      FileSystemLocation("models", "/dir/asdf")
    }
  }

  it should "parse a FS location correctly" in {
    assert("test|path", FileSystemLocation("test", "path"))
    assert("file:/C:/tmp/dir2/test/blub.txt", FileSystemLocation(null, "file:/C:/tmp/dir2/test/blub.txt"))

    assertThrows[AssertionError] {
      FileSystemLocation("models/dir/asfd")
    }
  }

  it should "parse FS root location correctly" in {
    assert("test|.", FileSystemLocation("test", "."))
    assert("test|", FileSystemLocation("test", "."))
    assert("test|", FileSystemLocation.root("test"))
    assert(FileSystemLocation.root("test"), "test|")
  }

  it should "stringify a FS location correctly" in {
    FileSystemLocation(null, "file:/C:/tmp/dir2/test/blub.txt").toString should be("file:/C:/tmp/dir2/test/blub.txt")
    FileSystemLocation("abc", "test/blub.txt").toString should be("abc|test/blub.txt")
  }

  it should "correctly append paths during 'locate'" in {
    FileSystemLocation(null, "file:/C:/tmp/dir2/").locate("test/blub.txt").toString should be(
      "file:/C:/tmp/dir2/test/blub.txt")
    FileSystemLocation(null, "file:/C:/tmp/dir2").locate("test/blub.txt").toString should be(
      "file:/C:/tmp/dir2/test/blub.txt")
    FileSystemLocation(null, "file:/C:/tmp/dir2").locate("test/").locate("blub.txt").toString should be(
      "file:/C:/tmp/dir2/test/blub.txt")
    FileSystemLocation(null, "file:/C:/tmp/dir2").locate("test").locate("blub.txt").toString should be(
      "file:/C:/tmp/dir2/test/blub.txt")
    FileSystemLocation(null, "file:/C:/tmp/dir2")
      .locate("test")
      .locate("test")
      .locate("..")
      .locate("blub.txt")
      .toString should be("file:/C:/tmp/dir2/test/blub.txt")
  }

  it should "parse s3a root location correctly " in {
    assertResolved("s3a://no-path-bucket/testfile.txt", FileSystemLocation("s3a_noPath", "testfile.txt"))
    assertResolved("s3a://no-path-bucket/", FileSystemLocation.root("s3a_noPath"))
  }

  it should "parse the Azure DLS2 storage location" in {
    assertResolved("abfss://test-container@test-storage-account-name.dfs.core.windows.net/testDirectory", FileSystemLocation("abfss", "testDirectory"))
  }

  it should "parse the Azure Blob storage location" in {
    assertResolved("wasbs://test-container@test-storage-account-name.blob.core.windows.net/testDirectory", FileSystemLocation("wasbs", "testDirectory"))
  }

  it should "parse the sftp location" in {
    val sftpFsLocation = FileSystemLocation("sftp", "testDirectory")
    sftpFsLocation.resolve.toString should be ("sftp://test-host:22/testDirectory")
    hadoopConfiguration.get("fs.sftp.impl") should be ("org.apache.hadoop.fs.sftp.SFTPFileSystem")
    hadoopConfiguration.get("fs.sftp.keyfile") should be ("/opt/id_rsa")
    hadoopConfiguration.get(f"fs.sftp.user.test-host") should be ( "tiki")
    hadoopConfiguration.get(f"fs.sftp.password.test-host.tiki") should be ( "")
  }

}
