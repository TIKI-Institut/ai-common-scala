package com.tiki.ai.fs

import com.tiki.ai.hadoop.HadoopConfigHolder
import org.scalatest.{FlatSpec, Matchers}
import org.apache.hadoop.fs.sftp.SFTPFileSystem

class SFTPFileSystemTest extends FlatSpec with Matchers with FileSystemsSupport with HadoopConfigHolder {

  // Tests shaded packages prefix https://tiki-institut.atlassian.net/browse/TIKIDSP-2545
  "Hadoop SFTPFileSystem" should "be initialized without error" in {
    val fs = new SFTPFileSystem()
    fs should not be null
  }

}
