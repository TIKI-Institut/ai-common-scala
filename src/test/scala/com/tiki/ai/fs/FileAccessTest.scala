package com.tiki.ai.fs

import java.io._

import com.tiki.ai.hadoop.HadoopConfigHolder
import org.scalatest.concurrent.PatienceConfiguration.{Interval, Timeout}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, FlatSpec, GivenWhenThen, Matchers}

import scala.concurrent.{ExecutionContext, Future}

case class StoringObject(map: Map[Long, String], strings: Seq[String], string: String, id: Long)

class FileAccessTest
    extends FlatSpec
    with GivenWhenThen
    with Matchers
    with ScalaFutures
    with BeforeAndAfterAll
    with FileSystemsSupport
    with HadoopConfigHolder {

  val uid: String            = java.util.UUID.randomUUID.toString.replaceAll("-", "")
  val testDirContext: String = s"target/$uid"

  val fs: FileSystemDescriptor = FileSystems("localFS")

  override def afterAll(): Unit = {
    // clean-up
    fs.delete(testDirContext)
  }

  it should "store the data correctly (using streams)" in {
    fs.open(s"$testDirContext/test.txt").write { oStream =>
      val writer = new BufferedWriter(new OutputStreamWriter(oStream))
      writer.write(uid)
      writer.flush()
    }

    fs.open(s"$testDirContext/test.txt").read { iStream =>
      val reader  = new BufferedReader(new InputStreamReader(iStream))
      val testUid = reader.readLine()
      testUid should be(uid)
    }
  }

  it should "store the data correctly (using object serialization)" in {

    val testee = StoringObject(Map(2L -> "BLUB"), Seq("Hallo"), "Fuzz", 667)

    fs.open(s"$testDirContext/obj.dat").writeObject(testee)

    val fromFile = fs.open(s"$testDirContext/obj.dat").readObject[StoringObject]

    fromFile.map.size should be(1)
    fromFile.map.head should be(2L -> "BLUB")

    fromFile.strings.size should be(1)
    fromFile.strings.head should be("Hallo")

    fromFile.string should be("Fuzz")
    fromFile.id should be(667)
  }

  it should "read and write text files" in {

    val text = s"""Do you know the film:\n"In my experience there is no such thing as luck."\n\n\tCorrect answer:""" +
      "\nObi Wan Kenobi from Star Wars\n\näöasdfwelgv12wer09wöl01mfß"

    fs.open(s"$testDirContext/test.txt").writeText(text)

    val readText = fs.open(s"$testDirContext/test.txt").readText

    readText shouldBe text
  }

  it should "read and write text files in parallel" in {

    import scala.concurrent.ExecutionContext.Implicits._

    val fileCount = 50
    val content   = "This is just an example file count"

    object FileUtil {
      //noinspection UnitInMap
      def writeFiles(count: Int)(implicit ec: ExecutionContext): Future[Boolean] =
        Future {
          (1 to count).map(c => fs.open(s"$testDirContext/$c.txt").writeText(content)).forall(_ => true)
        }(ec)

      def readFiles(count: Int)(implicit ec: ExecutionContext): Future[Seq[String]] =
        Future {
          (1 to count).map(c => fs.open(s"$testDirContext/$c.txt").readText)
        }(ec)
    }

    whenReady(FileUtil.writeFiles(fileCount), Timeout(Span(10, Seconds)), Interval(Span(500, Millis))) { result =>
      result shouldBe true
    }

    whenReady(FileUtil.readFiles(fileCount), Timeout(Span(10, Seconds)), Interval(Span(500, Millis))) { fileContents =>
      fileContents.length shouldBe fileCount
      fileContents.head shouldBe content
      fileContents.last shouldBe content
    }
  }
}
