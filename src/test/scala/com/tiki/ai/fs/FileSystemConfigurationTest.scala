package com.tiki.ai.fs

import com.tiki.ai.hadoop.HadoopConfigHolder
import org.scalatest.{FlatSpec, GivenWhenThen, Inside, Matchers}

class FileSystemConfigurationTest extends FlatSpec with GivenWhenThen with Matchers with Inside with FileSystemsSupport with HadoopConfigHolder {

  it should "configure hadoop fs correctly for s3a with path access mode" in {
    hadoopConfiguration.get("fs.s3a.bucket.path-bucket.endpoint") shouldBe "s3.eu-central-1.amazonaws.com"
    hadoopConfiguration.get("fs.s3a.bucket.path-bucket.access.key") shouldBe "enter-access-key-here"
    hadoopConfiguration.get("fs.s3a.bucket.path-bucket.secret.key") shouldBe "enter-secret-key-here"
    hadoopConfiguration.get("fs.s3a.bucket.path-bucket.connection.ssl.enabled") shouldBe "true"
    hadoopConfiguration.get("fs.s3a.bucket.path-bucket.path.style.access") shouldBe "true"
  }

  it should "configure hadoop fs correctly for s3a without path access mode" in {
    hadoopConfiguration.get("fs.s3a.bucket.no-path-bucket.endpoint") shouldBe "s3.eu-central-1.amazonaws.com"
    hadoopConfiguration.get("fs.s3a.bucket.no-path-bucket.access.key") shouldBe "enter-access-key-here"
    hadoopConfiguration.get("fs.s3a.bucket.no-path-bucket.secret.key") shouldBe "enter-secret-key-here"
    hadoopConfiguration.get("fs.s3a.bucket.no-path-bucket.connection.ssl.enabled") shouldBe "true"
    hadoopConfiguration.get("fs.s3a.bucket.no-path-bucket.path.style.access") shouldBe "false"
  }
}
