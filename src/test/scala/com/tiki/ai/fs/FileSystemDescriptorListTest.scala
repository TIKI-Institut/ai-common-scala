package com.tiki.ai.fs

import com.tiki.ai.hadoop.HadoopConfigHolder
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}

class FileSystemDescriptorListTest
    extends FlatSpec
    with GivenWhenThen
    with Matchers
    with FileSystemsSupport
    with HadoopConfigHolder {

  it should "store the data correctly (using streams)" in {

    val fs = FileSystems("localFsList")

    val myListA     = fs.list(".")
    val myListAPath = myListA.map(status => status.getPath.toString).sorted

    myListA.length shouldBe 2
    myListA.foreach { status =>
      status.getPath.toString should startWith("file:/")
      status.isDirectory shouldBe true
    }
    myListAPath(0) should endWith("src/test/resources/com/tiki/ai/fs/list/tree_1")
    myListAPath(1) should endWith("src/test/resources/com/tiki/ai/fs/list/tree_2")

    val myListB     = fs.list("tree_2")
    val myListBPath = myListB.map(status => status.getPath.toString).sorted

    myListB.length shouldBe 2
    myListB.foreach { status =>
      status.getPath.toString should startWith("file:/")
    }
    myListBPath(0) should endWith("src/test/resources/com/tiki/ai/fs/list/tree_2/5_bytes.txt")
    myListBPath(1) should endWith("src/test/resources/com/tiki/ai/fs/list/tree_2/tree_4")
  }

}
