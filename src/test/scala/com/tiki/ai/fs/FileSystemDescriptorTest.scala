package com.tiki.ai.fs

import com.tiki.ai.hadoop.HadoopConfigHolder

import java.io._
import org.apache.hadoop.fs.Path
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}

class FileSystemDescriptorTest extends FlatSpec with GivenWhenThen with Matchers with HadoopConfigHolder {

  class LocalFileSystemDescriptorWithoutRootPathAndHost extends FileSystemDescriptor {
    override lazy val basePath: Path = new Path("file", null, "/")
  }

  class LocalFileSystemDescriptorWithRootPath extends LocalFileSystemDescriptorWithoutRootPathAndHost {
    override lazy val basePath: Path = new Path("file", null, "/rootPath/subdir")
  }

  class LocalFileSystemDescriptorWithRootPathAndHost extends LocalFileSystemDescriptorWithoutRootPathAndHost {
    override lazy val basePath: Path = new Path("file", "localhorst", "/rootPath/subdir")
  }

  class CustomRemoteFileSystem extends FileSystemDescriptor {
    override lazy val basePath: Path = new Path("totallyCustom", "someServer:666", "/rootPath")
  }

  it should "return the correct URI" in {
    new LocalFileSystemDescriptorWithRootPath()
      .resolve(new Path("dir/file.txt"))
      .toString should be("file:/rootPath/subdir/dir/file.txt")
    new LocalFileSystemDescriptorWithRootPathAndHost()
      .resolve(new Path("dir/file.txt"))
      .toString should be("file://localhorst/rootPath/subdir/dir/file.txt")
    new CustomRemoteFileSystem().resolve(new Path("dir/file.txt")).toString should be(
      "totallyCustom://someServer:666/rootPath/dir/file.txt")
  }

  it should "fail to resolve a absolute URI" in {

    assertThrows[AssertionError] {
      new LocalFileSystemDescriptorWithRootPath().resolve(new Path("/dir/file.txt"))
    }

    assertThrows[AssertionError] {
      new LocalFileSystemDescriptorWithRootPath().resolve(new Path("/c/file.txt"))
    }
  }

  it should "prevent breakout" in {
    assertThrows[AssertionError] {
      LocalFileSystemMapping("C:\\tmp\\dir2\\").resolve("../blub.txt")
    }
  }

  it should "resolve the correct directory for local FS resources" in {

    assertThrows[AssertionError] {
      //need a argument
      LocalFileSystemMapping(null).resolve("test").toString should be(
        s"file:/${new File("").getAbsolutePath.replace('\\', '/')}/test")
    }

    LocalFileSystemMapping("test").resolve("test2").toString should endWith("/test/test2")
    LocalFileSystemMapping(".").resolve("test2").toString should endWith("/test2")
    LocalFileSystemMapping("./test").resolve("test2").toString should endWith("/test/test2")

  }

  it should "resolve the correct directory based on a global absolute dir (Windows)" in {
    LocalFileSystemMapping("C:/tmp/dir2/").resolve("test").toString should be("file:/C:/tmp/dir2/test")
    LocalFileSystemMapping("C:\\tmp\\dir2\\").resolve("test").toString should be("file:/C:/tmp/dir2/test")
    LocalFileSystemMapping("C:\\tmp\\dir2").resolve("test").toString should be("file:/C:/tmp/dir2/test")

    LocalFileSystemMapping("C:\\tmp\\dir2\\").resolve("test/blub.txt").toString should be("file:/C:/tmp/dir2/test/blub.txt")
    LocalFileSystemMapping("C:\\tmp\\dir2\\").resolve("./test/blub.txt").toString should be(
      "file:/C:/tmp/dir2/test/blub.txt")
    LocalFileSystemMapping("C:\\tmp\\dir2\\").resolve("./blubblub/../test/blub.txt").toString should be(
      "file:/C:/tmp/dir2/test/blub.txt")
  }

  it should "resolve the correct directory based on a global absolute dir (Unix)" in {
    LocalFileSystemMapping("/tmp/dir4/").resolve("test").toString should be("file:/tmp/dir4/test")
  }

  it should "determine isFile and isDirectory correctly" in {
    val fs = FileSystems("localFsList")
    fs.isDirectory("tree_1") shouldBe true
    fs.isFile("tree_1") shouldBe false

    fs.isDirectory("tree_1/10_bytes.txt") shouldBe false
    fs.isFile("tree_1/10_bytes.txt") shouldBe true
  }


}
