package com.tiki.ai.fs

import com.tiki.ai.hadoop.HadoopConfigHolder
import org.scalatest.{BeforeAndAfterAll, FlatSpec, GivenWhenThen, Matchers}

import java.io.{BufferedWriter, FileNotFoundException, OutputStreamWriter}

class FileSystemLocationListTest
    extends FlatSpec
    with GivenWhenThen
    with Matchers
    with FileSystemsSupport
    with BeforeAndAfterAll
    with HadoopConfigHolder {

  val uid: String               = java.util.UUID.randomUUID.toString.replaceAll("-", "")
  val testDirContext: String    = s"target/$uid"
  val emptyDirLocation          = FileSystemLocation("localFS", testDirContext)
  val fsLoc: FileSystemLocation = FileSystemLocation.root("localFsList")

  override def beforeAll(): Unit = {

    emptyDirLocation.locate("blub.txt").open().write { oStream =>
      val writer = new BufferedWriter(new OutputStreamWriter(oStream))
      writer.write(uid)
      writer.flush()
    }

    emptyDirLocation.locate("blub.txt").delete()
  }

  override def afterAll(): Unit = {
    // clean-up
    emptyDirLocation.delete()
  }

  it should "correctly list directory contents" in {

    val mylistA = fsLoc.list()

    mylistA.length shouldBe 2
    mylistA should contain allOf (
      FileSystemLocation("localFsList", "tree_1"),
      FileSystemLocation("localFsList", "tree_2"),
    )

    val mylistB = FileSystemLocation("localFsList", "tree_1").list()
    mylistB.length shouldBe 2
    mylistB should contain allOf (
      FileSystemLocation("localFsList", "tree_1/10_bytes.txt"),
      FileSystemLocation("localFsList", "tree_1/20_bytes.txt"),
    )

    val mylistC = FileSystemLocation("localFsList", "tree_2").list()
    mylistC.length shouldBe 2
    mylistC should contain allOf (
      FileSystemLocation("localFsList", "tree_2/5_bytes.txt"),
      FileSystemLocation("localFsList", "tree_2/tree_4"),
    )

    val mylistD = emptyDirLocation.list()
    mylistD.length shouldBe 0

    val myListE = FileSystemLocation("localFsList", "tree_1/20_bytes.txt").list()
    myListE.length shouldBe 0

    assertThrows[FileNotFoundException] {
      FileSystemLocation("localFsList", "tree_45").list().foreach(println)
    }

  }

  it should "correctly listFiles" in {

    val mylistA = fsLoc.listFiles(true)

    mylistA should have length 5
    mylistA should contain allOf (FileSystemLocation("localFsList", "tree_1/10_bytes.txt"),
    FileSystemLocation("localFsList", "tree_1/20_bytes.txt"),
    FileSystemLocation("localFsList", "tree_2/5_bytes.txt"),
    FileSystemLocation("localFsList", "tree_2/tree_4/10_bytes.txt"),
    FileSystemLocation("localFsList", "tree_2/tree_4/20_bytes.txt"))

    val mylistB = fsLoc.listFiles(false)
    mylistB should have length 0

    val mylistC = fsLoc.locate("tree_1").listFiles(false)
    mylistC should have length 2
    mylistC should contain allOf (FileSystemLocation("localFsList", "tree_1/10_bytes.txt"),
    FileSystemLocation("localFsList", "tree_1/20_bytes.txt"))


    val mylistD = fsLoc.locate("tree_1/10_bytes.txt").listFiles(false)
    mylistD should have length 1
    mylistD should contain (FileSystemLocation("localFsList", "tree_1/10_bytes.txt"))
  }

}
