package com.tiki.ai.dw

import org.joda.time.DateTime
import sttp.client3.{FileBody, MultipartBody, StringBody}
import sttp.client3.testing.SttpBackendStub
import sttp.model.{Header, Method, Part, StatusCode}

import scala.concurrent.duration.{Duration, DurationInt}


trait TestModelStubs {
  val fs: Right[Nothing, Seq[String]] = Right(Seq("smb", "hdfs", "file", "pvc"))
  val listResult: Right[Nothing, Seq[ListResult]] = Right(Seq(ListResult("first", isDir = true, "file", 1),
                                        ListResult("second", isDir = true, "file", 2),
                                        ListResult("third", isDir = true, "file", 3)))

  val copyTaskResult: TaskResult = TaskResult(1, "COPY", TaskStatus.StatusRunning, "source", None, overwrite = true, None, "principal", 1)

  val copyResult: Right[Nothing, TaskResult] = Right(copyTaskResult)
  val moveResult: Right[Nothing, TaskResult] = Right(copyTaskResult.copy(operation = "MOVE"))

  val statusResult: Right[Nothing, TaskResult] = Right(copyTaskResult.copy(status = TaskStatus.StatusDone))

  val authTokenActual: String = "this-is-a-jwt-token"
  val authTokenExpected: String = "this-is-a-jwt-token"

  val source: String = "file:/this/is/a/path"
  val target: String = "file:/this/is/an/other/path"

  val downloadSource: String       = "file:/download/source.txt"
  val uploadSource: String         = "file:/download/upload.txt"
  val expectedUploadFileCount: Int = 3

}

trait HttpServer extends TestModelStubs {

  def port: Int

  lazy val backendStub =  SttpBackendStub.synchronous
    .whenRequestMatches(req =>
      !(req.headers(1) == Header(name = "Authorization", value = s"Bearer $authTokenExpected")))
    .thenRespondWithCode(StatusCode.Unauthorized)

    .whenRequestMatches(req =>
      req.uri.path.startsWith(List("fs")) &&
        req.method == Method.GET)
    .thenRespond(fs)

    .whenRequestMatches(req =>
      req.uri.path.startsWith(List("task")) &&
        req.method == Method.GET)
    .thenRespond(statusResult)

    .whenRequestMatches { req =>
      req.uri.path.startsWith(List("list")) &&
        req.method == Method.POST &&
        (req.body match {
          case StringBody(s, _, _) => s == s"""{\"uri\":\"$source\"}"""
          case _ => false
        })
    }
    .thenRespond(listResult)

    .whenRequestMatches { req =>
      req.uri.path.startsWith(List("delete")) &&
        req.method == Method.POST &&
        (req.body match {
          case StringBody(s, _, _) => s ==  s"""{\"uri\":\"$source\"}"""
          case _ => false
        })
    }
    .thenRespond(true)

    .whenRequestMatches { req =>
      req.uri.path.startsWith(List("copy")) &&
        req.method == Method.POST  &&
        (req.body match {
          case StringBody(s, _, _) => s == s"""{\"from\":\"$source\",\"overwrite\":true,\"to\":\"$target\"}"""
          case _ => false
        })
    }
    .thenRespond(copyResult)

    .whenRequestMatches { req =>
      req.uri.path.startsWith(List("move")) &&
        req.method == Method.POST  &&
        (req.body match {
          case StringBody(s, _, _) => s == s"""{\"from\":\"$source\",\"overwrite\":true,\"to\":\"$target\"}"""
          case _ => false
        })
    }
    .thenRespond(moveResult)

    .whenRequestMatches { req =>
      req.uri.path.startsWith(List("mkdir")) &&
        req.method == Method.POST &&
        (req.body match {
          case StringBody(s, _, _) => s ==  s"""{\"uri\":\"$source\"}"""
          case _ => false
        })
    }
    .thenRespond(true)

    .whenRequestMatches { req =>
      req.uri.path.startsWith(List("download")) &&
        req.method == Method.POST &&
        (req.body match {
          case StringBody(s, _, _) => s ==  s"""{\"uri\":\"$downloadSource\"}"""
          case _ => false
        })
    }
    .thenRespond("Hello World")

    .whenRequestMatches { req =>
      req.uri.path.startsWith(List("upload")) &&
        req.method == Method.POST &&
        (req.body match {
          case MultipartBody(parts) => parts.map {
            case Part(name, StringBody(s, _, _), _, _) if name == "to" => s == uploadSource
            case Part(name, StringBody(s, _, _), _, _) if name == "overwrite" => s == "false"
            case Part(name, FileBody(f, _), _, _) if name == "files" => f.name == "one.txt" || f.name == "two.txt" || f.name == "three.txt"
            case _ => false
          }.forall(x => x)
          case _ => false
        }) &&
        (req.body match {
          case MultipartBody(parts) => parts.count(_.name == "files") == expectedUploadFileCount
          case _ => false
        })
    }
    .thenRespond(true)

    .whenAnyRequest
    .thenRespondWithCode(StatusCode.BadRequest)
  }
