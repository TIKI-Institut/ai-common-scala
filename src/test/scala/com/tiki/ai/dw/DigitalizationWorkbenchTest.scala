package com.tiki.ai.dw

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

import java.io.File
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Try, Success}

class DigitalizationWorkbenchTest
  extends FlatSpec
    with Matchers
    with ScalaFutures
    with BeforeAndAfterAll
    with HttpServer {

  override def port = 9000

  val testClient: DigitalizationWorkbenchClient =
    new DigitalizationWorkbenchClient(serviceURL = s"http://localhost:$port") {
      override lazy val backend = backendStub
  }

  trait TestContext extends TestModelStubs {
    implicit val bearerContext: BearerContext = new BearerContext {
      override def token: String = authTokenActual
    }
  }

  "The client" should "handle fs requests" in new TestContext {

    val result: Try[Seq[String]] = testClient.fs()

    result.get shouldBe fs.value
  }

  it should "handle list request" in new TestContext {

    val result: Try[Seq[ListResult]] = testClient.list("file:/this/is/a/path")

    result.get shouldBe listResult.value

  }

  it should "handle delete request" in new TestContext {

    val result: Try[Unit] = testClient.delete("file:/this/is/a/path")

    result.isSuccess shouldBe true
  }

  it should "handle copy request" in new TestContext {

    val result: Try[Unit] = testClient.copy(source, target, wait = true, updateWait = 2.seconds)

    result.isSuccess.shouldEqual(true)
  }

  it should "handle move request" in new TestContext {

    val result: Try[Unit] = testClient.move(source, target, wait = false)

    result.isSuccess.shouldEqual(true)
  }

  it should "get status" in new TestContext {

    val result: Try[String] = testClient.taskStatus(taskID = 1)

    result.shouldEqual(Success(TaskStatus.StatusDone))
  }

  it should "throw client exception when rest server can not handle request" in new TestContext {

    val result: Try[Unit] = testClient.move("unexpectedA", "unexpectedB", wait = true)

    result match {
      case Failure(exception: ClientException) => exception.msg shouldBe "unexpected status 400 code"
      case _ => fail("expected a ClientException")
    }
  }

  it should "throw client exception when token is not valid" in new TestContext {

    override implicit val bearerContext: BearerContext = new BearerContext {
      override def token: String = "invalid token"
    }

    val result: Try[Seq[String]] = testClient.fs()

    result match {
      case Failure(exception: ClientException) => exception.msg shouldBe "unexpected status 401 code on operation"
      case _ => fail("expected a ClientException")
    }
  }

  it should "handle download request" in new TestContext {

    val result: Try[Array[Byte]] = testClient.download(downloadSource)

    result.get.map(_.toChar).mkString("") shouldBe "Hello World"
  }

  it should "handle upload request" in new TestContext {

    val files: Seq[File] = Seq(
      new File(getClass.getClassLoader.getResource("com/tiki/ai/dw/one.txt").getFile),
      new File(getClass.getClassLoader.getResource("com/tiki/ai/dw/two.txt").getFile),
      new File(getClass.getClassLoader.getResource("com/tiki/ai/dw/three.txt").getFile)
    )

    val result: Try[Unit] = testClient.upload(uploadSource, files)

    result.isSuccess shouldBe true
  }

  it should "handle mkdir request" in new TestContext {

    val result: Try[Unit] = testClient.mkDir(source)

    result.isSuccess shouldBe true
  }
}
