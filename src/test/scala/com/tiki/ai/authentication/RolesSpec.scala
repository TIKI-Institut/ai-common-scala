package com.tiki.ai.authentication

import java.io.ByteArrayOutputStream

import akka.http.scaladsl.server.{Directive0, Route}
import com.tiki.ai.authentication.oidc.OidcToken
import com.tiki.ai.bin.webKernel.WebResource
import org.scalatest.{FlatSpec, Matchers}

class RolesSpec extends FlatSpec with Matchers {

  it should "ensure that RolesListCommand produces correct output" in {
    val stream = new ByteArrayOutputStream()

    Console.withOut(stream) {
      RolesListCommand.listRoles(Array("roles"), autoExit = false)
    }

    stream.flush()
    val output = stream.toString
    stream.close()

    output.indexOf("tsr") should be > 0
    output.indexOf("tsrt") should be > 0
    output.indexOf("twr") should be > 0
    output.indexOf("{\"roles\": [") should be >= 0
  }
}

@RequiredRoles(roles = Array("tsr"))
private[authentication] class RoleSpecTestSecuredResource

@RequiredRoles(roles = Array("tsrt"))
private[authentication] trait RoleSpecTestSecuredResourceTrait

@RequiredRoles(roles = Array("twr"))
private[authentication] class RoleSpecTestWebResource extends WebResource {
  override def endpoint: Directive0                       = null
  override def route(oidcToken: Option[OidcToken]): Route = null
}
