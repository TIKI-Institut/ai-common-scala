package com.tiki.ai.authentication.web

import org.scalatest.{FlatSpec, Matchers}

class JwksSpec extends FlatSpec with Matchers {

  it should "return the correct public key (using RSA)" in {

    val jwksClass = new Jwks {}

    val publicKey = jwksClass.getPublicKeyFromJWKS(
      getClass.getClassLoader.getResource("com/tiki/ai/authentication/jwks.json").toString,
      kid = "dHnOaXbheTK9m1mWjYcg8NDgFmuekpjhL6LafCrrhmA"
    )

    publicKey should not be null
  }

  it should "throw exception when public key can not be found for the given kid" in {

    val jwksClass = new Jwks {}

    assertThrows[IllegalArgumentException] {
      jwksClass.getPublicKeyFromJWKS(
        getClass.getClassLoader.getResource("com/tiki/ai/authentication/jwks.json").toString,
        kid = "not-knwon-kid"
      )
    }
  }

  it should "throw exception if key identifier is duplicated in jwks.json" in {

    val jwksClass = new Jwks {}

    assertThrows[IllegalArgumentException] {
      jwksClass.getPublicKeyFromJWKS(
        getClass.getClassLoader.getResource("com/tiki/ai/authentication/jwks.json").toString,
        kid = "qOLo4HgeFsLYo2GJbTDeEQ0qwJ69uSjf2dgd5A5-3J4"
      )
    }
  }


}
