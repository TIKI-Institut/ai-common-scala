package com.tiki.ai.authentication
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.`X-Forwarded-Proto`
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{FlatSpec, _}

import scala.language.postfixOps

trait SharedRouteTest { this: FlatSpec with ScalatestRouteTest with Matchers =>

  def withUnsecuredRequest(routes: Route): Unit = {
    it should "respect requests with no authentication provided" in {
      Get("/") ~> Route.seal(routes) ~> check {
        handled shouldBe true
        status shouldEqual StatusCodes.Unauthorized
      }
    }
  }

  def withCorrectHttpsProxyHeader(routes: Route): Unit = {
    it should "respect proxy related requests" in {
      Get("/").addHeader(`X-Forwarded-Proto`("https")) ~> Route.seal(routes) ~> check {
        handled shouldBe true
        status shouldEqual StatusCodes.TemporaryRedirect
        response
          .getHeader("Location")
          .get()
          .value() shouldBe "https://auth.tiki-dsp.io/auth/realms/DSP/protocol/openid-connect/auth?redirect_uri=https%3A%2F%2Fexample.com%2Fdsp-service-path%2Foidc_callback%3Fdestination=https%3A%2F%2Fexample.com%2F&client_id=demo&scope=openid&response_type=code&access_type=offline"
      }
    }
  }

  def withWrongHttpsProxyHeader(routes: Route): Unit = {
    it should "respect only https related proxy requests" in {
      Get("/").addHeader(`X-Forwarded-Proto`("error-value")) ~> Route.seal(routes) ~> check {
        handled shouldBe true
        status shouldEqual StatusCodes.TemporaryRedirect
        response
          .getHeader("Location")
          .get()
          .value() shouldBe "https://auth.tiki-dsp.io/auth/realms/DSP/protocol/openid-connect/auth?redirect_uri=http%3A%2F%2Fexample.com%2Fdsp-service-path%2Foidc_callback%3Fdestination=http%3A%2F%2Fexample.com%2F&client_id=demo&scope=openid&response_type=code&access_type=offline"
      }
    }
  }
}
