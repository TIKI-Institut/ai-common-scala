package com.tiki.ai.authentication

import akka.actor.ActorSystem
import akka.http.scaladsl.model.headers.{Authorization, OAuth2BearerToken}
import akka.http.scaladsl.model.{ContentTypes, StatusCodes}
import akka.http.scaladsl.server
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Directive1, Route}
import akka.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import akka.stream.Materializer
import com.tiki.ai.akka.AkkaContext
import com.tiki.ai.authentication.oidc.{OidcSprayJson, OidcToken}
import com.tiki.ai.authentication.security.RSA
import com.tiki.ai.authentication.web.{BearerTokenNotFoundRejection, RoleSecurityDirective, TokenSecurityDirective}
import org.scalatest._
import org.scalatest.concurrent.{Eventually, IntegrationPatience, ScalaFutures}
import pdi.jwt.JwtAlgorithm.RS256
import pdi.jwt.{JwtAlgorithm, JwtHeader}

import java.security.{PrivateKey, PublicKey}
import java.time.Instant
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * This is the only test for the whole service.
  *
  * It tests top level behaviour ignoring implementation details.
  *
  */
class SecurityAuthorizationTest
    extends FlatSpec
    with Matchers
    with ScalatestRouteTest
    with Eventually
    with ScalaFutures
    with IntegrationPatience
    with SharedRouteTest { self =>

  override def afterAll(): Unit = {
    system.terminate()
  }

  private def getRequest(path: String, routes: Route)(action: => Assertion) = {
    Get(path) ~> Route.seal(routes) ~> check {
      routeHandledAssertion
      action
    }
  }

  private def routeHandledAssertion: Assertion = {
    handled shouldBe true
  }

  private def forbiddenAssertion: Assertion = {
    status shouldBe StatusCodes.Forbidden
    contentType shouldBe ContentTypes.`text/plain(UTF-8)`
  }

  private def unauthorizedAssertion: Assertion = {
    status shouldBe StatusCodes.Unauthorized
    contentType shouldBe ContentTypes.`text/plain(UTF-8)`
  }

  private def getWithToken = withToken(Get) _

  private def withToken(method: RequestBuilder)(path: String, token: String, route: server.Route)(action: => Assertion) = {
    method(path).addHeader(Authorization(OAuth2BearerToken(token))) ~> Route.seal(route) ~> check(action)
  }

  trait TestContext extends AkkaContext {
    implicit def system: ActorSystem        = self.system
    implicit def materializer: Materializer = self.materializer
    implicit def ec: ExecutionContext       = self.executor
  }

  trait TokenSecurityContext extends TokenSecurityDirective with TestContext {
    override def publicKeyFromJwkKey(tokenKeyId: String): PublicKey =
      RSA.getPublicKey(getClass.getClassLoader.getResource("com/tiki/ai/authentication/public.pem"))
    val privateKey: PrivateKey =
      RSA.getPrivateKey(getClass.getClassLoader.getResource("com/tiki/ai/authentication/private.pem"))
  }

  trait NoTokenContext extends TokenSecurityDirective with TestContext {
    override def authorize: Directive1[OidcToken] = provide(null)
  }

  trait MultipleRolesContext extends TokenSecurityDirective with TestContext {
    override def authorize: Directive1[OidcToken] =
      provide(
        new OidcToken(
          bearerToken = "raw-token",
          oauthClient = "test-client",
          realmRoles = Roles("needRole", "needRole2"),
          subject = Some("username"),
          jwtId = Some("id"),
          issuedAt = Some(new java.util.Date().getTime)
        ))
  }

  trait SingleRoleContext extends TokenSecurityDirective with TestContext {
    override def authorize: Directive1[OidcToken] =
      provide(
        new OidcToken(
          bearerToken = "raw-token",
          oauthClient = "test-client",
          realmRoles = Roles("needRole"),
          subject = Some("username"),
          jwtId = Some("id"),
          issuedAt = Some(new java.util.Date().getTime)
        ))
  }

  trait NoRolesContext extends TokenSecurityDirective with TestContext {
    override def authorize: Directive1[OidcToken] =
      provide(
        new OidcToken(bearerToken = "raw-token",
                      oauthClient = "test-client",
                      subject = Some("username"),
                      jwtId = Some("id"),
                      issuedAt = Some(Instant.now().getEpochSecond)))
  }

  trait PublicRoute {
    val testRoute: Route = pathEndOrSingleSlash {
      get {
        complete("Success")
      }
    }
  }

  trait MaliciouseRoute {
    val testRoute: Route = pathEndOrSingleSlash {
      get {
        reject(BearerTokenNotFoundRejection)
      }
    }
  }

  trait SecuredRoute { self: RoleSecurityDirective =>
    val testRoute: Route = pathEndOrSingleSlash {
      get {
        requireAllRoles(Seq.empty) { _ =>
          complete("Success")
        }
      }
    }
  }

  trait SingleRoleRoute { self: RoleSecurityDirective =>
    val testRoute: String => Route = role =>
      pathEndOrSingleSlash {
        get {
          requireAllRoles(Seq(role)) { _ =>
            complete("Success")
          }
        }
    }
  }

  trait SingleMaliciousRoleRoute { self: RoleSecurityDirective =>
    val testRoute: String => Route = role =>
      pathEndOrSingleSlash {
        get {
          requireAllRoles(Seq(role)) { _ =>
            //CUSTOM
            reject(BearerTokenNotFoundRejection)
          }
        }
    }
  }

  trait MultipleRoleRoute { self: RoleSecurityDirective =>
    val testRoute: Seq[String] => Route = roles =>
      pathEndOrSingleSlash {
        get {
          requireAllRoles(roles) { _ =>
            complete("Success")
          }
        }
    }
  }

  // Overrides default timeout used by ScalaFutures operations, like whenReady (We need this, cause the build system is really slow)
  override implicit val patienceConfig: PatienceConfig = PatienceConfig(timeout = 20 seconds)

  implicit def default(implicit system: ActorSystem): RouteTestTimeout =
    RouteTestTimeout(20 seconds)

  /////////////////////////////////////////////////////////////////////
  // Test unsecured directives
  /////////////////////////////////////////////////////////////////////

  it should "succeed on requesting a unsecured resource without a token" in new PublicRoute {
    getRequest("/", testRoute) {
      responseAs[String] shouldEqual "Success"
    }
  }

  it should "succeed on requesting a unsecured resource with a token without roles" in new NoRolesContext with PublicRoute {
    getRequest("/", testRoute) {
      responseAs[String] shouldEqual "Success"
    }
  }

  it should "succeed on requesting a unsecured resource with a token with a single role" in new SingleRoleContext
  with PublicRoute {
    getRequest("/", testRoute) {
      responseAs[String] shouldEqual "Success"
    }
  }

  it should "succeed on requesting a unsecured resource with a token with multiple roles" in new MultipleRolesContext
  with PublicRoute {
    getRequest("/", testRoute) {
      responseAs[String] shouldEqual "Success"
    }
  }

  /////////////////////////////////////////////////////////////////////
  // Test login directives
  /////////////////////////////////////////////////////////////////////

  it should "fail on requesting a secured resource without a token" in new SecuredRoute with NoTokenContext {
    getRequest("/", testRoute) {
      unauthorizedAssertion
    }
  }

  it should "succeed on requesting a login secured resource with a token without roles" in new NoRolesContext
  with SecuredRoute {
    getRequest("/", testRoute) {
      responseAs[String] shouldEqual "Success"
    }
  }

  it should "succeed on requesting a login secured resource with a token with a single role" in new SingleRoleContext
  with SecuredRoute {
    getRequest("/", testRoute) {
      responseAs[String] shouldEqual "Success"
    }
  }

  it should "succeed on requesting a login secured resource with a token with multiple roles" in new MultipleRolesContext
  with SecuredRoute {
    getRequest("/", testRoute) {
      responseAs[String] shouldEqual "Success"
    }
  }

  /////////////////////////////////////////////////////////////////////
  // Test single-role secured directives
  /////////////////////////////////////////////////////////////////////

  it should "fail on requesting a single role secured resource without a token" in new SingleRoleRoute with NoTokenContext {
    getRequest("/", testRoute("needRole")) {
      unauthorizedAssertion
    }
  }

  it should "fail on requesting a single role secured resource with a token not having any roles" in new NoRolesContext
  with SingleRoleRoute {
    getRequest("/", testRoute("needRole")) {
      responseAs[String] shouldEqual "The supplied authentication is not authorized to access this resource"
    }
  }

  it should "succeed on requesting a single role secured resource with a token having a single role which fits" in new SingleRoleContext
  with SingleRoleRoute {
    getRequest("/", testRoute("needRole")) {
      responseAs[String] shouldEqual "Success"
    }
  }

  it should "fail on requesting a single role secured resource with a token having a single role which doesnt fits" in new SingleRoleContext
  with SingleRoleRoute {
    getRequest("/", testRoute("needRole2")) {
      responseAs[String] shouldEqual "The supplied authentication is not authorized to access this resource"
    }
  }

  it should "succeed on requesting a single role secured resource with a token having multiple roles containing a fitting role" in new MultipleRolesContext
  with SingleRoleRoute {
    getRequest("/", testRoute("needRole")) {
      responseAs[String] shouldEqual "Success"
    }
  }

  it should "succeed on requesting a single role secured resource with a token having multiple roles containing a fitting role #2" in new MultipleRolesContext
  with SingleRoleRoute {
    getRequest("/", testRoute("needRole2")) {
      responseAs[String] shouldEqual "Success"
    }
  }

  it should "fail on requesting a single role secured resource with a token having multiple roles not containing a fitting role" in new MultipleRolesContext
  with SingleRoleRoute {
    getRequest("/", testRoute("needRole2222")) {
      responseAs[String] shouldEqual "The supplied authentication is not authorized to access this resource"
    }
  }

  /////////////////////////////////////////////////////////////////////
  // Test multi-role secured directives
  /////////////////////////////////////////////////////////////////////

  it should "fail on requesting a multi role secured resource without a token" in new MultipleRoleRoute with NoTokenContext {
    getRequest("/", testRoute(Seq("needRole", "needRole2"))) {
      unauthorizedAssertion
    }
  }

  it should "fail on requesting a multi role secured  with a token not having any roles" in new NoRolesContext
  with MultipleRoleRoute {
    getRequest("/", testRoute(Seq("needRole", "needRole2"))) {
      responseAs[String] shouldEqual "The supplied authentication is not authorized to access this resource"
    }
  }

  it should "fail on requesting a multi role secured with a token having only a single role which fits" in new SingleRoleContext
  with MultipleRoleRoute {
    getRequest("/", testRoute(Seq("needRole", "needRole2"))) {
      responseAs[String] shouldEqual "The supplied authentication is not authorized to access this resource"
    }
  }

  it should "fail on requesting a multi role secured with a token having a single role which doesnt fits" in new SingleRoleContext
  with MultipleRoleRoute {
    getRequest("/", testRoute(Seq("needRol4", "needRole5"))) {
      responseAs[String] shouldEqual "The supplied authentication is not authorized to access this resource"
    }
  }

  it should "succeed on requesting a multi role secured with a token having multiple roles which all fits" in new MultipleRolesContext
  with MultipleRoleRoute {
    getRequest("/", testRoute(Seq("needRole", "needRole2"))) {
      responseAs[String] shouldEqual "Success"
    }
  }

  it should "fail when no token is provided in header, cookie or parameter" in new MultipleRoleRoute with NoTokenContext {
    getRequest("/", testRoute(Seq("realmRole1", "clientRole1"))) {
      unauthorizedAssertion
    }
  }

  /* /////////////////// */
  /* Test token security */
  /* /////////////////// */

  it should "fail on parsing an empty token" in new TokenSecurityContext with MultipleRoleRoute {
    getWithToken("/", "", testRoute(Seq("realmRole1", "clientRole1"))) {
      forbiddenAssertion
    }
  }

  it should "fail when a token is expired" in new TokenSecurityContext with MultipleRoleRoute {

    val expiredToken = new OidcToken(
      bearerToken = "raw-token",
      oauthClient = oauthClient,
      realmRoles = Roles("needRole", "needRole2"),
      allClientRoles = Map("client1" -> Roles("c1", "c2"), "client2" -> Roles("cc1", "cc2")),
      subject = Some("username"),
      jwtId = Some("id"),
      expiration = Some(Instant.now().plusSeconds(0).getEpochSecond),
      issuer = Some("https://auth.tiki-dsp.io/auth/realms/TIKI"),
      audience = Some(Set(oauthClient))
    )

    getWithToken("/",
                 OidcSprayJson(oauthClient, "some-token").encode(expiredToken, privateKey, RS256),
                 testRoute(Seq("needRole", "needRole2"))) {
      forbiddenAssertion
    }
  }

  it should "fail when roles are provided which are not in the token" in new TokenSecurityContext with MultipleRoleRoute {
    val token = new OidcToken(
      bearerToken = "raw-token",
      oauthClient = oauthClient,
      realmRoles = Roles("needRole", "needRole2"),
      subject = Some("username"),
      jwtId = Some("id"),
      expiration = Some(Instant.now().plusSeconds(360).getEpochSecond),
      issuer = Some("https://auth.tiki-dsp.io/auth/realms/TIKI"),
      audience = Some(Set(oauthClient))
    )

    getWithToken("/",
                 OidcSprayJson(oauthClient, "some-token").encode(token, privateKey, RS256),
                 testRoute(Seq("needRole", "nonexistingrole"))) {
      forbiddenAssertion
    }
  }

  it should "succeed on parsing a valid token with known issuer and client roles" in new TokenSecurityContext
  with MultipleRoleRoute {
    val token = new OidcToken(
      bearerToken = "raw-token",
      oauthClient = oauthClient,
      realmRoles = Roles("needRole", "needRole2"),
      subject = Some("username"),
      jwtId = Some("id"),
      expiration = Some(Instant.now().plusSeconds(360).getEpochSecond),
      issuer = Some("https://auth.tiki-dsp.io/auth/realms/tiki"),
      audience = Some(Set(oauthClient))
    )

    val header: JwtHeader =
      JwtHeader(algorithm = JwtAlgorithm.RS256, typ = "JWT", contentType = "application/json", keyId = "key-id")

    getWithToken("/",
                 OidcSprayJson(oauthClient, "some-token").encode(header, token, privateKey),
                 testRoute(Seq("needRole", "needRole2"))) {
      responseAs[String] shouldEqual "Success"
    }
  }

  // Unsecured request test
  it should behave like withUnsecuredRequest((new SecuredRoute with NoTokenContext).testRoute)

}
