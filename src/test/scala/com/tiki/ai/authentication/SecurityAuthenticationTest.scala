package com.tiki.ai.authentication

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.{Authorization, Cookie, OAuth2BearerToken}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.stream.Materializer
import com.tiki.ai.akka.AkkaContext
import com.tiki.ai.authentication.web.TokenSecurityDirective
import org.scalatest._
import org.scalatest.concurrent.{Eventually, IntegrationPatience, ScalaFutures}

import scala.concurrent.ExecutionContext
import scala.language.postfixOps

/**
  * This is the only test for the whole service.
  *
  * It tests top level behaviour ignoring implementation details.
  *
  */
class SecurityAuthenticationTest
    extends FlatSpec
    with Matchers
    with ScalatestRouteTest
    with Eventually
    with ScalaFutures
    with IntegrationPatience
    with SharedRouteTest { self =>

  trait TestContext extends TokenSecurityDirective with AkkaContext {

    implicit def system: ActorSystem        = self.system
    implicit def materializer: Materializer = self.materializer
    implicit def ec: ExecutionContext       = self.executor
  }

  trait BearerRoute { self: TestContext =>
    val testRoute: Route =
      pathEndOrSingleSlash {
        bearerToken { t =>
          get {
            complete(t)
          }
        }
      }
  }

  it should "extract the token from HEADER" in new TestContext with BearerRoute {
    Get("/").withHeaders(Authorization(OAuth2BearerToken("HEADER_TOKEN"))) ~> Route.seal(testRoute) ~> check {
      handled shouldBe true
      responseAs[String] shouldEqual "HEADER_TOKEN"
    }
  }

  it should "extract the token from COOKIE" in new TestContext with BearerRoute {
    Get("/").withHeaders(Cookie("X-AUTH-TOKEN", "COOKIE_TOKEN")) ~> Route.seal(testRoute) ~> check {
      handled shouldBe true
      responseAs[String] shouldEqual "COOKIE_TOKEN"
    }
  }

  it should "extract the token from URL PARAMETER" in new TestContext with BearerRoute {
    Get("/?code=PARAMETER_TOKEN") ~> Route.seal(testRoute) ~> check {
      handled shouldBe true
      responseAs[String] shouldEqual "PARAMETER_TOKEN"
    }
  }

  it should "extract the token from HEADER > COOKIE > URL PARAMETER" in new TestContext with BearerRoute {
    Get("/?code=PARAMETER_TOKEN")
      .withHeaders(Cookie("X-AUTH-TOKEN", "COOKIE_TOKEN"))
      .withHeaders(Authorization(OAuth2BearerToken("HEADER_TOKEN"))) ~> Route.seal(testRoute) ~> check {
      handled shouldBe true
      responseAs[String] shouldEqual "HEADER_TOKEN"
    }
  }

  it should "extract the token from COOKIE > URL PARAMETER" in new TestContext with BearerRoute {
    Get("/?code=PARAMETER_TOKEN")
      .withHeaders(Cookie("X-AUTH-TOKEN", "COOKIE_TOKEN")) ~> Route.seal(testRoute) ~> check {
      handled shouldBe true
      responseAs[String] shouldEqual "COOKIE_TOKEN"
    }
  }

  it should "fail if no token was provided" in new TestContext with BearerRoute {
    Get("/") ~> Route.seal(testRoute) ~> check {
      handled shouldBe true
      status shouldEqual StatusCodes.Unauthorized
    }
  }

  it should "correctly configured with max request header size of 32k" in new TestContext with BearerRoute {
    val configValue: String = testConfig.getString("akka.http.parsing.max-header-value-length")

    configValue shouldBe "32k"
  }

  // Unsecured request test
  it should behave like withUnsecuredRequest((new TestContext with BearerRoute).testRoute)
}
