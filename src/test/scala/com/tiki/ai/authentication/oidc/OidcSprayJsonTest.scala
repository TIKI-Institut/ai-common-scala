package com.tiki.ai.authentication.oidc

import com.tiki.ai.authentication.Roles
import com.tiki.ai.authentication.security.RSA
import org.scalatest._
import pdi.jwt.JwtAlgorithm.RS256
import pdi.jwt.exceptions.JwtExpirationException
import pdi.jwt.{JwtAlgorithm, JwtHeader}

import java.security.SignatureException
import java.time.temporal.ChronoUnit
import java.time.{Clock, Instant}
import scala.util.Success

class OidcSprayJsonTest extends FlatSpec with Matchers {

  private val subject = Some("testsubject")

  private val oauthTestClient = "test-client"
  private val claims          = new OidcToken(oauthClient = oauthTestClient, subject = subject, bearerToken = "some-token")

  private implicit val clock: Clock = Clock.systemUTC()

  it should "create and verify a token" in {
    val rawToken =
      OidcSprayJson(oauthTestClient, "some-token").encode(claims.expiresIn(10))
    rawToken.length should not be 0
    OidcSprayJson(oauthTestClient, "some-token").decode(rawToken) match {
      case Success(dt) =>
        dt.subject shouldEqual subject
      case _ => Failed("Raw token could not be decoded")
    }
  }

  it should "create but fail to validate expired token" in {
    val rawToken = OidcSprayJson(oauthTestClient, "some-token").encode(claims.expiresIn(0))
    rawToken.length should not be 0

    assertThrows[JwtExpirationException] {
      OidcSprayJson(oauthTestClient, "some-token").validate(rawToken)
    }
  }

  it should "fail to validate token create with different keys" in {

    val header: JwtHeader =
      JwtHeader(algorithm = JwtAlgorithm.RS256, typ = "JWT", contentType = "application/json", keyId = "key-id")

    val privKey1 = RSA.getPrivateKey(getClass.getClassLoader.getResource("com/tiki/ai/authentication/private.pem"))
    val pubKey2  = RSA.getPublicKey(getClass.getClassLoader.getResource("com/tiki/ai/authentication/public-2.pem"))

    val rawToken = OidcSprayJson(oauthTestClient, "some-token").encode(header, claims.expiresIn(10), privKey1)
    rawToken.length should not be 0

    assertThrows[SignatureException] {
      OidcSprayJson(oauthTestClient, "some-token").validate(rawToken, pubKey2, Seq(RS256))
    }
  }

  it should "create a token with nonempty claims" in {
    val realmRoles  = Roles("read_status", "write_alarm")
    val clientRoles = Roles("client_role_1", "client_role_2", "client_role_3")

    val fullClaims = new OidcToken(
      bearerToken = "raw-token",
      oauthClient = oauthTestClient,
      allClientRoles = Map("test-client" -> clientRoles),
      realmRoles = realmRoles,
      subject = subject,
      expiration = Some(Instant.now().plus(36500L, ChronoUnit.DAYS).getEpochSecond)
    )

    fullClaims.realmRoles.roles should contain("write_alarm")
    fullClaims.clientRoles.roles should contain("client_role_2")

    val rawToken = OidcSprayJson(oauthTestClient, "some-token").encode(fullClaims)
    rawToken.length should not be 0

    OidcSprayJson(oauthTestClient, "some-token").decode(rawToken) match {
      case Success(dt) =>
        dt.realmRoles.roles.contains("write_alarm") shouldBe true
        dt.clientRoles.roles.contains("client_role_2") shouldBe true
      case _ => Failed("Raw token could not be decoded")
    }
  }
}
