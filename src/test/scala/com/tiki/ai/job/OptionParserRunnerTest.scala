package com.tiki.ai.job

import com.tiki.ai.job.OptionParserRunner.OptionParserArguments
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}
import scopt.{OParser, OptionParser}

import scala.util.Try

class OptionParserRunnerTest
  extends FlatSpec
    with GivenWhenThen
    with Matchers {

  it should "run the job with the given cli arguments" in {

    val arguments: Seq[String] = Seq[String]("--name", "blub")

    val result = TestJob(arguments)

    result.isSuccess shouldEqual true
  }

  it should "run the job with the given cli arguments (new scopt OParser)" in {

    TestJobOParser(Seq[String]("--name", "blub")).isSuccess.shouldEqual(true)
  }

  it should "run the job through CLIRunner explictely calling execute" in {
    val job = new TestJob()

    job.execute(Seq[String]("--name", "blub"))

    job.myJobName.shouldEqual("blub")
  }

  it should "exit abnormaly if job is being executed with wrong params " in {
    assertThrows[IllegalArgumentException](TestJob(Seq[String]("--dskgjlhdsfgj", "blub")))
  }
}

private[job] case class TestJobConfig(name: String = "TestTest") extends JobConfig {
  val parser: OptionParser[TestJobConfig] = new OptionParser[TestJobConfig]("initialJobNAme") {
    opt[String]('n', "name") optional() valueName "<name>" action { (x, c) =>
      c.copy(name = x)
    } text "Job name"
  }
}

object TestJob extends OptionParserRunner {
  def apply(arguments:Seq[String]): Try[Unit] = new TestJob().execute(arguments)
}

private[job] class TestJob
  extends Job[TestJobConfig]
    with CLIOptionParserRunner[TestJobConfig] {

  var myJobName: String = _

  class JobNameNotBlubException extends Exception

  /**
    * Main method that executes business logic of the job
    */
  override def run(jobConfig: TestJobConfig): Unit = {
    myJobName = jobConfig.name

    if (myJobName != "blub") throw new JobNameNotBlubException
  }

  override def parse(arguments: Seq[String]): TestJobConfig = {
    val config = TestJobConfig()

    config.withArguments(arguments)(config.parser)
  }
}

object TestJobOParser extends OptionParserRunner {
  def apply(arguments: Seq[String]): Try[Unit] = new TestJobOParser().execute(arguments)
}

private[job] class TestJobOParser
   extends Job[TestJobConfig]
   with CLIOptionParserRunner[TestJobConfig] {

    var myJobName: String = _

    class JobNameNotBlubException extends Exception

    /**
      * Main method that executes business logic of the job
      */
    override def run(jobConfig: TestJobConfig): Unit = {
      myJobName = jobConfig.name

      if (myJobName != "blub") throw new JobNameNotBlubException
    }

    override def parse(arguments: Seq[String]): TestJobConfig = {
      val config = TestJobConfig()

      val parser: OParser[String, TestJobConfig] = {
        val builder = OParser.builder[TestJobConfig]

        import builder._

        OParser.sequence(
          opt[String]('n', "name") optional() valueName "<name>" action { (x, c) =>
            c.copy(name = x)
          } text "Job name"
        )
      }

      config.withArguments(arguments, parser)
    }
}
