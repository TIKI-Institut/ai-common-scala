package com.tiki.ai.job

import org.scalatest.{FlatSpec, Matchers}

class JobExecutionSpec extends FlatSpec with Matchers {

  private val statePath = "target/job.config"

  it should "write state to a file" in {
    JobExecution.persist(JobExecution("12345", JobStatus.finished, ""), statePath)

    val loadedJobState = JobExecution.fromFile(statePath)
    loadedJobState.status shouldEqual JobStatus.finished
    loadedJobState.executionId shouldEqual "12345"
  }

  it should "generate json file" in {
    val jobState = JobExecution("12345", JobStatus.finished, "")
    val json     = JobExecution.asJsonString(jobState)
    json.contains("12345") shouldBe true
    json.contains("finished") shouldBe true
  }

}
