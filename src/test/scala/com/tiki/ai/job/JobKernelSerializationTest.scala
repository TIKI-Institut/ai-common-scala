package com.tiki.ai.job

import com.tiki.ai.spark.{SparkApp, SparkDependentTest, SparkSessionConfiguration}
import org.scalatest.{FlatSpec, Matchers}

import java.io.{ByteArrayOutputStream, NotSerializableException, ObjectOutputStream}

trait JobKernelSerializationTest extends FlatSpec with Matchers with SparkDependentTest {

  sealed trait NotSerializableStuff {
    def body: () => Unit = () => println("Body")
  }

  it should "be serializable" in {
    // Create a sample spark job
    val normalSparkJob = new TestSparkJob()

    // Serialize the job
    // No error should occur
    try {
      new ObjectOutputStream(new ByteArrayOutputStream()).writeObject(normalSparkJob)
    } catch {
      case e: Exception =>
        println(e.getMessage)
        throw e
    }

    // A malformed job should not be serializable
    assertThrows[NotSerializableException] {
      val brokenJob = new TestSparkJob with NotSerializableStuff
      // No serialization error should be raised
      new ObjectOutputStream(new ByteArrayOutputStream()).writeObject(brokenJob)
    }
  }
}

private[job] class TestSparkJob extends SparkApp[JobConfig] {

  var result: Int = -1

  /**
    * Main method that executes business logic of the job
    */
  override def run(jobConfig: JobConfig): Unit = {
    result = spark.sparkContext.parallelize(1 until 200).reduce(_ + _)
  }
  updateSparkConfig(SparkSessionConfiguration.runSparkLocally)

}
