package com.tiki.ai.hadoop

import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

sealed trait Feature extends HadoopConfigSupport {
  updateHadoopConfig(_.set("static-config", "static-value"))
}

sealed trait HadoopFeature1Support extends Feature {
  updateHadoopConfig(_.set("feature1-config", "value-f1"))
}

sealed trait HadoopFeature2Support extends Feature {
  updateHadoopConfig(_.set("feature2-config", "value-f2"))
}

sealed trait BuildingBlockSupport extends HadoopFeature1Support with HadoopFeature2Support {
  updateHadoopConfig(_.set("feature-for-building-block-config", "value-block"))
}

sealed trait ContextA extends HadoopFeature1Support with HadoopConfigHolder
sealed trait ContextB extends HadoopFeature2Support with HadoopConfigHolder {
  updateHadoopConfig(_.set("feature2-config", "new-hard-feature2-config-value"))
}

/**
  * A minimalistic example to run a spark app
  */
class HadoopConfigurationTest extends FlatSpec with Matchers with BeforeAndAfterAll {

  it should "respect all registered hadoop configuration changes" in new ContextA {
    hadoopConfiguration.get("static-config") shouldBe "static-value"
    hadoopConfiguration.get("feature1-config") shouldBe "value-f1"
  }

  it should "respect all registered hadoop configuration changes when changed inside constructor code" in new ContextB {
    hadoopConfiguration.get("static-config") shouldBe "static-value"
    hadoopConfiguration.get("feature2-config") shouldBe "new-hard-feature2-config-value"
  }

  it should "respect all registered hadoop configuration changes from multiple traits" in new ContextB
  with HadoopFeature1Support {
    hadoopConfiguration.get("static-config") shouldBe "static-value"
    hadoopConfiguration.get("feature1-config") shouldBe "value-f1"
    hadoopConfiguration.get("feature2-config") shouldBe "new-hard-feature2-config-value"
  }

  it should "respect all registered hadoop configuration changes from nested traits" in new BuildingBlockSupport
  with HadoopConfigHolder {
    hadoopConfiguration.get("static-config") shouldBe "static-value"
    hadoopConfiguration.get("feature1-config") shouldBe "value-f1"
    hadoopConfiguration.get("feature2-config") shouldBe "value-f2"
    hadoopConfiguration.get("feature-for-building-block-config") shouldBe "value-block"
  }

  it should "respect all registered hadoop configuration changes from method code (not in constructor)" in new ContextA {
    updateHadoopConfig(_.set("feature1-config", "new-method-changed-feature1-config-value"))
    hadoopConfiguration.get("feature1-config") shouldBe "new-method-changed-feature1-config-value"
  }

  it should "respect all registered hadoop configuration changes after the hadoop config was created the first time" in new ContextA {
    hadoopConfiguration.get("feature1-config") shouldBe "value-f1"
    updateHadoopConfig(_.set("feature1-config", "new-method-changed-feature1-config-value"))
    hadoopConfiguration.get("feature1-config") shouldBe "new-method-changed-feature1-config-value"
  }

  it should "respect all registered hadoop configuration changes in its own holders if there are multiple holders in one execution context" in {
    val ctxA = new ContextA {}
    val ctxB = new ContextB {}

    ctxA.hadoopConfiguration.get("feature1-config") shouldBe "value-f1"
    ctxB.hadoopConfiguration.get("feature1-config") shouldBe null

    ctxA.hadoopConfiguration.get("feature2-config") shouldBe null
    ctxB.hadoopConfiguration.get("feature2-config") shouldBe "new-hard-feature2-config-value"
  }
}
