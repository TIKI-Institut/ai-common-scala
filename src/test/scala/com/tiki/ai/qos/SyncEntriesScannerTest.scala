package com.tiki.ai.qos

import com.tiki.ai.qos.model.SyncQosEntry
import org.scalatest.{FlatSpec, Matchers}

class SyncEntryTest extends SyncQosEntry {
  override val name: String = "testWebEntry"
  override def run()        = 3
}

class SyncEntriesScannerTest extends FlatSpec with Matchers with SyncEntriesScanner {

  it should "collect all sync entries" in {
    val entries = syncEntries
    entries.head.name shouldEqual "testWebEntry"
    entries.head.entryType shouldEqual "Sync"
  }
}
