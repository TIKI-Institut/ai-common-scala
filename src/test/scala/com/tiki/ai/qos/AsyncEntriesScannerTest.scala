package com.tiki.ai.qos

import org.scalatest.{FlatSpec, Matchers}

class AsyncEntriesScannerTest extends FlatSpec with Matchers with AsyncEntriesScanner {

  it should "collect all async entries" in {
    val entries = asyncEntries
    entries.head.name shouldEqual "asyncEntryTest"
    entries.head.entryType shouldEqual "Async"
  }
}
