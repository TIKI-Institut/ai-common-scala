package com.tiki.ai.qos.cmd

import com.tiki.ai.qos.model.{FlavorReference, QosEndpoint, QosEntry, QosJsonSupport}
import org.scalatest.{FlatSpec, Matchers}

trait TestQosRegisterCommand extends QosRegisterCommand {
  override val exit            = new ExitMock()
  override lazy val mqttClient = new MqttClientMock("entryTest")
}

class QosRegisterCommandTest extends FlatSpec with Matchers with QosJsonSupport {

  import spray.json._

  it should "register all qos jobs" in new TestQosRegisterCommand {
    qosRegister(Array("qos-register"))

    val registeredEndpoint: QosEndpoint = mqttClient.message.parseJson.convertTo[QosEndpoint]
    registeredEndpoint shouldBe QosEndpoint(Seq(QosEntry("asyncEntryTest", "Async")).toList,
                                            flavorReference =
                                              FlavorReference("dsp-service", "tiki", "dev", "test-flavor", "0.0.1"))
  }
}
