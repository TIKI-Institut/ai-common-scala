package com.tiki.ai.qos.cmd

import com.tiki.ai.sys.Exit

class ExitMock extends Exit {
  var code: Option[Int] = None
  override def exit(code: Int): Unit = {
    if (this.code.isEmpty) {
      this.code = Some(code)
    }
  }
}
