package com.tiki.ai.qos.cmd

import akka.Done
import akka.stream.alpakka.mqtt.MqttQoS
import com.tiki.ai.mqtt.MqttClient

import scala.concurrent.Future

class MqttClientMock(clientId: String) extends MqttClient {
  var message: String = ""

  def publishMessage(msg: String, payload: String, mqttQos: MqttQoS): Future[Done] = Future.successful {
    message = msg
    Done
  }
}
