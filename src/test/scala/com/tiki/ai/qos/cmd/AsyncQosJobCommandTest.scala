package com.tiki.ai.qos.cmd

import com.tiki.ai.qos.model.{AsyncQosEntry, QosJsonSupport, QosRunResult, QosRunStatus}
import com.tiki.ai.sys.Exit
import org.scalatest.{FlatSpec, Matchers}
import spray.json.DefaultJsonProtocol

class AsyncEntryTest() extends AsyncQosEntry {
  override def run(): Double = 5
  override def name: String  = "asyncEntryTest"
}

trait TestAsyncJobCommand extends AsyncQosJobCommand {
  override val exit            = new ExitMock()
  override lazy val mqttClient = new MqttClientMock("entryTest")
}

class AsyncQosJobCommandTest extends FlatSpec with Matchers with QosJsonSupport {

  import spray.json._

  it should "run async qos job" in new TestAsyncJobCommand {
    run(Array("qos-run", "1", "asyncEntryTest"))
    exit.code.get should be(0)
  }

  it should "inform about non existing async qos job" in new TestAsyncJobCommand {
    run(Array("qos-run", "1", "notExists"))
    exit.code.get should be(0)

    val msg = mqttClient.message.parseJson.convertTo[QosRunResult]
    msg.status should be(QosRunStatus.NotFound)

  }

  it should "inform about non invalid parameter" in new TestAsyncJobCommand {
    run(Array("qos-run", "invalid", "notExists"))

    val msg = mqttClient.message.parseJson.convertTo[QosRunResult]
    msg.status should be(QosRunStatus.NotFound)

    exit.code.get should be(Exit.ABNORMAL_TERMINATION)

  }

  it should "inform about non existing parameter" in new TestAsyncJobCommand {
    run(Array("qos-run", "1"))
    exit.code.get should be(Exit.ABNORMAL_TERMINATION)
  }

}
