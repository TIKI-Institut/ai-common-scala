package com.tiki.ai.qos

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.MissingQueryParamRejection
import com.tiki.ai.bin.webKernel.{SecuredRoutes, WebResourceProvider}
import com.tiki.ai.config.CommonLibConfig
import com.tiki.ai.qos.model.{QosEndpoint, QosJsonSupport, QosRunResult, QosRunStatus}
import org.scalatest.{FlatSpec, Matchers}
import spray.json.DefaultJsonProtocol
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.concurrent.{IntegrationPatience, ScalaFutures}

class QosRoutesTest
    extends FlatSpec
    with Matchers
    with WebResourceProvider
    with ScalatestRouteTest
    with QosRoutes
    with QosJsonSupport
    with DefaultJsonProtocol
      with ScalaFutures
      with IntegrationPatience {

  import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

  val routes: Route = new SecuredRoutes(webResources, CommonLibConfig.AuthConfig.dspWebDeploymentPath).routes ~ qosRoutes

  it should "not handle qos endpoint" in {
    Get("/qos") ~> routes ~> check {
      handled shouldEqual false
    }
  }

  it should "list entries" in {
    Get("/qos/list") ~> routes ~> check {
      val rsp = responseAs[QosEndpoint]
      rsp.flavorReference.name shouldBe ("dsp-service")
      rsp.podReference.get.pod shouldBe ("test-pod")
      rsp.entries.head.name shouldBe ("testWebEntry")
    }
  }

  it should "execute entry" in {
    Get("/qos/run?transactionId=1&name=testWebEntry") ~> routes ~> check {
      val rsp = responseAs[QosRunResult]
      rsp.status shouldBe (QosRunStatus.Success)
      rsp.score shouldBe (Some(3.0))
    }
  }

  it should "inform about non existing entry" in {
    Get("/qos/run?transactionId=1&name=nonExistEntry") ~> routes ~> check {
      val rsp = responseAs[QosRunResult]
      rsp.status shouldBe (QosRunStatus.NotFound)
    }
  }

  it should "inform about missing name query parameter" in {
    Get("/qos/run?transactionId=1") ~> routes ~> check {
      rejection shouldEqual MissingQueryParamRejection("name")
    }
  }

  it should "inform about transactionId query parameter" in {
    Get("/qos/run?name=nonExistEntry") ~> routes ~> check {
      rejection shouldEqual MissingQueryParamRejection("transactionId")
    }
  }

  it should "return bad request status by invalid transactionId query parameter" in {
    Get("/qos/run?transactionId=invalidValue&name=testWebEntry") ~> routes ~> check {
      status shouldEqual StatusCodes.BadRequest
    }
  }

}
