package com.tiki.ai.spark

import com.tiki.ai.job.JobConfig
import com.tiki.ai.job.JobRunner.executeJob
import org.apache.spark.sql.functions.{col, to_timestamp}
import org.scalatest.MustMatchers.convertToAnyMustWrapper
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

trait SparkTimeZoneTest
  extends FlatSpec
    with Matchers
    with BeforeAndAfterAll
    with SparkDependentTest {

  //This test whether the setting spark.sql.session.timeZone is set to utc. Since this does not change anything
  //one if your machine runs in UTC, this will always work on Jenkins, but you can check that
  //the setting is indeed needed on your local machine
  it should "convert the timestamp correctly" in {

    val config = SparkTimeZoneJobConfig()
    val job = new SparkTimeZoneJob()

    executeJob(job, config) must be a 'success

    job.result should be(3600)
  }
}

private[spark] case class SparkTimeZoneJobConfig(name: String = "timezoneJob")
  extends JobConfig

private[spark] class SparkTimeZoneJob
  extends SparkApp[SparkTimeZoneJobConfig] {

  updateSparkConfig(SparkSessionConfiguration.runSparkLocally)

  var result: Long = 0

  /**
    * Main method that executes business logic of the job
    */
  override def run(jobConfig: SparkTimeZoneJobConfig): Unit = {

    import spark.implicits._

    result = spark.createDataFrame(Seq(Tuple1("1970-01-01 01:00:00"))).toDF("timestring")
      .withColumn("timestamp", to_timestamp(col("timestring")))
      .withColumn("timelong", col("timestamp").cast("long"))
      .select("timelong").as[Long].collect().head
  }

}
