package com.tiki.ai.spark
import com.tiki.ai.hadoop.HadoopConfigSupport
import com.tiki.ai.job.{CLIOptionParserRunner, JobConfig, OptionParserRunner}
import org.apache.spark.SparkConf
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

import scala.util.Try

/**
  * A minimalistic example to run a spark app
  */
class SingleSparkJobTest extends
  FlatSpec
  with Matchers
  with BeforeAndAfterAll {


  val config: MostSimpleSparkJobConfig = MostSimpleSparkJobConfig("some-name")
  implicit val sparkSessionFactory: Option[SparkSessionFactory] = None

  it should "run a spark job without touching spark variable (no spark session will be created)" in {
    val job = new MostSimpleSparkJobWithoutTouchingSparkSession {
      override def terminate(jobConfig: MostSimpleSparkJobConfig): Unit = {}
    }

    val arguments = Seq("--name", "myname")
    job.execute(arguments)

    job.duration should be < 1000L
  }



  it should "run a spark job with touching spark variable (spark session will be created)" in {

    val arguments = Seq("--name", "myname")
    val job = new MostSimpleSparkJobWithTouchingSparkSession()

    job.execute(arguments)


     job.sparkConfig.isEmpty shouldBe false

   assertThrows[NoSuchElementException] {
     job.sparkConfig.get.get("spark.hadoop.some-test-config")
   }
  }

  it should "run a spark job with touching spark variable and using additional hadoop configs support (spark session will be created)" in {
    val job = new MostSimpleSparkJobWithTouchingSparkSession with AdditionalHadoopConfig
    val arguments = Seq("--name", "myname")
    job.execute(arguments)
    job.sparkConfig.get.get("spark.hadoop.some-test-config") shouldBe "some-test-config-value"
  }
}

trait AdditionalHadoopConfig extends HadoopConfigSupport {
  updateHadoopConfig(_.set("some-test-config", "some-test-config-value"))
}

case class MostSimpleSparkJobConfig(name: String) extends JobConfig

object MostSimpleSparkJobWithoutTouchingSparkSession extends OptionParserRunner {
  override def apply(arguments: Seq[String]): Try[Unit] =
    new MostSimpleSparkJobWithTouchingSparkSession().execute(arguments)
}

class MostSimpleSparkJobWithoutTouchingSparkSession
  extends SparkApp[MostSimpleSparkJobConfig]
  with CLIOptionParserRunner[MostSimpleSparkJobConfig] {
  updateSparkConfig(SparkSessionConfiguration.runSparkLocally)

  var duration: Long = 0

  /**
    * Transfer the local configs to your sessionFactory
    */
  override def initialize(jobConfig: MostSimpleSparkJobConfig): Unit = {
    super.initialize(jobConfig)

    duration = -System.currentTimeMillis()
  }

  /**
    * Clean-up method called after the job execution (after run())
    */
  override def terminate(jobConfig: MostSimpleSparkJobConfig): Unit = {
    super.terminate(jobConfig)
    duration += System.currentTimeMillis()
  }

  override def run(jobConfig: MostSimpleSparkJobConfig): Unit = {
    println("Spark job successfully run")
  }

  def parse(arguments: Seq[String]): MostSimpleSparkJobConfig = MostSimpleSparkJobConfig(name = "abc")
}


object MostSimpleSparkJobWithTouchingSparkSession extends OptionParserRunner {
  override def apply(arguments: Seq[String]): Try[Unit] =
    new MostSimpleSparkJobWithTouchingSparkSession().execute(arguments)
}
class MostSimpleSparkJobWithTouchingSparkSession
  extends SparkApp[MostSimpleSparkJobConfig]
    with CLIOptionParserRunner[MostSimpleSparkJobConfig] {
  updateSparkConfig(SparkSessionConfiguration.runSparkLocally)

  var sparkConfig: Option[SparkConf] = None

  override def run(jobConfig: MostSimpleSparkJobConfig): Unit = {

    import spark.implicits._

    val ds = spark.createDataset(Seq(1, 2, 3, 4))
    ds.count()
    sparkConfig = Some(spark.sparkContext.getConf)
    println("Spark job successfully run")
  }

  override def parse(arguments: Seq[String]): MostSimpleSparkJobConfig = MostSimpleSparkJobConfig(name = "def")
}
