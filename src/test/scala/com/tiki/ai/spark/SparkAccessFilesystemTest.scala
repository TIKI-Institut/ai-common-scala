package com.tiki.ai.spark

import com.tiki.ai.fs.FsLocation
import com.tiki.ai.job.JobConfig
import com.tiki.ai.job.JobRunner.executeJob
import org.apache.spark.SerializableWritable
import org.scalatest.MustMatchers.convertToAnyMustWrapper
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

trait SparkAccessFilesystemTest
  extends FlatSpec
    with Matchers
    with BeforeAndAfterAll
    with SparkDependentTest {


  it should "be able to access a FS Location in middle of a spark job" in {

    val config = SparkAccessFilesystemTestJobConfig()
    val job = new SparkAccessFilesystemTestJob()

    executeJob(job, config) must be a 'success

    job.result should be(25)
  }
}

private[spark] case class SparkAccessFilesystemTestJobConfig(name: String = "testJob",
                                                             fileToRead: FsLocation =
                                                             FsLocation("localFsList", "tree_2/5_bytes.txt"))
  extends JobConfig

private[spark] class SparkAccessFilesystemTestJob
  extends SparkApp[SparkAccessFilesystemTestJobConfig] {

  updateSparkConfig(SparkSessionConfiguration.runSparkLocally)

  var result: Int = 0

  /**
    * Main method that executes business logic of the job
    */
  override def run(jobConfig: SparkAccessFilesystemTestJobConfig): Unit = {

    import spark.implicits._

    val hadoopConfigB = context.broadcast(new SerializableWritable(context.hadoopConfiguration))

    result = spark.sparkContext
      .parallelize(0 to 4)
      .toDS()
      .map { i =>
        (i, jobConfig.fileToRead)
      }
      .repartition()
      .map {
        case (_, fsLocation) =>
          val text = fsLocation.open()(hadoopConfigB.value.value).readText
          text.length

      }
      .reduce((a, b) => a + b)

  }

}
