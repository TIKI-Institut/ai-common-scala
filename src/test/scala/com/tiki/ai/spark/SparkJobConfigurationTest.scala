package com.tiki.ai.spark

import org.apache.spark.SparkConf
import org.scalatest.MustMatchers.convertToAnyMustWrapper
import org.scalatest.{FlatSpec, Matchers}

class SingleSparkJobWithConfigChanges
  extends SparkApp[MostSimpleSparkJobConfig] {
  updateSparkConfig(SparkSessionConfiguration.runSparkLocally)
  updateHadoopConfig(_.set("late-hadoop-config-1", "some-value"))
  updateHadoopConfig(_.set("late-hadoop-config-2", "some-old-value"))
  updateHadoopConfig(_.set("late-hadoop-config-3", "some-oldest-value"))
  updateSparkConfig(_.set("late-spark-config-1", "some-value"))
  updateSparkConfig(_.set("late-spark-config-2", "some-old-value"))
  updateHadoopConfig(_.set("late-spark-config-3", "some-oldest-value"))

  var sparkConfig: Option[SparkConf] = None

  /**
    * Transfer the local configs to your sessionFactory
    */
  override def initialize(jobConfig: MostSimpleSparkJobConfig): Unit = {
    updateHadoopConfig(_.set("late-hadoop-config-2", "some-value-2"))
    updateSparkConfig(_.set("late-spark-config-2", "some-value-2"))
    super.initialize(jobConfig)
  }

  override def run(jobConfig: MostSimpleSparkJobConfig): Unit = {
    updateHadoopConfig(_.set("late-hadoop-config-3", "some-value-3"))
    updateSparkConfig(_.set("late-spark-config-3", "some-value-3"))

    sparkConfig = Some(spark.sparkContext.getConf)
    println("Spark job successfully run")
  }
}

class TestSingleSparkJobWithConfigChanges extends FlatSpec with Matchers {

  import com.tiki.ai.job.JobRunner._

  val job = new SingleSparkJobWithConfigChanges
  val config: MostSimpleSparkJobConfig = MostSimpleSparkJobConfig("some-name")
  it should "set the spark & hadoop conf" in {
    executeJob(job, config) must be a 'success

    job.sparkConfig.isEmpty shouldBe false

    job.sparkConfig.get.get("late-spark-config-1") shouldBe "some-value"
    job.sparkConfig.get.get("late-spark-config-2") shouldBe "some-value-2"
    job.sparkConfig.get.get("late-spark-config-3") shouldBe "some-value-3"
    job.sparkConfig.get.get("spark.hadoop.late-hadoop-config-1") shouldBe "some-value"
    job.sparkConfig.get.get("spark.hadoop.late-hadoop-config-2") shouldBe "some-value-2"
    job.sparkConfig.get.get("spark.hadoop.late-hadoop-config-3") shouldBe "some-value-3"
  }
}
