package com.tiki.ai.spark

import com.tiki.ai.hadoop.HadoopConfigHolder

import com.tiki.ai.spark.config._
import org.apache.spark.SparkConf
import org.apache.spark.sql.{Dataset, SparkSession}
import org.scalatest.{FlatSpec, Matchers}

class SparkSessionNestedConfigurationTest extends FlatSpec with Matchers {

  import com.tiki.ai.spark.config.SparkSessionConfigSupport._

  it should "respect managed configurations of multiple traits" in {

    val job = new JobMock

    val sparkConfig = new SparkConf()

    job.applySparkConfig(sparkConfig)

    sparkConfig.get(SparkExecutorCoresConfigName) shouldEqual "16"
    sparkConfig.get(SparkExecutorMemoryConfigName) shouldEqual "512m"
  }

  it should "respect configurations from code outside of job" in {

    val job = new JobMock

    job.updateSparkConfig(SparkSessionConfiguration.runSparkLocally)
    job.executorCount(1024)
    job.executorCores(4096)

    val sparkConfig = new SparkConf()

    job.applySparkConfig(sparkConfig)

    sparkConfig.get(SparkExecutorMemoryConfigName) shouldEqual "512m"
    sparkConfig.get(SparkExecutorCountConfigName) shouldEqual "1024"
    sparkConfig.get(SparkExecutorCoresConfigName) shouldEqual "4096"
    sparkConfig.get("spark.master") shouldEqual "local"
  }

}

private[spark] class JobMock extends MyProducer with MyTransform with SparkSessionConfigHolder with HadoopConfigHolder {

}

trait MyProducer extends SparkSessionConfigSupport  {

  executorCores(4)
  executorMemory(512)

  def loadData()(implicit spark: SparkSession): Dataset[(Int, String)] = {
    import spark.implicits._
    spark.sparkContext.parallelize(Seq(3 -> "blub", 4 -> "fooooo", 5 -> "bar")).toDS()
  }
}

trait MyTransform extends SparkSessionConfigSupport  {

  executorCores(16)
  executorMemory(64)

  def transform(in: Dataset[(Int, String)])(implicit spark: SparkSession): Dataset[Int] = {
    import spark.implicits._
    in.map { case (int, string) => int + string.length }
  }
}
