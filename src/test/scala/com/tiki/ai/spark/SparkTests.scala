package com.tiki.ai.spark

import com.tiki.ai.job.JobKernelSerializationTest
import org.scalatest.{BeforeAndAfterAll, Suite, Suites}

trait SparkDependentTest {
  implicit def sparkSessionFactory: SparkSessionFactory
}

class SparkTests extends Suites() with BeforeAndAfterAll {

  implicit val sessionFactory : SparkSessionFactory= new SharedSparkSessionFactory()

  override protected def afterAll(): Unit = {
    super.afterAll()
    sessionFactory.currentSession().foreach(_.stop())
  }

  override val nestedSuites: collection.immutable.IndexedSeq[Suite] =
    Vector(
      new JobKernelSerializationTest { override implicit val sparkSessionFactory: SparkSessionFactory = sessionFactory },
      new SparkAccessFilesystemTest  { override implicit val sparkSessionFactory: SparkSessionFactory = sessionFactory },
      new SparkTimeZoneTest          { override implicit val sparkSessionFactory: SparkSessionFactory = sessionFactory }
    )
}
