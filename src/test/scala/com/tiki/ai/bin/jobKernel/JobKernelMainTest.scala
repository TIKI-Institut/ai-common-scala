package com.tiki.ai.bin.jobKernel

import java.security.Permission

import com.tiki.ai.job.{JobConfig, OptionParserRunner}
import com.tiki.ai.spark.{SparkApp, SparkSessionConfiguration}
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}
import scopt.OptionParser

import scala.util.Try

// TODO eliminate these, see todo on beforeAll()
sealed case class SystemExitException(status: Int)
    extends SecurityException("System.exit() was attempted but shall be thrown") {}

// TODO eliminate these, see todo on beforeAll()
sealed class NoExitSecurityManager extends SecurityManager {
  override def checkPermission(perm: Permission): Unit = {}

  override def checkPermission(perm: Permission, context: Object): Unit = {}

  override def checkExit(status: Int): Unit = {
    super.checkExit(status)
    throw SystemExitException(status)
  }
}

trait JobKernelMainTest
  extends FlatSpec
    with Matchers
    with BeforeAndAfterAll {
  // TODO re-factor this, because if used more than one time, it causes Exception!
  // "Forked test harness failed: java.net.SocketException: Connection reset"
  // which can only be worked around if setting testForkedParallel to false which is a BAD solution.
  // Sporadically causes also spark job tests to fail with spark-session-closed exception
  override def beforeAll(): Unit = System.setSecurityManager(new NoExitSecurityManager())
  override def afterAll(): Unit  = System.setSecurityManager(null)

  it should "execute correctly configured job without an error" in {
    try {
      Main.main(Array("com.tiki.ai.bin.jobKernel.LocalTestSparkJobCLIRunner", "--name", "Meine Anwendung"))
    } catch {
      case e: SystemExitException => e.status should be(0)
    }
  }

  it should "exit with non-zero exit code if jobs run method throws" in {
    try {
      Main.main(Array("com.tiki.ai.bin.jobKernel.LocalTestSparkJobCLIRunner", "--name", "Meine Anwendung", "--fail", "true"))
    } catch {
      case e: SystemExitException => e.status should not be 0
    }
  }

  it should "abort execution with exception, if provided job arguments are incorrect" in {
    assertThrows[IllegalArgumentException](
      Main.main(Array("com.tiki.ai.bin.jobKernel.LocalTestSparkJobCLIRunner", "--blblb", "Meine Anwendung")))
  }

  it should "abort execution with exception, if non existent job runner is provided" in {
    assertThrows[IllegalArgumentException](Main.main(Array("com.tiki.ai.bin.jobKernel.NonExistentJobCLIRunner")))
  }
}

private[jobKernel] case class LocalTestSparkJobConfig(name: String = "testJob", fail: Boolean = false) extends JobConfig

private[jobKernel] class LocalTestSparkJob
  extends SparkApp[LocalTestSparkJobConfig] {

  updateSparkConfig(SparkSessionConfiguration.runSparkLocally)
  updateHadoopConfig(c => { c.set("adfss.sec", "") })

  var result: Int = -1

  /**
    * Main method that executes business logic of the job
    */
  override def run(jobConfig: LocalTestSparkJobConfig): Unit = {
    if (jobConfig.fail) throw new RuntimeException("I shall fail!")

    result = spark.sparkContext.parallelize(1 until 200).reduce(_ + _)
  }


}

private[jobKernel] object LocalTestSparkJobCLIRunner extends OptionParserRunner {
  import com.tiki.ai.job.JobRunner._



  def apply(arguments: Seq[String]): Try[Unit] = {

    val job = new LocalTestSparkJob()

    val parser = new OptionParser[LocalTestSparkJobConfig]("initialJobNAme") {
      opt[String]('n', "name") optional() valueName "<name>" action { (x, c) =>
        c.copy(name = x)
      } text "Job name"
      opt[Boolean]("fail") optional() valueName "<true|false>" action { (fail, c) =>
        c.copy(fail = fail)
      }
    }

    val config = parser.parse(arguments, LocalTestSparkJobConfig()).get

    executeJob(job, config)

  }

}
