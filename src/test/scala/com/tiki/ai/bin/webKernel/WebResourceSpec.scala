package com.tiki.ai.bin.webKernel

import akka.http.scaladsl.server.{Directive0, Route}
import com.tiki.ai.authentication.RequiredRoles
import com.tiki.ai.authentication.oidc.OidcToken
import org.scalatest.{FlatSpec, Matchers}

class WebResourceSpec extends FlatSpec with Matchers {

  it should "enumerate solely WebResource descendants" in new WebResourceProvider {
    webResources.size should be > 0
    webResources.foreach {
      case y: WebResourceTestWebResource =>
        assert(y.getClass.isAnnotationPresent(classOf[RequiredRoles]),
               "WebResourceTestWebResource has to be annotated with @RequiredRoles but it was not")

        y.getClass
          .getAnnotation(classOf[RequiredRoles])
          .roles should contain("twr")
      case _ => // noop
    }
  }
}

@RequiredRoles(roles = Array("twr"))
private[webKernel] class WebResourceTestWebResource extends WebResource {
  override def endpoint: Directive0                       = null
  override def route(oidcToken: Option[OidcToken]): Route = null
}

// the abstract class shall not cause instantiation exception
@RequiredRoles(roles = Array("atwr"))
private[webKernel] abstract class WebResourceAbstractTestWebResource extends WebResource {
  override def endpoint: Directive0                       = null
  override def route(oidcToken: Option[OidcToken]): Route = null
}

// the trait shall not cause instantiation exception
@RequiredRoles(roles = Array("ttwr"))
private[webKernel] trait WebResourceTraitTestWebResource extends WebResource {
  override def endpoint: Directive0                       = null
  override def route(oidcToken: Option[OidcToken]): Route = null
}
