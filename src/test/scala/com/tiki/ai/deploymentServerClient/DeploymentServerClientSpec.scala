package com.tiki.ai.deploymentServerClient

import org.scalatest.words.ShouldVerb
import org.scalatest.{FlatSpec, Matchers}
import sttp.capabilities
import sttp.client3.testing.SttpBackendStub
import sttp.client3.{Identity, Response, StringBody, UriContext}
import sttp.model.{Method, StatusCode}

import scala.util.{Failure, Success}

class DeploymentServerClientSpec
  extends FlatSpec
    with DeploymentServerClient
    with Matchers
    with ShouldVerb {

  override def principal = "principal"
  override def project = "project"
  override def environment = "environment"
  override def flavorVersion = "1.1.1"

  override lazy val httpBackend: SttpBackendStub[Identity, capabilities.WebSockets] = mockedDeploymentServerBackend

  val mockedDeploymentServerBackend: SttpBackendStub[Identity, capabilities.WebSockets] = SttpBackendStub.synchronous
    .whenRequestMatchesPartial {

      //Check Auth header (fail fast)
      case request
        if request.headers("Authorization") != Vector("Bearer someToken") =>
        Response("unauthorized", StatusCode.Unauthorized)

      //The deploy request without with or without override json
      case request
        if request.method == Method.POST &&
          request.uri == uri"https://deployment-server.tiki-dsp.io/latest/phase/deploy/principal/project/environment/train-model-job/1.1.1" =>
        //If the request body is set, it has to be the override expected one
        if (request.body.show != "empty") {
          request.headers("Content-Type") shouldBe Vector("application/json")
          request.body.asInstanceOf[StringBody].s shouldBe """{"a": 2}"""
        }

        Response("success", StatusCode.Accepted)

      //If nothing matches
      case _ => Response("error", StatusCode.BadRequest)
    }

  it should "Send the deploy request without override json (success))" in {
    val response = deploy("train-model-job", token = "someToken")

    response shouldBe Success((202, "success"))
  }

  it should "Send the deploy request without override json (bad request))" in {

    val response = deploy("train-model-job", deploymentServerBaseUrl = "fake-base-url", token = "someToken")

    response shouldBe Failure(DeploymentServerClientException("unexpected status 400 code on " +
      "fake-base-url/latest/phase/deploy/principal/project/environment/train-model-job/1.1.1 POST"))
  }

  it should "Send the deploy request without override json (unauthorized))" in {

    val response = deploy("train-model-job", deploymentServerBaseUrl = "fake-base-url", token = "token123")

    response shouldBe Failure(DeploymentServerClientException("unexpected status 401 code on " +
      "fake-base-url/latest/phase/deploy/principal/project/environment/train-model-job/1.1.1 POST"))
  }

  it should "Send the deploy request with override json (success)" in {

    val response = deploy("train-model-job", token = "someToken", overrideJson = Some("""{"a": 2}"""))

    response shouldBe Success((202, "success"))
  }

  it should "Read the token from the file" in {

    val testFile = getClass.getClassLoader.getResource("com/tiki/ai/deploymentServerClient/tokenFile").getFile
    val token = getTokenFromFile(testFile)

    token shouldBe "eyJhbGciOiJSUzI1NiIsInR5cCI"
  }
}
