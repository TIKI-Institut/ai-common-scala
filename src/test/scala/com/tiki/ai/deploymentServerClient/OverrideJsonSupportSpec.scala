package com.tiki.ai.deploymentServerClient

import org.scalatest.words.ShouldVerb
import org.scalatest.{FlatSpec, Matchers}

class OverrideJsonSupportSpec
  extends FlatSpec
    with Matchers
    with ShouldVerb
    with OverrideJsonSupport {

  it should "Build an override json string with args, envVars & a postfix" in {
    val testOverrideJsonStringWithAll = buildJobOverrideJsonString(jobName = "sampleJobName",jobArgs = Map(("--A", "B"), ("--C", "D")), postfix = "-11", envVars = Map("a" -> "b", "c" -> "d"))
    testOverrideJsonStringWithAll shouldEqual """[{"args":["driver","sampleJobName","--A","B","--C","D"],"envVars":[{"name":"a","value":"b"},{"name":"c","value":"d"}],"namePostfix":"-11"}]"""
  }

  it should "Build an override json string with only a postfix" in {
    val testOverrideJsonStringWithPostfix = buildJobOverrideJsonString(jobName = "sampleJobName", postfix = "-11")
    testOverrideJsonStringWithPostfix shouldEqual """[{"args":[],"envVars":[],"namePostfix":"-11"}]"""
  }

  it should "Build an override json string with only envVars" in {
    val testOverrideJsonStringWithEnvVars = buildJobOverrideJsonString(jobName = "sampleJobName", envVars = Map("a" -> "b", "c" -> "d"))
    testOverrideJsonStringWithEnvVars shouldEqual """[{"args":[],"envVars":[{"name":"a","value":"b"},{"name":"c","value":"d"}],"namePostfix":""}]"""
  }

  it should "Build an override json string with only args (Spark Job)" in {
    val testOverrideJsonStringWithArgs = buildJobOverrideJsonString("sampleJobName", jobArgs = Map(("--A", "B"), ("--C", "D")))
    testOverrideJsonStringWithArgs shouldEqual """[{"args":["driver","sampleJobName","--A","B","--C","D"],"envVars":[],"namePostfix":""}]"""
  }

  it should "Build an override json string with only args (no Spark Job)" in {
    val testOverrideJsonStringWithArgs = buildJobOverrideJsonString("sampleJobName", jobArgs = Map(("--A", "B"), ("--C", "D")), isSparkJob = false)
    testOverrideJsonStringWithArgs shouldEqual """[{"args":["sampleJobName","--A","B","--C","D"],"envVars":[],"namePostfix":""}]"""
  }
}
