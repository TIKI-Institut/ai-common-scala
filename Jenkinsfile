pipeline {
    agent { kubernetes { label 'dsp-scala-212' } }

    options {
        // do not clean TAG build logs
        buildDiscarder(env.TAG_NAME == null ? logRotator(daysToKeepStr: '30') : null)
        // ensures that automated branch indexing doesn't trigger branch job builds,
        // but also destroys the its feature to trigger on bitbucket commit push
        // solution: additional bitbucketPush trigger below
        overrideIndexTriggers(false)
    }

    triggers {
        // ensure that the branch job itself gets triggered on bitbucket commit push
        bitbucketPush()
    }

    stages {
        stage('Prepare') {
            steps {
                bitbucketStatusNotify(buildState: 'INPROGRESS')
            }
        }
        stage('Build') {
            steps {
                container('scala212') {
                    sh '''sbt clean +compile'''
                }
            }
        }

        stage('Test'){
            steps {
                container('scala212') {
                    sh '''sbt clean test'''
                }
            }
        }

        stage('Publish Snapshot'){
            steps {
                container('scala212') {
                    sh '''sbt +publish'''
                }
            }
        }
    }

    post {
        failure {
            notifyViaSlack("#FF0000")
            bitbucketStatusNotify(buildState: 'FAILED')
        }
        fixed {
            notifyViaSlack("#00FF00")
            bitbucketStatusNotify(buildState: 'SUCCESSFUL')
        }
        always {
            junit 'target/test-reports/*.xml'
        }
    }

}

// this function decides if this build is a release build
Boolean isReleaseVersion() {
    def releaseVersions = ["master"]
    return releaseVersions.contains(env.BRANCH_NAME)
}


def notifyViaSlack(String colorCode) {
  def subject = "${currentBuild.currentResult}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} ${env.BUILD_URL}"

  slackSend (color: colorCode, message: summary)
}
