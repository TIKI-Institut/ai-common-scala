import sbt.Keys._
import sbt._

// *****************************************************************************
// Library dependencies
// *****************************************************************************
lazy val library = new {

  lazy val scala212 = "2.12.17"
  object Version {
    val akka       = "2.6.20"  // WARNING: With 2.7.0 and above, Akka has a paid licensing model !
    val akkaHttp   = "10.2.10" // WARNING: With 10.4.0 and above, Akka Http has a paid licensing model !
    val akkaMqtt   = "3.0.4"   // WARNING: With 5.0.0 and above, Alpakka Mqtt has a paid licensing model !
    val classgraph = "4.8.149"
    val jwt        = "9.0.2"
    val nscalaTime = "2.18.0"
    val scalaMock  = "4.1.0"
    val scalaTest  = "3.0.5"
    val scopt      = "4.0.1"
    val sl4j       = "2.0.6"
    val typesafe   = "1.4.1"
    val sttp       = "3.5.2" //Note: All versions above 3.5.2 require JDK 11!!

    // Provided by spark dsp runtime
    val breeze = "1.2" // Note: Should be the same version as in spark-mllib of the used spark version
    val hadoop = "3.3.4"
    val spark  = "3.4.1"
    val jsch   = "0.1.55"
  }

  val classgraph                     = "io.github.classgraph" % "classgraph" % Version.classgraph
  val scopt                          = "com.github.scopt" %% "scopt" % Version.scopt
  val typesafeConfig                 = "com.typesafe" % "config" % Version.typesafe
  val nscalaTime                     = "com.github.nscala-time" %% "nscala-time" % Version.nscalaTime
  val sttp                           = "com.softwaremill.sttp.client3" %% "core" % Version.sttp
  val sttpSprayJson                  = "com.softwaremill.sttp.client3" %% "spray-json" % Version.sttp
  val jwtDependencies: Seq[ModuleID] = Seq(Jwt.jwt, Jwt.jwtSprayJson)

  // Test dependencies
  val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest % Test
  val scalaMock = "org.scalamock" %% "scalamock" % Version.scalaMock % Test

  private object Jwt {
    val jwt          = "com.github.jwt-scala" %% "jwt-core"       % Version.jwt
    val jwtSprayJson = "com.github.jwt-scala" %% "jwt-spray-json" % Version.jwt
  }

  val sparkDependencies: Seq[ModuleID] =
    Seq(Spark.core, Spark.sql, Spark.breeze).map(_ % Provided).map(_.excludeAll(Spark.exclusions: _*))

  private trait SparkExclusions {
    // excluding hadoop because spark comes with hard dependency on hadoop-client 2.6!
    val exclusions: Seq[ExclusionRule] = Seq[ExclusionRule](
      ExclusionRule("log4j", "log4j")
    )
  }

  private object Spark extends SparkExclusions {
    val core   = "org.apache.spark" %% "spark-core" % Version.spark
    val sql    = "org.apache.spark" %% "spark-sql"  % Version.spark
    val breeze = "org.scalanlp"     %% "breeze"     % Version.breeze
  }

  private object Logging {
    val slf4J        = "org.slf4j" % "slf4j-api"      % Version.sl4j force ()
    val jclOverSlf4J = "org.slf4j" % "jcl-over-slf4j" % Version.sl4j
    val julToSlf4J   = "org.slf4j" % "jul-to-slf4j"   % Version.sl4j

    // forcing only logback on Test scope
    val log4jOverSlf4J = "org.slf4j"      % "log4j-over-slf4j" % Version.sl4j % Test
    val logback        = "ch.qos.logback" % "logback-classic"  % "1.2.11"     % Test
  }

  val loggingDependencies = Seq(Logging.slf4J, Logging.julToSlf4J, Logging.jclOverSlf4J)

  val loggingDependenciesForTest = Seq(Logging.log4jOverSlf4J, Logging.logback)

  val akkaDependencies =
    Seq(Akka.actor, Akka.slf4j, Akka.testkit, Akka.stream)

  val akkaHttpDependencies = Seq(Akka.akkaHttp, Akka.akkaHttpTest, Akka.sprayJson)

  val akkaAlpakkaDependencies = Seq(Akka.alpakkaMqtt)

  private object Akka {
    val actor  = "com.typesafe.akka" %% "akka-actor"  % Version.akka
    val stream = "com.typesafe.akka" %% "akka-stream" % Version.akka
    val slf4j  = "com.typesafe.akka" %% "akka-slf4j"  % Version.akka

    val akkaHttp  = "com.typesafe.akka" %% "akka-http"            % Version.akkaHttp
    val sprayJson = "com.typesafe.akka" %% "akka-http-spray-json" % Version.akkaHttp

    // akka-stream-alpakka-mqtt 2.0.x introduces akka 2.5 dependency which is incompatible with akka 2.4
    // we need to exclude the dependency on akka, to consolidate the akka versions
    // otherwise apps using ai-common-scala will resolve teh newer akka version introduced by 'akka-stream-alpakka-mqtt'
    val alpakkaMqtt = "com.lightbend.akka" %% "akka-stream-alpakka-mqtt" % Version.akkaMqtt excludeAll ExclusionRule(
      "com.typesafe.akka")

    val testkit      = "com.typesafe.akka" %% "akka-testkit"      % Version.akka     % Test
    val akkaHttpTest = "com.typesafe.akka" %% "akka-http-testkit" % Version.akkaHttp % Test
  }

  val hadoopDependencies: Seq[ModuleID] =
    // hadoop jars are provided by cluster
    Seq(Hadoop.client, Hadoop.jsch).map(_ % Provided).map(_.excludeAll(Hadoop.exclusions: _*))

  private object Hadoop {
    // Using the new shaded hadoop client library introduced in 3.3.1
    // Spark and our DSP Kernels also uses this libraries
    // To avoid any classpath problems and also to simplify dependency hell due to transitive dependencies we are switching to the shaded client libs
    val client = "org.apache.hadoop"                   % "hadoop-client-api" % Version.hadoop
    val jsch   = "org.apache.hadoop.shaded.com.jcraft" % "jsch"              % Version.jsch

    val exclusions: Seq[ExclusionRule] = Seq[ExclusionRule](
      ExclusionRule("log4j", "log4j")
    )
  }

}

// *****************************************************************************
// Settings
// *****************************************************************************

// The nexus credentials to publish to the remote repository
credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")

lazy val projectSettings =
  Seq(
    organization := "com.tiki",
    organizationName := "TIKI GmbH",
    startYear := Some(2018),
    scalaVersion := library.scala212,
    licenses += ("Apache-2.0", new URL("https://www.apache.org/licenses/LICENSE-2.0.txt")),
    javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-encoding", "UTF-8"),
    resolvers ++= Seq(
      "DSP" at "https://nexus.tiki-dsp.io/repository/maven-public/"
    ),
    publishTo := {
      if (isSnapshot.value)
        Some("nexus-snapshots" at "https://nexus.tiki-dsp.io/repository/maven-snapshots/")
      else
        Some("nexus-releases" at "https://nexus.tiki-dsp.io/repository/maven-releases/")
    },
    scalacOptions ++= Seq(
      "-deprecation",
      "-encoding",
      "UTF-8",
      "-feature",
      "-unchecked",
      "-Ywarn-numeric-widen",
      "-Xfatal-warnings",
      "-Yno-adapted-args",
      "-Xfuture",
      "-Ypartial-unification"
    ),
    Compile / doc / sources := Seq.empty
  )

lazy val testSettings: Seq[Def.Setting[_]] = Seq(
  Test / fork := true,
  Test / javaOptions ++= Seq("-XX:+UseG1GC",
                             "-XX:+UseCompressedOops",
                             "-XX:+UseStringDeduplication",
                             "-Dfile.encoding=UTF-8")
)

// *****************************************************************************
// Projects
// *****************************************************************************
lazy val `ai-common` = (project in file("."))
  .settings(projectSettings)
  .settings(testSettings)
  .settings(
    libraryDependencies ++= Seq(
      "org.scala-lang" % "scala-reflect" % scalaVersion.value,
      library.classgraph,
      library.nscalaTime,
      library.scopt,
      library.typesafeConfig,
      library.sttp,
      library.sttpSprayJson,
      library.scalaTest,
      library.scalaMock
    )
      ++ library.loggingDependenciesForTest
      ++ library.loggingDependencies
      ++ library.akkaDependencies
      ++ library.akkaHttpDependencies
      ++ library.akkaAlpakkaDependencies
      ++ library.hadoopDependencies
      ++ library.jwtDependencies
      ++ library.sparkDependencies
  )
