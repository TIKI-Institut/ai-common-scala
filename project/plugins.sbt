// Comment to get more information during initialization
logLevel := Level.Info

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.7.5")

addSbtPlugin("com.github.sbt" % "sbt-release" % "1.1.0")

addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.5.3")

addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.4.0")

addDependencyTreePlugin

/**
  * This will check that the transitive dependencies of your project do not exhibit any binary compatibility conflict,
  * assuming that the methods of your Compile confliction (in src/main/) are all called.
  * TODO: Integrate this in our build pipeline when all errors are resolved
  * Usage:
  *   sbt missinglinkCheck
  */
addSbtPlugin("ch.epfl.scala" % "sbt-missinglink" % "0.3.3")
